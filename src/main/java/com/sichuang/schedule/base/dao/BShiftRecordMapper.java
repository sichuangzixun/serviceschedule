package com.sichuang.schedule.base.dao;

import com.sichuang.schedule.model.entity.WorkRecordExample;

import java.util.List;

/**
 * @author xs
 * @date 2018/9/11 11:19
 */
public interface BShiftRecordMapper {

    /**
     * 更新值班员名单
     * @param shiftRecordIdList
     * @return
     */
   int updateWorkers(List<Integer> shiftRecordIdList);

    /**
     * 获取班次id
     * @param example
     * @return
     */
   List<Integer> getShiftRecordIdList(WorkRecordExample example);
}
