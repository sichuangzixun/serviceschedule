package com.sichuang.schedule.base.dao;

import com.alibaba.fastjson.JSONObject;
import com.sichuang.schedule.model.entity.ShiftRecordExample;
import com.sichuang.schedule.model.entity.WorkItem;
import com.sichuang.schedule.model.entity.WorkItemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BWorkItemMapper {

    /**
     * 更新处理情况，累加
     * @param workItem
     * @return
     */
    int updateHandleInformation(WorkItem workItem);
}
