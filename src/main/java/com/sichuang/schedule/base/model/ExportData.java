package com.sichuang.schedule.base.model;

import com.sichuang.schedule.model.entity.AttentionPoint;
import com.sichuang.schedule.model.entity.ShiftRecord;
import com.sichuang.schedule.model.entity.WorkRecord;

import java.util.List;

public class ExportData {

    /**
     * 值班记录
     */
    List<WorkRecord> workRecords;

    /**
     * 交接内容
     */
    List<ShiftRecord> shiftRecord;
//    /**
//     * 白班
//     */
//    List<WorkRecord> dayWorkRecord;
//    /**
//     * 早班
//     */
//    List<WorkRecord> morningWorkRecord;
//    /**
//     * 夜班
//     */
//    List<WorkRecord> nightWorkRecord;
//    /**
//     * 宵班
//     */
//    List<WorkRecord> midNightWorkRecord;
//
//    /**
//     * 考勤
//     */
//    List<WorkRecord> attendanceWorkRecord;
//
//    /**
//     * 工作纪录记录
//     */
//    List<WorkRecord> workDisciplineRecord;


//    /**
//     * 宵班与早班交接
//     */
//    List<ShiftRecord> midNightToMorningShiftRecord;
//    /**
//     * 早班与夜班交接
//     */
//    List<ShiftRecord> morningToNightShiftRecord;
//    /**
//     * 夜班与宵班交接
//     */
//    List<ShiftRecord> nightToMidNightShiftRecord;
//    /**
//     * 当天遗留事项
//     */
//    List<ShiftRecord> todayUnFinishedShiftRecord;
//
    /**
     * 长期知识
     */
    List<AttentionPoint> longAttentionPoint;

    /**
     * 短期知识
     */
    List<AttentionPoint> shortAttentionPoint;

    public List<WorkRecord> getWorkRecords() {
        return workRecords;
    }

    public void setWorkRecords(List<WorkRecord> workRecords) {
        this.workRecords = workRecords;
    }

    public List<ShiftRecord> getShiftRecord() {
        return shiftRecord;
    }

    public void setShiftRecord(List<ShiftRecord> shiftRecord) {
        this.shiftRecord = shiftRecord;
    }

    public List<AttentionPoint> getLongAttentionPoint() {
        return longAttentionPoint;
    }

    public void setLongAttentionPoint(List<AttentionPoint> LongAttentionPoint) {
        this.longAttentionPoint = LongAttentionPoint;
    }

    public List<AttentionPoint> getShortAttentionPoint() {
        return shortAttentionPoint;
    }

    public void setShortAttentionPoint(List<AttentionPoint> shortAttentionPoint) {
        this.shortAttentionPoint = shortAttentionPoint;
    }
}
