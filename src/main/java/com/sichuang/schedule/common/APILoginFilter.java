package com.sichuang.schedule.common;

import com.alibaba.fastjson.JSON;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.service.IAccessRecordService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录过滤器
 *
 * @author qw
 */
public class APILoginFilter implements Filter {

    private IAccessRecordService accessRecordService;

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
        // 获得在下面代码中要用的request,response,session对象
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        // 获得用户请求的URI
        String path = servletRequest.getRequestURI();
        System.out.println(path);

        if (accessRecordService == null) {
            BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
            accessRecordService = (IAccessRecordService) factory.getBean("accessRecordService");
        }
        try {
            // 保存访问记录
            //accessRecordService.add(servletRequest);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        // 登陆页面无需过滤
        if (path.indexOf("/login") > -1) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        User userSession = BaseController.getUserSession(servletRequest);
        // 判断如果没有取到用户信息
        if (userSession == null) {
            // 返回未登录信息
            String msg = JSON.toJSONString(MyResponse.getNotLoginResponse(), MyResponse.features);
            // 返回未登录信息
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            servletResponse.getWriter().print(msg);
            return;
        }

        // 已经登陆,继续此次请求
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

}
