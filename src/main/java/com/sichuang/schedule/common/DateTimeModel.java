package com.sichuang.schedule.common;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by qw on 2016/5/12. email：512609950@qq.com
 */
public class DateTimeModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static String DATE_TYPE_1 = "yyyy/MM/dd";// 定义标准日期格式1
	public static String DATE_TYPE_2 = "yyyy-MM-dd";// 定义标准日期格式2
	public static String DATE_TYPE_3 = "yyyy/MM/dd HH:mm:ss";// 定义标准日期格式3，带有时分秒
	public static String DATE_TYPE_4 = "yyyy-MM-dd HH:mm:ss";// 定义标准日期格式3，带有时分秒
	public static String DATE_TYPE_5 = "yyyy/MM/dd HH:mm";// 定义标准日期格式3，带有时分
	public static String DATE_TYPE_6 = "yyyy-MM-dd HH:mm";// 定义标准日期格式3，带有时分
	public static String DATE_TYPE_7 = "yyyy/MM/dd HH";// 定义标准日期格式3，带有小时
	public static String DATE_TYPE_8 = "yyyy-MM-dd HH";// 定义标准日期格式3，带有小时
	public static String MONTH_TYPE_C = "yyyy年M月";
	public static String MONTH_TYPE_N = "yyyy-MM";
	public static String YEAR_TYPE_C = "yyyy年";

	public Date date;
	private Calendar calendar;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private int second;

	/**
	 * 时间戳转换其他时间构造方法
	 * 
	 * @param milliseconds
	 *            毫秒时间戳，长度 13位
	 * @throws ParseException
	 */
	public DateTimeModel(long milliseconds) throws ParseException {
		this.date = new Date(milliseconds);
	}

	/**
	 * 时间字符串转其他时间构造方法
	 * 
	 * @param timeString
	 *            时间字符串 如：2016/5/12
	 * @param dateFormat
	 *            时间格式 如：yyyy/MM/dd
	 * @throws ParseException
	 */
	public DateTimeModel(String timeString, String dateFormat) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);// 小写的mm表示的是分钟
		this.date = sdf.parse(timeString);

	}

	/**
	 * date 转其他时间
	 * 
	 * @param date
	 */
	public DateTimeModel(Date date) {

		if (date == null) {
			date = new Date();
		}
		this.date = date;

	}

	/**
	 * 默认获取系统当前时间
	 */
	public DateTimeModel() {
		this.date = new Date();
	}

	/**
	 * 获取时间字符串，如：2016/5/12
	 * 
	 * @param dateTYpe
	 *            输出的时间格式 如yyyy/MM/dd
	 * @return
	 */
	public String getDataString(String dateTYpe) {

		SimpleDateFormat df = new SimpleDateFormat(dateTYpe);
		return df.format(this.date);
	}

	public String getYYMMDDDataString() {

		String string = getDataString(DATE_TYPE_1);
		return string.replace("/", "");
	}

	public String getYYMMDDhhmmssDataString() {

		String string = getDataString(DATE_TYPE_3);
		return string.replace("/", "").replace(" ", "").replace(":", "");
	}

	/**
	 * 获取毫秒时间戳 长度13位
	 * 
	 * @return
	 */
	public long getTimestamp() {
		return this.date.getTime();
	}

	public Date getDate() {
		return this.date;
	}

	public Calendar getCalendar() {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	public int getYear() {
		return getCalendar().get(Calendar.YEAR);
	}

	public int getMonth() {
		return getCalendar().get(Calendar.MONTH);
	}

	public int getDay() {
		return getCalendar().get(Calendar.DAY_OF_MONTH);
	}

	public int getHour() {
		return getCalendar().get(Calendar.HOUR) + 1;
	}

	public int getHourOfDay() {
		return getCalendar().get(Calendar.HOUR_OF_DAY);
	}

	public int getMinute() {
		return getCalendar().get(Calendar.MINUTE);
	}

	public int getSecond() {
		return getCalendar().get(Calendar.SECOND);
	}

	public Date setTime(int hour, int minute, int second){
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		return calendar.getTime();
	}

	/**
	 * 计算两个时间间隔天数,举例小于24小时返回1，25小时返回2
	 * 
	 * @param fDate
	 *            小的时间
	 * @param oDate
	 *            大的时间
	 * @return
	 */
	public static int getIntervalDays(Date fDate, Date oDate) {
		if (null == fDate || null == oDate) {
			return -1;
		}
		long intervalMilli = oDate.getTime() - fDate.getTime();
		return (int) (intervalMilli / (24 * 60 * 60 * 1000)) + 1;
	}

	/*
	 * 毫秒转化时分秒毫秒
	 */
	public static String formatTime(Long ms) {
		Integer ss = 1000;
		Integer mi = ss * 60;
		Integer hh = mi * 60;
		Integer dd = hh * 24;

		Long day = ms / dd;
		Long hour = (ms - day * dd) / hh;
		Long minute = (ms - day * dd - hour * hh) / mi;
		Long second = (ms - day * dd - hour * hh - minute * mi) / ss;
		Long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;

		StringBuffer sb = new StringBuffer();
		if (day > 0) {
			sb.append(day + "天");
		}
		if (hour > 0) {
			sb.append(hour + "小时");
		}
		if (minute > 0) {
			sb.append(minute + "分");
		}
		if (second > 0) {
			if (milliSecond > 0) {
				sb.append((second + 1) + "秒");
			} else {
				sb.append(second + "秒");
			}
		}
		return sb.toString();
	}

	/**
	 * 获取时间字符串，如：2016/5/12
	 * 
	 * @param dateType 输出的时间格式 如yyyy/MM/dd
	 * @return
	 */
	public static String getDateString(String dateType, Date date) {

		if(dateType == null){
			dateType = DATE_TYPE_4;
		}
		SimpleDateFormat df = new SimpleDateFormat(dateType);
		return df.format(date);
	}

	/**
	 * 获得上个月第一天
	 * 
	 * @return
	 */
	public Date getFirstDayOfLastMonth() {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, this.getYear());
		// 设置月份
		cal.set(Calendar.MONTH, this.getMonth() - 1);
		// 获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获得一年第一天
	 * 
	 * @param year
	 * @return
	 */
	public static Date getFirstDayOfYear(int year) {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, year);
		// 设置月份
		cal.set(Calendar.MONTH, 0);
		// 获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获取一年的最后一天
	 * 
	 * @param year
	 * @return
	 */
	public static Date getLastDayOfYear(int year) {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, year);
		// 设置月份
		cal.set(Calendar.MONTH, 11);
		// 获取某月最小天数
		int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, lastDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获得上一年该月第一天
	 * 
	 * @return
	 */
	public Date getFirstDayOfMonthLastYear() {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, this.getYear() - 1);
		// 设置月份
		cal.set(Calendar.MONTH, this.getMonth());
		// 获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获得该月第一天
	 * 
	 * @return
	 */
	public Date getFirstDayOfMonth() {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, this.getYear());
		// 设置月份
		cal.set(Calendar.MONTH, this.getMonth());
		// 获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获得该月最后一天
	 * 
	 * @return
	 */
	public Date getLastDayOfMonth() {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, this.getYear());
		// 设置月份
		cal.set(Calendar.MONTH, this.getMonth());
		// 获取某月最大天数
		int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最大天数
		cal.set(Calendar.DAY_OF_MONTH, lastDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获得该月第一天
	 * 
	 * @return
	 */
	public Date getFirstDayOfNextMonth() {
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, this.getYear());
		// 设置月份
		cal.set(Calendar.MONTH, this.getMonth() + 1);
		// 获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		// 设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);

		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TYPE_2);
		DateTimeModel firstDayOfMonth;
		try {
			firstDayOfMonth = new DateTimeModel(sdf.format(cal.getTime()), DATE_TYPE_2);
			return firstDayOfMonth.getDate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

    /**
     * 获取当前日期23:59:59
     * @param date
     * @return
     */
	public static Date getLastTimeOfDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		return c.getTime();
	}

    /**
     * 获取当前日期00:00:00
     * @param date
     * @return
     */
	public static Date getFirstTimeOfDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY,0);
		c.set(Calendar.MINUTE,0);
		c.set(Calendar.SECOND, 0);
		return c.getTime();
	}

	/**
	 * 获取前一天
	 * @param date
	 * @return
	 */
	public static Date getPreviousDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) -1);
		return c.getTime();
	}

	public static void main(String[] args) throws ParseException {
		DateTimeModel dateTimeModel = new DateTimeModel(new Date());
//		Date nextMonthFirstDate = dateTimeModel.getFirstDayOfNextMonth();
		// Date nextMonthFirstDate = getFirstDayOfYear(2017);
		Date date = dateTimeModel.setTime(15,30,0);

		System.out.println(DateTimeModel.getDateString(DATE_TYPE_4, date));
		System.out.println(DateTimeModel.getDateString(DATE_TYPE_4, DateTimeModel.getFirstTimeOfDay(date)));
		System.out.println(DateTimeModel.getDateString(DATE_TYPE_4, DateTimeModel.getLastTimeOfDay(date)));
		// System.out.println(dateTimeModel.getMinute());
	}

}
