package com.sichuang.schedule.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sichuang.schedule.common.ExcelException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExcelUtils {
    private static final String EXTENSION_XLS = "xls";
    private static final String EXTENSION_XLSX = "xlsx";

    /***
     * <pre>
     * 取得Workbook对象(xls和xlsx对象不同,不过都是Workbook的实现类)
     *   xls:HSSFWorkbook
     *   xlsx：XSSFWorkbook
     * @param filePath
     * @return
     * @throws IOException
     * </pre>
     */
    public static Workbook getWorkbook(String filePath) throws IOException {
        Workbook workbook = null;
        InputStream is = new FileInputStream(filePath);
        if (filePath.endsWith(EXTENSION_XLS)) {
            workbook = new HSSFWorkbook(is);
        } else if (filePath.endsWith(EXTENSION_XLSX)) {
            workbook = new XSSFWorkbook(is);
        }
        return workbook;
    }
//	/**
//	 * 把一个excel中的cellstyletable复制到另一个excel，这里会报错，不能用这种方法，不明白呀？？？？？
//	 * @param fromBook
//	 * @param toBook
//	 */
//	public static void copyBookCellStyle(XSSFWorkbook fromBook,XSSFWorkbook toBook){
//		for(short i=0;i<fromBook.getNumCellStyles();i++){
//			XSSFCellStyle fromStyle=fromBook.getCellStyleAt(i);
//			XSSFCellStyle toStyle=toBook.getCellStyleAt(i);
//			if(toStyle==null){
//				toStyle=toBook.createCellStyle();
//			}
//			copyCellStyle(fromStyle,toStyle);
//		}
//	}

    /**
     * 复制一个单元格样式到目的单元格样式
     *
     * @param fromStyle
     * @param toStyle
     */
    public static XSSFCellStyle copyCellStyle(XSSFCellStyle fromStyle,
                                              XSSFCellStyle toStyle) {
        toStyle.setAlignment(fromStyle.getAlignment());
        //边框和边框颜色
        toStyle.setBorderBottom(fromStyle.getBorderBottom());
        toStyle.setBorderLeft(fromStyle.getBorderLeft());
        toStyle.setBorderRight(fromStyle.getBorderRight());
        toStyle.setBorderTop(fromStyle.getBorderTop());
        toStyle.setTopBorderColor(fromStyle.getTopBorderColor());
        toStyle.setBottomBorderColor(fromStyle.getBottomBorderColor());
        toStyle.setRightBorderColor(fromStyle.getRightBorderColor());
        toStyle.setLeftBorderColor(fromStyle.getLeftBorderColor());

        //背景和前景
        toStyle.setFillBackgroundColor(fromStyle.getFillBackgroundColor());
        toStyle.setFillForegroundColor(fromStyle.getFillForegroundColor());

        toStyle.setDataFormat(fromStyle.getDataFormat());
        toStyle.setFillPattern(fromStyle.getFillPattern());
        toStyle.setFont(fromStyle.getFont());
        toStyle.setHidden(fromStyle.getHidden());
        toStyle.setIndention(fromStyle.getIndention());//首行缩进
        toStyle.setLocked(fromStyle.getLocked());
        toStyle.setRotation(fromStyle.getRotation());//旋转
        toStyle.setVerticalAlignment(fromStyle.getVerticalAlignment());
        toStyle.setWrapText(fromStyle.getWrapText());
        return toStyle;
    }

    /**
     * Sheet复制
     *
     * @param fromSheet
     * @param toSheet
     * @param copyValueFlag
     */
    public static void copySheet(XSSFWorkbook wb, XSSFSheet fromSheet, XSSFSheet toSheet,
                                 boolean copyValueFlag) {
        //合并区域处理
        mergerRegion(fromSheet, toSheet);
        for (Iterator rowIt = fromSheet.rowIterator(); rowIt.hasNext(); ) {
            XSSFRow tmpRow = (XSSFRow) rowIt.next();
            XSSFRow newRow = toSheet.createRow(tmpRow.getRowNum());
            //行复制
            copyRow(wb, tmpRow, newRow, copyValueFlag);
        }
        for (int i = 0; i < 11; i++) {
            toSheet.setColumnWidth(i, fromSheet.getColumnWidth(i));
        }
    }

    /**
     * 行复制功能
     *
     * @param fromRow
     * @param toRow
     */
    public static void copyRow(XSSFWorkbook wb, XSSFRow fromRow, XSSFRow toRow, boolean copyValueFlag) {
        toRow.setHeight(fromRow.getHeight());
        for (Iterator cellIt = fromRow.cellIterator(); cellIt.hasNext(); ) {
            XSSFCell tmpCell = (XSSFCell) cellIt.next();
            XSSFCell newCell = toRow.createCell(tmpCell.getColumnIndex());
            copyCell(wb, tmpCell, newCell, copyValueFlag);
        }
    }

    /**
     * 复制原有sheet的合并单元格到新创建的sheet
     */
    public static void mergerRegion(XSSFSheet fromSheet, XSSFSheet toSheet) {
        int sheetMergerCount = fromSheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergerCount; i++) {
//            Region mergedRegionAt = fromSheet.getMergedRegionAt(i);
//            toSheet.addMergedRegion(mergedRegionAt);
            CellRangeAddress mergedRegionAt = fromSheet.getMergedRegion(i);
            toSheet.addMergedRegion(mergedRegionAt);
        }
    }

    /**
     * 复制单元格
     *
     * @param srcCell
     * @param distCell
     * @param copyValueFlag true则连同cell的内容一起复制
     */
    public static void copyCell(XSSFWorkbook wb, XSSFCell srcCell, XSSFCell distCell,
                                boolean copyValueFlag) {
        XSSFCellStyle newstyle = wb.createCellStyle();
        distCell.setCellStyle(srcCell.getCellStyle());
//        distCell.setEncoding(srcCell.getEncoding());
        //样式
//        distCell.setCellStyle(newstyle);
        //评论
        if (srcCell.getCellComment() != null) {
            distCell.setCellComment(srcCell.getCellComment());
        }
        // 不同数据类型处理
        int srcCellType = srcCell.getCellType();
        distCell.setCellType(srcCellType);
        if (copyValueFlag) {
            if (srcCellType == XSSFCell.CELL_TYPE_NUMERIC) {
//                if (XSSFDateUtil.isCellDateFormatted(srcCell)) {
//                    distCell.setCellValue(srcCell.getDateCellValue());
//                } else {
                distCell.setCellValue(srcCell.getNumericCellValue());
//                }
            } else if (srcCellType == XSSFCell.CELL_TYPE_STRING) {
                distCell.setCellValue(srcCell.getRichStringCellValue());
            } else if (srcCellType == XSSFCell.CELL_TYPE_BLANK) {
                // nothing21
            } else if (srcCellType == XSSFCell.CELL_TYPE_BOOLEAN) {
                distCell.setCellValue(srcCell.getBooleanCellValue());
            } else if (srcCellType == XSSFCell.CELL_TYPE_ERROR) {
                distCell.setCellErrorValue(srcCell.getErrorCellValue());
            } else if (srcCellType == XSSFCell.CELL_TYPE_FORMULA) {
                distCell.setCellFormula(srcCell.getCellFormula());
            } else { // nothing29
            }
        }
    }


    /**
     * @param list      数据源
     * @param fieldMap  类的英文属性和Excel中的中文列名的对应关系
     *                  如果需要的是引用对象的属性，则英文属性使用类似于EL表达式的格式
     *                  如：list中存放的都是student，student中又有college属性，而我们需要学院名称，则可以这样写
     *                  fieldMap.put("college.collegeName","学院名称")
     * @param sheetName 工作表的名称
     * @param sheetSize 每个工作表中记录的最大个数
     * @param out       导出流
     * @throws ExcelException
     * @MethodName : listToExcel
     * @Description : 导出Excel（可以导出到本地文件系统，也可以导出到浏览器，可自定义工作表大小）
     */
    public static <T> void listToExcel(List<T> list, LinkedHashMap<String, String> fieldMap, String sheetName, int sheetSize, OutputStream out) throws ExcelException {
        if (list.size() == 0 || list == null) {
            throw new ExcelException("数据源中没有任何数据");
        }
        if (sheetSize > 65535 || sheetSize < 1) {
            sheetSize = 65535;
        }
        //创建工作簿并发送到OutputStream指定的地方
        XSSFWorkbook wwb;
        try {
            wwb = new XSSFWorkbook();

            //因为2003的Excel一个工作表最多可以有65536条记录，除去列头剩下65535条
            //所以如果记录太多，需要放到多个工作表中，其实就是个分页的过程
            //1.计算一共有多少个工作表
            double sheetNum = Math.ceil(list.size() / new Integer(sheetSize).doubleValue());
            if (sheetName == null) {
                sheetName = "Sheet";
            }
            //2.创建相应的工作表，并向其中填充数据
            for (int i = 0; i < sheetNum; i++) {
                //如果只有一个工作表的情况
                if (1 == sheetNum) {
                    XSSFSheet sheet = wwb.createSheet(sheetName);
                    fillSheet(sheet, list, fieldMap, 0, list.size() - 1);
                    //有多个工作表的情况
                } else {
                    XSSFSheet sheet = wwb.createSheet(sheetName + (i + 1));
                    //获取开始索引和结束索引
                    int firstIndex = i * sheetSize;
                    int lastIndex = (i + 1) * sheetSize - 1 > list.size() - 1 ? list.size() - 1 : (i + 1) * sheetSize - 1;
                    //填充工作表
                    fillSheet(sheet, list, fieldMap, firstIndex, lastIndex);
                }
            }
            wwb.write(out);
            wwb.close();

        } catch (Exception e) {
            e.printStackTrace();
            //如果是ExcelException，则直接抛出
            if (e instanceof ExcelException) {
                throw (ExcelException) e;
                //否则将其它异常包装成ExcelException再抛出
            } else {
                throw new ExcelException("导出Excel失败");
            }
        }
    }

    /**
     * @param list     数据源
     * @param fieldMap 类的英文属性和Excel中的中文列名的对应关系
     * @param out      导出流
     * @throws ExcelException
     * @MethodName : listToExcel
     * @Description : 导出Excel（可以导出到本地文件系统，也可以导出到浏览器，工作表大小为2003支持的最大值）
     */
    public static <T> void listToExcel(
            List<T> list,
            LinkedHashMap<String, String> fieldMap,
            String sheetName,
            OutputStream out
    ) throws ExcelException {
        listToExcel(list, fieldMap, sheetName, 65535, out);
    }

    /**
     * @param list      数据源
     * @param fieldMap  类的英文属性和Excel中的中文列名的对应关系
     * @param sheetSize 每个工作表中记录的最大个数
     * @param response  使用response可以导出到浏览器
     * @throws ExcelException
     * @MethodName : listToExcel
     * @Description : 导出Excel（导出到浏览器，可以自定义工作表的大小）
     */
    public static <T> void listToExcel(List<T> list, LinkedHashMap<String, String> fieldMap, String sheetName, int sheetSize, HttpServletResponse response) throws ExcelException {
        //设置默认文件名为当前时间：年月日时分秒
        String time = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()).toString();
        String fileName = sheetName + time + ".xlsx";
        try {
            //设置response头信息
            response.reset();
            response.setContentType("application/vnd.ms-excel");        //改成输出excel文件
            response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
            //创建工作簿并发送到浏览器
            OutputStream out = response.getOutputStream();
            listToExcel(list, fieldMap, sheetName, sheetSize, out);
        } catch (Exception e) {
            e.printStackTrace();
            //如果是ExcelException，则直接抛出
            if (e instanceof ExcelException) {
                throw (ExcelException) e;
                //否则将其它异常包装成ExcelException再抛出
            } else {
                throw new ExcelException("导出Excel失败");
            }
        }
    }

    /**
     * @param list     数据源
     * @param fieldMap 类的英文属性和Excel中的中文列名的对应关系
     * @param response 使用response可以导出到浏览器
     * @throws ExcelException
     * @MethodName : listToExcel
     * @Description : 导出Excel（导出到浏览器，工作表的大小是2003支持的最大值）
     */
    public static <T> void listToExcel(List<T> list, LinkedHashMap<String, String> fieldMap, String sheetName, HttpServletResponse response) throws ExcelException {
        listToExcel(list, fieldMap, sheetName, 65535, response);
    }

    /**
     * @param in           ：承载着Excel的输入流
     * @param entityClass  ：List中对象的类型（Excel中的每一行都要转化为该类型的对象）
     * @param fieldMap     ：Excel中的中文列头和类的英文属性的对应关系Map
     * @param uniqueFields ：指定业务主键组合（即复合主键），这些列的组合不能重复
     * @return ：List
     * @throws ExcelException
     * @MethodName : excelToList
     * @Description : 将Excel转化为List
     */
    public static <T> List<T> excelToList(InputStream in, String sheetName, Class<T> entityClass, LinkedHashMap<String, String> fieldMap, String[] uniqueFields) throws ExcelException {
        //定义要返回的list
        List<T> resultList = new ArrayList<T>();
        try {
            //根据Excel数据源创建WorkBook
            XSSFWorkbook wb = new XSSFWorkbook(in);
            //获取工作表
            XSSFSheet sheet = wb.getSheet(sheetName);
            //获取工作表的有效行数
            int realRows = 0;
            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                int nullCols = 0;
                for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {
                    XSSFCell currentCell = sheet.getRow(i).getCell(j);
                    if (currentCell == null || "".equals(currentCell.getStringCellValue())) {
                        nullCols++;
                    }
                }
                if (nullCols == sheet.getRow(0).getLastCellNum()) {
                    break;
                } else {
                    realRows++;
                }
            }


            //如果Excel中没有数据则提示错误
            if (realRows <= 1) {
                throw new ExcelException("Excel文件中没有任何数据");
            }
            XSSFRow firstRow = sheet.getRow(0);
            String[] excelFieldNames = new String[firstRow.getLastCellNum()];
            //获取Excel中的列名
            for (int i = 0; i < firstRow.getLastCellNum(); i++) {
                excelFieldNames[i] = firstRow.getCell(i).getStringCellValue().trim();
            }
            //判断需要的字段在Excel中是否都存在
            boolean isExist = true;
            List<String> excelFieldList = Arrays.asList(excelFieldNames);
            for (String cnName : fieldMap.keySet()) {
                if (!excelFieldList.contains(cnName)) {
                    isExist = false;
                    break;
                }
            }
            //如果有列名不存在，则抛出异常，提示错误
            if (!isExist) {
                throw new ExcelException("Excel中缺少必要的字段，或字段名称有误");
            }
            //将列名和列号放入Map中,这样通过列名就可以拿到列号
            LinkedHashMap<String, Integer> colMap = new LinkedHashMap<String, Integer>();
            for (int i = 0; i < excelFieldNames.length; i++) {
                colMap.put(excelFieldNames[i], i);
            }
//            //判断是否有重复行
//            //1.获取uniqueFields指定的列
//            XSSFCell[][] uniqueCells = new XSSFCell[uniqueFields.length][];
//            for (int i = 0; i < uniqueFields.length; i++) {
//                int col = colMap.get(uniqueFields[i]);
//                uniqueCells[i] = sheet.getRow(0).getCell(col);
//            }
//
//            //2.从指定列中寻找重复行
//            for (int i = 1; i < realRows; i++) {
//                int nullCols = 0;
//                for (int j = 0; j < uniqueFields.length; j++) {
//                    String currentContent = uniqueCells[j][i].getContents();
//                    Cell sameCell = sheet.findCell(currentContent,
//                            uniqueCells[j][i].getColumn(),
//                            uniqueCells[j][i].getRow() + 1,
//                            uniqueCells[j][i].getColumn(),
//                            uniqueCells[j][realRows - 1].getRow(),
//                            true);
//                    if (sameCell != null) {
//                        nullCols++;
//                    }
//                }
//
//                if (nullCols == uniqueFields.length) {
//                    throw new ExcelException("Excel中有重复行，请检查");
//                }
//            }
            //将sheet转换为list
            for (int i = 1; i < realRows; i++) {
                //新建要转换的对象
                T entity = entityClass.newInstance();

                //给对象中的字段赋值
                for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
                    //获取中文字段名
                    String cnNormalName = entry.getKey();
                    //获取英文字段名
                    String enNormalName = entry.getValue();
                    //根据中文字段名获取列号
                    int col = colMap.get(cnNormalName);

                    //获取当前单元格中的内容
                    String content = sheet.getRow(i).getCell(col).getStringCellValue().trim();
                    //给对象赋值
                    setFieldValueByName(enNormalName, content, entity);
                }

                resultList.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //如果是ExcelException，则直接抛出
            if (e instanceof ExcelException) {
                throw (ExcelException) e;
                //否则将其它异常包装成ExcelException再抛出
            } else {
                e.printStackTrace();
                throw new ExcelException("导入Excel失败");
            }
        }
        return resultList;
    }





    /*<-------------------------辅助的私有方法----------------------------------------------->*/

    /**
     * @param fieldName 字段名
     * @param o         对象
     * @return 字段值
     * @MethodName : getFieldValueByName
     * @Description : 根据字段名获取字段值
     */
    private static Object getFieldValueByName(String fieldName, Object o) throws Exception {
        Object value = null;
        Field field = getFieldByName(fieldName, o.getClass());
        if (field != null) {
            field.setAccessible(true);
            value = field.get(o);
        } else {
            throw new ExcelException(o.getClass().getSimpleName() + "类不存在字段名 " + fieldName);
        }

        return value;
    }

    /**
     * @param fieldName 字段名
     * @param clazz     包含该字段的类
     * @return 字段
     * @MethodName : getFieldByName
     * @Description : 根据字段名获取字段
     */
    private static Field getFieldByName(String fieldName, Class<?> clazz) {
        //拿到本类的所有字段
        Field[] selfFields = clazz.getDeclaredFields();
        //如果本类中存在该字段，则返回
        for (Field field : selfFields) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        //否则，查看父类中是否存在此字段，如果有则返回
        Class<?> superClazz = clazz.getSuperclass();
        if (superClazz != null && superClazz != Object.class) {
            return getFieldByName(fieldName, superClazz);
        }
        //如果本类和父类都没有，则返回空
        return null;
    }


    /**
     * @param fieldNameSequence 带路径的属性名或简单属性名
     * @param o                 对象
     * @return 属性值
     * @throws Exception
     * @MethodName : getFieldValueByNameSequence
     * @Description :
     * 根据带路径或不带路径的属性名获取属性值
     * 即接受简单属性名，如userName等，又接受带路径的属性名，如student.department.name等
     */
    private static Object getFieldValueByNameSequence(String fieldNameSequence, Object o) throws Exception {
        Object value = null;
        //将fieldNameSequence进行拆分
        String[] attributes = fieldNameSequence.split("\\.");
        if (attributes.length == 1) {
            value = getFieldValueByName(fieldNameSequence, o);
        } else {
            //根据属性名获取属性对象
            Object fieldObj = getFieldValueByName(attributes[0], o);
            String subFieldNameSequence = fieldNameSequence.substring(fieldNameSequence.indexOf(".") + 1);
            value = getFieldValueByNameSequence(subFieldNameSequence, fieldObj);
        }
        return value;
    }


    /**
     * @param fieldName  字段名
     * @param fieldValue 字段值
     * @param o          对象
     * @MethodName : setFieldValueByName
     * @Description : 根据字段名给对象的字段赋值
     */
    private static void setFieldValueByName(String fieldName, Object fieldValue, Object o) throws Exception {
        Field field = getFieldByName(fieldName, o.getClass());
        if (field != null) {
            field.setAccessible(true);
            //获取字段类型
            Class<?> fieldType = field.getType();
            //根据字段类型给字段赋值
            if (String.class == fieldType) {
                field.set(o, String.valueOf(fieldValue));
            } else if ((Integer.TYPE == fieldType)
                    || (Integer.class == fieldType)) {
                field.set(o, Integer.parseInt(fieldValue.toString()));
            } else if ((Long.TYPE == fieldType)
                    || (Long.class == fieldType)) {
                field.set(o, Long.valueOf(fieldValue.toString()));
            } else if ((Float.TYPE == fieldType)
                    || (Float.class == fieldType)) {
                field.set(o, Float.valueOf(fieldValue.toString()));
            } else if ((Short.TYPE == fieldType)
                    || (Short.class == fieldType)) {
                field.set(o, Short.valueOf(fieldValue.toString()));
            } else if ((Double.TYPE == fieldType)
                    || (Double.class == fieldType)) {
                field.set(o, Double.valueOf(fieldValue.toString()));
            } else if (Character.TYPE == fieldType) {
                if ((fieldValue != null) && (fieldValue.toString().length() > 0)) {
                    field.set(o, Character
                            .valueOf(fieldValue.toString().charAt(0)));
                }
            } else if (Date.class == fieldType) {
                field.set(o, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fieldValue.toString()));
            } else {
                field.set(o, fieldValue);
            }
        } else {
            throw new ExcelException(o.getClass().getSimpleName() + "类不存在字段名 " + fieldName);
        }
    }


    /**
     * @param ws
     * @MethodName : setColumnAutoSize
     * @Description : 设置工作表自动列宽和首行加粗
     */
    private static void setColumnAutoSize(XSSFSheet ws, int extraWith) {
        //获取本列的最宽单元格的宽度
        XSSFRow firstRow = ws.getRow(0);
        for (int i = 0; i < firstRow.getLastCellNum(); i++) {
            int colWith = 0;
            for (int j = 0; j < ws.getLastRowNum(); j++) {
                XSSFRow row = ws.getRow(j);
                String content = row.getCell(i).getStringCellValue();
                int cellWith = content.getBytes().length;
                if (colWith < cellWith) {
                    colWith = cellWith;
                }
            }
            //设置宽度自适应
            ws.autoSizeColumn(i, true);
            CellStyle cellStyle = ws.getColumnStyle(i);
            cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
            ws.setDefaultColumnStyle(i, cellStyle);//垂直居中  );
        }

    }

    /**
     * @param sheet      工作表
     * @param list       数据源
     * @param fieldMap   中英文字段对应关系的Map
     * @param firstIndex 开始索引
     * @param lastIndex  结束索引
     * @MethodName : fillSheet
     * @Description : 向工作表中填充数据
     */
    private static <T> void fillSheet(XSSFSheet sheet, List<T> list, LinkedHashMap<String, String> fieldMap, int firstIndex, int lastIndex) throws Exception {

        //定义存放英文字段名和中文字段名的数组
        String[] enFields = new String[fieldMap.size()];
        String[] cnFields = new String[fieldMap.size()];

        //填充数组
        int count = 0;
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            enFields[count] = entry.getKey();
            cnFields[count] = entry.getValue();
            count++;
        }
        //填充表头
        XSSFRow row = sheet.createRow(0);
        for (int i = 0; i < cnFields.length; i++) {
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(cnFields[i]);
        }

        //填充内容
        int rowNo = 1;
        for (int index = firstIndex; index <= lastIndex; index++) {
            //获取单个对象
            T item = list.get(index);
            row = sheet.createRow(rowNo);
            for (int i = 0; i < enFields.length; i++) {
                String enField = enFields[i];
                String typeMap = null;
                if (enField.contains("#")) {
                    String[] strings = enField.split("#");
                    enField = strings[0];
                    typeMap = strings[1];
                }
                Object objValue;
                if (item instanceof JSONObject) {
                    objValue = ((JSONObject) item).get(enField);
                } else {
                    objValue = getFieldValueByNameSequence(enField, item);
                }
                Object objValue1 = null;
                if (objValue != null && typeMap != null) {
                    try {
                        //判断是否是枚举json
                        objValue1 = ((Map<Integer, String>)JSON.parse(typeMap)).get(objValue.toString());
                    }catch (Exception  e){
                        try {
                            //判断是否为时间格式
                            DateTimeModel dateTimeModel = new DateTimeModel((Date)objValue);
                            objValue1 = dateTimeModel.getDataString(typeMap);
                        }catch (Exception  e1){
                            objValue1 = objValue;
                        }
                    }
                } else {
                    objValue1 = objValue;
                }
                XSSFCell cell = row.createCell(i);
                if (objValue1 == null){
                    cell.setCellValue("");
                }else {
                    cell.setCellValue(objValue1.toString());
                }
            }

            rowNo++;
        }
        //设置自动列宽
        setColumnAutoSize(sheet, 5);
    }

//    public Map<String, Object> exportExcel(WorkItem workItem, ShiftRecord shiftRecord, Date shiftTimeEnd, HttpServletResponse response) {
//
//        Map<String, Object> map = getWorkItem(workItem, shiftRecord, shiftTimeEnd, new Page(1, 60000));
//        List<JSONObject> workItems = (List<JSONObject>) MyResponse.getDataByRootMap(map);
//
//        LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String, String>();
//        fieldMap.put("shiftTime", "交接时间");
//        fieldMap.put("shiftType#" + JSON.toJSONString(EnumManage.ShiftTypeEnum.getList()), "值班班次");
//        fieldMap.put("dutyOfficer", "值班负责人");
//        fieldMap.put("manager", "助理负责人");
//
//        fieldMap.put("workContent", "交接内容");
//        fieldMap.put("pointType#" + JSON.toJSONString(EnumManage.PointTypeEnum.getList()), "类型");
//        fieldMap.put("handleTime", "处理时间");
//        fieldMap.put("handlerName", "处理人");
//        fieldMap.put("handleInformation", "完成情况");
//        fieldMap.put("itemTypeNames", "分类");
////        File file = new File("C:\\Users\\qw\\Desktop\\customer.xls");
////        FileOutputStream fos = null;
////        try {
////            if (!file.exists()) {
////                //file.mkdirs();
////                file.createNewFile();
////
////            }
////            fos = new FileOutputStream(file);
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//        try {
////            ExcelUtil.listToExcel(workItems, fieldMap, "2018", fos);
//            ExcelUtil.listToExcel(workItems, fieldMap, null, response);
////            fos.close();
//        } catch (ExcelException e) {
//            e.printStackTrace();
//            return MyResponse.getDefaultErrorResponse();
//        }
//        return MyResponse.getDefaultSuccessResponse();
//    }
}