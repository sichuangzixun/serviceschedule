package com.sichuang.schedule.common;

import com.sichuang.schedule.controller.BaseController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.*;

public class FileUtils {


	private static String url;
	//文件路径：公司的：upload/

    /**
     * 保存文件
     * @param fileName 文件名
     * @param segments 文件夹绝对路径
     * @param filedata 文件
     * @return
     */
	public static String saveFile(String fileName, String segments, MultipartFile filedata) {
        // TODO Auto-generated method stub
        // 根据配置文件获取服务器图片存放路径
        url = segments;

        /* 构建文件目录 */
        File fileDir = new File(url);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        try {
            FileOutputStream out = new FileOutputStream(url + "/" + fileName + ".jpeg");
            // 写入文件
            out.write(filedata.getBytes());
            out.flush();
            out.close();
            //保存成功后返回图片路径
            String url1 = "upload/" + segments + (segments.isEmpty()? "" : "/") + fileName + ".jpeg";
            return url1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }
	

	/**
	 * 保存文件
	 * @param pathUrl 文件夹绝对路径
	 * @param filedata 文件
	 * @param fileName 文件名
	 * @return
	 */
	public static String saveFile(String pathUrl, MultipartFile filedata, String fileName) {
        // TODO Auto-generated method stub
        // 根据配置文件获取服务器图片存放路径
        url = pathUrl;

        /* 构建文件目录 */
        File fileDir = new File(url);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        try {
            FileOutputStream out = new FileOutputStream(url +fileName);
            // 写入文件
            out.write(filedata.getBytes());
            out.flush();
            out.close();
            //保存成功后返回图片路径
            return pathUrl + fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
	
	/**
	 * web上传文件
	 * @param request
	 * @param fileName 文件名
	 * @param pathUrl 文件夹绝对路径
	 * @return (原文件名，文件保存路径)
	 */
	public static Map<String, Object> fileUpload(HttpServletRequest request, String fileName, String pathUrl) {  
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext()); 
        multipartResolver.setDefaultEncoding("UTF-8");
        ArrayList<Object> fileList = new ArrayList<>();
        int index = 0;  
        // 判断request是否有文件上传  
        if (multipartResolver.isMultipart(request)) {

            String path = fileName.substring(0,fileName.lastIndexOf("/"));
        	// 根据配置文件获取服务器图片存放路径
            url = pathUrl + path;

            /* 构建文件目录 */
            File fileDir = new File(url);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
  
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
            MultiValueMap<String,MultipartFile> multiFileMap = multiRequest.getMultiFileMap();
            List<MultipartFile> fileSet = new LinkedList<>();
            for(Map.Entry<String, List<MultipartFile>> temp : multiFileMap.entrySet()){
                fileSet = temp.getValue();
            }

            for(MultipartFile file : fileSet){
//            Iterator<String> iter = multiRequest.getFileNames();
//
//            while (iter.hasNext()) {
//                MultipartFile file = multiRequest.getFile(iter.next());
                if (file != null && file.getSize() > 0) {  
                    String oldFileName = file.getOriginalFilename();  
                    String suffixName = oldFileName.substring(oldFileName.lastIndexOf("."));
                    if(suffixName.trim().equals(".exe")){
                    	continue;
                    }
                    String newFileName = fileName + "_" + index + suffixName;  
  
                    String newFile = Paths.get(pathUrl, newFileName).toString();
                    try {  
                        // 将上传文件写到服务器上指定的文件                      	
                        file.transferTo(new File(newFile));
                        Map<String, Object> fileInfo = new HashMap<>();
                        fileInfo.put("oldFileName", oldFileName);
                        fileInfo.put("newFilePath", newFileName);
                        fileList.add(fileInfo);
                    } catch (Exception e) {  
                        e.printStackTrace();  
                    }  
  
                    
                    index++;  
                }  
            }  
        }  
   
        if(index == 0){
        	return MyResponse.getDefaultSuccessResponse();          	
        }
        
        if(fileList.isEmpty() == true){
        	return MyResponse.getResponse(false, "上传文件失败！");    
        }
        
        if(fileList.size() == index){
    		return MyResponse.getResponse(true, fileList);
    	}
        
        return MyResponse.getErrorResponse();
    }


    /**
     * 删除文件
     * @param pathUrl 文件绝对路径
     * @return
     */
    public static Map<String, Object> deleteFile(String pathUrl){
        File file = new File(pathUrl);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                return MyResponse.getDefaultSuccessResponse();
            } else {
                return MyResponse.getErrorResponse("删除失败！");
            }
        } else {
            return MyResponse.getErrorResponse("文件不存在！");
        }
    }

}
