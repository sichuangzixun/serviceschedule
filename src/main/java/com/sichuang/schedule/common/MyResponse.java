package com.sichuang.schedule.common;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sichuang.schedule.model.entity.Page;

import java.util.HashMap;
import java.util.Map;

public class MyResponse {

    static SerializerFeature[] features = {SerializerFeature.WriteMapNullValue};
    private static int errorCode = 40001;
    private static int successCode = 40000;
    private static int needLoginCode = -1;
    private static int defaultSuccessCode = 40002;

    /**
     * @param msg
     * @param code -1未登陆，40000请求成功,40001请求不成功
     * @param data
     * @return接口返回数据格式
     */
    public static Map<String, Object> getResponse(int code, String msg, Object data) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("msg", msg);
        map.put("data", data);
        return map;
    }

    public static Map<String, Object> getResponse(boolean code, String msg, Object data) {
        if (code) {
            return getResponse(successCode, msg, data);
        } else {
            return getResponse(errorCode, msg, data);
        }
    }

    public static Map<String, Object> getResponse(boolean code, Object data) {
        if (code) {
            return getResponse(successCode, "", data);
        } else {
            return getResponse(errorCode, "", data);
        }
    }

    public static Map<String, Object> getResponse(boolean code, String msg) {
        if (code) {
            return getResponse(successCode, msg, null);
        } else {
            return getResponse(errorCode, msg, null);
        }
    }

    public static Map<String, Object> getResponse(boolean code) {
        if (code) {
            return getResponse(successCode, "", null);
        } else {
            return getResponse(errorCode, "", null);
        }
    }

    public static Map<String, Object> getErrorResponse(String msg) {
        return getResponse(errorCode, msg, null);
    }

    public static Map<String, Object> getNotLoginResponse(String msg) {
        return getResponse(needLoginCode, "验证失败，请重新登录！", null);
    }

    public static Map<String, Object> getSQLErrorResponse() {
        return getErrorResponse("未知原因，操作失败！");
    }

    public static Map<String, Object> getSQLSuccessResponse() {
        return getSuccessResponse("操作成功！");
    }

    public static Map<String, Object> getNotLoginResponse() {
        return getNotLoginResponse("验证失败，请重新登录！");
    }

    public static Map<String, Object> getErrorResponse() {
        return getResponse(false, "请求错误！");
    }

    public static Map<String, Object> getSuccessResponse() {
        return getResponse(successCode, "请求成功！", null);
    }

    public static Map<String, Object> getDefaultSuccessResponse() {
        return getResponse(defaultSuccessCode, "请求成功！", null);
    }

    public static Map<String, Object> getSuccessResponse(String msg, Object data) {
        return getResponse(successCode, msg, data);
    }

    public static Map<String, Object> getSuccessResponse(Object data) {
        return getResponse(successCode, "操作成功！", data);
    }

    public static Map<String, Object> getSuccessResponse(String msg) {
        return getResponse(successCode, msg, null);
    }

    public static Map<String, Object> getResponse(int count) {
        return MyResponse.getSuccessResponse("成功修改" + count + "条记录！");
    }

    public static boolean isSuccessByRootMap(Map<String, Object> map) {
        return map.get("code").equals(successCode);
    }

    public static boolean isErrorByRootMap(Map<String, Object> map) {
        return map.get("code").equals(errorCode);
    }

    public static Object getDataByRootMap(Map<String, Object> map) {
        return map.get("data");
    }

    public static String getMsgByRootMap(Map<String, Object> map) {
        return map.get("msg").toString();
    }

    /**
     * 设置不用加密的方法
     *
     * @param map
     * @return
     */
    public static Map<String, Object> getNotEncryptString(Map<String, Object> map) {
        map.put("NotEncrypt", true);
        return map;
    }

    /**
     * 返回请求错误码
     *
     * @return
     */
    public static int getErrorCode() {
        return errorCode;
    }

    /**
     * 返回请求成功码
     *
     * @return
     */
    public static int getSuccessCode() {
        return successCode;
    }

    /**
     * 返回未登陆码
     *
     * @return
     */
    public static int getNeedLoginCode() {
        return needLoginCode;
    }

    /**
     * 分页搜索返回结果
     *
     * @param code
     * @param msg
     * @param page
     * @param data
     * @return
     */
    public static Map<String, Object> getPageResponse(boolean code, String msg, Page page, Object data) {
        Map<String, Object> map = getResponse(code, msg, data);
        map.put("page", page);
        return map;
    }

    public static Map<String, Object> getResponse(boolean code, Page page, Object data) {
        // TODO Auto-generated method stub
        return getPageResponse(code, "", page, data);
    }

}
