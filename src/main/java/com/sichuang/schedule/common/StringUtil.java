package com.sichuang.schedule.common;

import java.util.ArrayList;
import java.util.List;

public class StringUtil {
	
	public static boolean isEmpty(String string) {
		if (string == null || string.trim().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 解析ids字符串
	 * @param ids
	 * @return
	 */
	public static List<Integer> parseIdList(String ids){
		List<Integer> idList = new ArrayList<>();
		if(StringUtil.isEmpty(ids) == false){
			String[] idStrings = ids.split(",");
			for (int i = 0; i < idStrings.length; i++) {
				try {
					int id = Integer.parseInt(idStrings[i]);
					if(id > 0){
						//未使用权限组可以删除
						idList.add(id);
					}
				}catch (Exception e){
					idList.clear();
					return idList;
				}
			}
		}
		return idList;
	}

	/**
	 * 根据当前时间生成编号
	 * @param prefix
	 * @return
	 */
	public static String createSystemNO(String prefix){
		DateTimeModel dateTimeModel = new DateTimeModel();
		return prefix + dateTimeModel.getDataString("yyyyMMdd-HHmmss");
	}
}
