package com.sichuang.schedule.controller;

import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.model.entity.AttentionPoint;
import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.service.IAttentionPointService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/api/attentionPoint")
public class AttentionPointController extends BaseController {

    @Resource
    protected IAttentionPointService attentionPointService;

    @ResponseBody
    @RequestMapping("/search")
    public Map<String, Object> get(HttpServletRequest request, AttentionPoint attentionPoint, Page page) {
        return attentionPointService.getByModelAndPage(attentionPoint, page);
    }

    @ResponseBody
    @RequestMapping("/getByScheduleDate")
    public Map<String, Object> getByScheduleDate(HttpServletRequest request, Integer pointType, Date scheduleDate) {
        return attentionPointService.getByScheduleDateAndPage(scheduleDate, pointType);
    }


    @ResponseBody
    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, AttentionPoint attentionPoint) {
        User user = getUserSession(request);
        attentionPoint.setAddManName(user.getUserName());
        return attentionPointService.add(attentionPoint);
    }

    @ResponseBody
    @RequestMapping("/update")
    public Map<String, Object> update(HttpServletRequest request, AttentionPoint attentionPoint) {
        return attentionPointService.update(attentionPoint, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String attentionPointIds){
        return attentionPointService.removeByAttentionPointIds(attentionPointIds, getUserSession(request));
    }
}
