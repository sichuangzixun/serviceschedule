package com.sichuang.schedule.controller;


import com.alibaba.fastjson.JSON;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.common.MyResponse;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


public class BaseController extends MyResponse {
    private static HttpSession httpSession;
    public static final String TimeFormat = "yyyy-MM-dd HH:mm:ss";
    public static final String DateFormat = "yyyy-MM-dd";
    // 本地配置
    public static final String URL = "/schedule/";
    //tms web css version page.css
    public static final String TMSCssVersion = "1.2.1";
    //tms web js version function.js
    public static final String TMSJsVersion = "1.2.1";
    public static final String LoginUrl = URL + "web/user/login";
    public static final String MsgUrl = URL + "web/user/message";


    /**
     * 根据session 判断用户是否登录
     *
     * @param httpServletRequest
     * @return
     */
    protected boolean isLogin(HttpServletRequest httpServletRequest) {
        User userSession = getUserSession(httpServletRequest);
        if (userSession != null) {
            return true;
        }
        return false;
    }


    /**
     * 保存用户session信息
     *
     * @param request
     * @param userSession
     */
    public static void saveUserSession(HttpServletRequest request, User userSession) {
        httpSession = request.getSession();
        httpSession.setAttribute("userSession", userSession);
    }


    /**
     * 获取session保存用户信息
     *
     * @param request
     * @return
     */
    public static User getUserSession(HttpServletRequest request) {
        httpSession = request.getSession();
        User userSession = (User) httpSession.getAttribute("userSession");
        return userSession;
    }

    /**
     * 获取当前服务器路径
     *
     * @param request
     * @return
     */
    public static String getBasePath(HttpServletRequest request) {
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
                + "/";
        return basePath;
    }

    /**
     * 返回suggest搜索数据
     *
     * @param callBack
     * @param data
     * @return
     */
    public static String getSuggestCallBack(String callBack, Object data) {
        return callBack + "(" + JSON.toJSONString(data) + ")";
    }

    /**
     * 班次
     * @return
     */
    public static List<String> getShiftRecordExcelTitle() {
        List<String> title = new ArrayList<>();
        title.add("交接时间");
        title.add("班次");
        title.add("现场负责人");
        title.add("值班员");
        title.add("本班次遗留工作");
        title.add("长期通知");
        title.add("短期通知");
        return title;
    }
}
