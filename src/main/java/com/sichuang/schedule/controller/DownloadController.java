package com.sichuang.schedule.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @author xs
 * @date 2018/7/25 9:17
 */
@Controller
@RequestMapping("/download")
public class DownloadController extends BaseController {

    @Value("#{configProperties['filepath']}")
    private String filepath;

    @RequestMapping()
    public void download(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //获得请求文件名
        String filename = request.getParameter("filename");

        response.setContentType("application/OCTET-STREAM;charset=UTF-8");
        //设置Content-Disposition
        response.setHeader("Content-Disposition", "attachment;filename=");//+ filename
        //读取目标文件，通过response将目标文件写到客户端
        //获取目标文件的绝对路径
        String fullFileName = filepath + filename;

        //读取文件
        InputStream in = new FileInputStream(fullFileName);
        OutputStream out = response.getOutputStream();

        //写文件
        int b;
        while ((b = in.read()) != -1) {
            out.write(b);
        }

        in.close();
        out.flush();
        out.close();
    }
}
