package com.sichuang.schedule.controller;

import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.service.IItemTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/api/itemType")
public class ItemTypeController extends BaseController{

    @Resource protected IItemTypeService iItemTypeService;

    @ResponseBody
    @RequestMapping("/get")
    public Map<String, Object> get(HttpServletRequest request, Integer dataType, Page page){
        return iItemTypeService.getByPage(dataType, page);
    }

    @ResponseBody
    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, ItemType itemType){
        User user = getUserSession(request);
        itemType.setAddManName(user.getUserName());
        return iItemTypeService.addTypes(itemType);
    }

    @ResponseBody
    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String typeIds){
        return iItemTypeService.removeByTypeIds(typeIds, getUserSession(request));
    }

}
