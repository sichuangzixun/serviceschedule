package com.sichuang.schedule.controller;

import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.ShiftRecord;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IShiftRecordService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/api/shiftRecord")
public class ShiftRecordController extends BaseController{

    @Resource
    protected IShiftRecordService shiftRecordService;


    @ResponseBody
    @RequestMapping("/search")
    public Map<String, Object> search(HttpServletRequest request, ShiftRecord shiftRecord, Date dutyDateEnd, Page page){
        return shiftRecordService.getByModelAndPage(shiftRecord, dutyDateEnd, page);
    }

    @ResponseBody
    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, ShiftRecord shiftRecord){
        User userSession = getUserSession(request);
        shiftRecord.setAddManName(userSession.getUserName());
        return shiftRecordService.addShiftRecord(shiftRecord);
    }


    @ResponseBody
    @RequestMapping("/update")
    public Map<String, Object> update(HttpServletRequest request, ShiftRecord shiftRecord){
        return shiftRecordService.update(shiftRecord, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/updateShiftStatus")
    public Map<String, Object> updateShiftStatus(HttpServletRequest request, String shiftRecordIds){
        return shiftRecordService.endShift(shiftRecordIds, getUserSession(request));
    }

   /* @ResponseBody
    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String shiftRecordIds){
        return shiftRecordService.remove(shiftRecordIds, getUserSession(request));
    }*/
}
