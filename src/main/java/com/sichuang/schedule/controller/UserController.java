package com.sichuang.schedule.controller;

import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("/api/user")
public class UserController extends BaseController {

    @Resource
    private IUserService userService;

    //------------------- 用户登录--------------------------------------------------------
    @ResponseBody
    @RequestMapping("/login")
    public Map<String, Object> login(HttpServletRequest request, String loginName, String password) {
        User user = new User();
        user.setLoginName(loginName);
        user.setPassword(password);
        return userService.login(request, user);
    }

    @ResponseBody
    @RequestMapping("/changePassword")
    private Map<String, Object> changePassword(HttpServletRequest request, String oldPassword, String newPassword) {
        if (!isLogin(request)) {
            return getNotLoginResponse();
        }
        return userService.changePassword(request, oldPassword, newPassword);
    }

    @ResponseBody
    @RequestMapping("/search")
    public Map<String, Object> search(HttpServletRequest request, Page page){
        return userService.search(page);
    }

    @ResponseBody
    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String userIds){
        return userService.removeByUserIds(userIds, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/save")
    public Map<String, Object> save(HttpServletRequest request, User user){
        User userSession = getUserSession(request);
        return userService.save(user, userSession);
    }
}
