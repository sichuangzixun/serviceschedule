package com.sichuang.schedule.controller;

import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.common.StringUtil;
import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkItem;
import com.sichuang.schedule.service.IWorkItemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/workItem")
public class WorkItemController extends BaseController{

    @Resource protected IWorkItemService workItemService;

    @ResponseBody
    @RequestMapping("/search")
    public Map<String, Object> search(HttpServletRequest request, WorkItem workItem, Date addTimeEnd, Date endTimeEnd, Page page){
        return workItemService.getByModelAndPage(workItem,addTimeEnd, endTimeEnd, page);
    }

    @ResponseBody
    @RequestMapping("/getByShiftTime")
    public Map<String, Object> getByShiftTime(HttpServletRequest request, Date shiftTime){
        List<WorkItem> workItemList = workItemService.getByShiftTime(null, shiftTime);
        Page page = new Page(1, workItemList.size());
        return getResponse(true, page, workItemList);
    }


    @ResponseBody
    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, WorkItem workItem){
        User user = getUserSession(request);
        workItem.setAddManName(user.getUserName());
        return workItemService.addWorkItem(workItem, user);
    }

    @ResponseBody
    @RequestMapping("/update")
    public Map<String, Object> update(HttpServletRequest request, WorkItem workItem){
        return workItemService.update(workItem, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String workItemIds){
        return workItemService.removeByWorkItemIds(workItemIds, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/complete")
    public Map<String, Object> complete(HttpServletRequest request, String workItemIds){
        return workItemService.completeByWorkItemIds(workItemIds, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/end")
    public Map<String, Object> end(HttpServletRequest request, WorkItem workItem, Integer shiftRecordId){
        return workItemService.complete(workItem, shiftRecordId, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/handle")
    public Map<String, Object> handle(HttpServletRequest request, WorkItem workItem, Integer shiftRecordId){
        return workItemService.handle(workItem, shiftRecordId, getUserSession(request));
    }

}
