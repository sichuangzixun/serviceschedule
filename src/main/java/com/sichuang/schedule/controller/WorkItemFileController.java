package com.sichuang.schedule.controller;

import com.sichuang.schedule.service.IWorkItemFileService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xs
 * @date 2018/7/23 17:23
 */
@Controller
@RequestMapping("/api/workItemFile")
public class WorkItemFileController extends BaseController{

    @Resource
    protected IWorkItemFileService workItemFileService;

    @ResponseBody
    @RequestMapping("/remove")
    Map<String, Object> remove(HttpServletRequest request, String fileIds){
        return workItemFileService.removeByFileIds(fileIds, getUserSession(request));
    }
}
