package com.sichuang.schedule.controller;

import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkRecord;
import com.sichuang.schedule.service.IWorkRecordService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/api/workRecord")
public class WorkRecordController extends BaseController {

    @Resource
    protected IWorkRecordService workRecordService;

    @ResponseBody
    @RequestMapping("/search")
    public Map<String, Object> search(HttpServletRequest request, WorkRecord workRecord, Date dutyDateEnd, Page page) {
        return workRecordService.getByModelAndPage(workRecord, dutyDateEnd, page);
    }

    @ResponseBody
    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, WorkRecord workRecord) {
        User userSession = getUserSession(request);
        workRecord.setAddManName(userSession.getUserName());
        return workRecordService.addWorkRecord(workRecord);
    }

    @ResponseBody
    @RequestMapping("/update")
    public Map<String, Object> update(HttpServletRequest request, WorkRecord workRecord) {
        return workRecordService.update(workRecord, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String workRecordIds) {
        return workRecordService.removeByWorkRecordIds(workRecordIds, getUserSession(request));
    }

    @ResponseBody
    @RequestMapping("/removeByMonth")
    public Map<String, Object> removeByMonth(HttpServletRequest request, Date dutyDate) {
        return workRecordService.removeByMonth(dutyDate);
    }

}
