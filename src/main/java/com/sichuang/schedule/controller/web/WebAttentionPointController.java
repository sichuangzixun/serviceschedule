package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.AttentionPoint;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IAttentionPointService;
import com.sichuang.schedule.service.IItemTypeService;
import com.sichuang.schedule.service.IWorkItemFileService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/web/attentionPoint")
public class WebAttentionPointController extends BaseController {

    @Resource
    protected IAttentionPointService attentionPointService;
    @Resource
    protected IItemTypeService itemTypeService;
    @Resource
    private IWorkItemFileService workItemFileService;

    @RequestMapping("")
    public String index(Model model, HttpServletRequest request) {
        model.addAttribute("PointTypeEnum", EnumManage.PointTypeEnum.getList());
        return "attentionPoints";
    }

    @RequestMapping("/add")
    public String add(Model model, AttentionPoint attentionPoint){
        //默认通知宣贯时间为7天
        DateTime dateTime = new DateTime();
        //设置开始日期
        attentionPoint.setBeginDate(dateTime.toDate());
        //设置结束日期
        Date endDate = dateTime.plusDays(6).toDate();
        attentionPoint.setEndDate(endDate);
        model.addAttribute("attentionPoint", attentionPoint);
        model.addAttribute("PointTypeEnum", EnumManage.PointTypeEnum.getList());
        int dataType = EnumManage.DataTypeEnum.Label.getId();
        model.addAttribute("itemTypeList", itemTypeService.getList(dataType));
        return "attentionPointAdd";
    }

    @RequestMapping("/update")
    public String update(Model model, HttpServletRequest request, Integer attentionPointId){
        Map<String, Object> map = attentionPointService.verifyAttentionPoint(attentionPointId, getUserSession(request));
        if(isErrorByRootMap(map)){
            model.addAllAttributes(map);
            return "message";
        }
        AttentionPoint attentionPoint = (AttentionPoint) getDataByRootMap(map);
        model.addAttribute("attentionPoint", attentionPoint);
        model.addAttribute("PointTypeEnum", EnumManage.PointTypeEnum.getList());
        int dataType = EnumManage.DataTypeEnum.Label.getId();
        model.addAttribute("itemTypeList", itemTypeService.getList(dataType));
        return "attentionPointUpdate";
    }

    @RequestMapping("/end")
    public String end(Model model, HttpServletRequest request, Integer attentionPointId){
        Map<String, Object> map = attentionPointService.verifyAttentionPoint(attentionPointId, getUserSession(request));
        if(isErrorByRootMap(map)){
            model.addAllAttributes(map);
            return "message";
        }
        AttentionPoint attentionPoint = (AttentionPoint) getDataByRootMap(map);
        model.addAttribute("attentionPoint", attentionPoint);
        return "attentionPointEnd";
    }

    @RequestMapping("/info")
    public String info(Model model, HttpServletRequest request, Integer attentionPointId){
        Map<String, Object> map = attentionPointService.verifyAttentionPoint(attentionPointId, null);
        if(isErrorByRootMap(map)){
            model.addAllAttributes(map);
            return "message";
        }
        AttentionPoint attentionPoint = (AttentionPoint) getDataByRootMap(map);
        model.addAttribute("attentionPoint", attentionPoint);
        model.addAttribute("PointTypeEnum", EnumManage.PointTypeEnum.getList());
        int fileType =  EnumManage.FileTypeEnum.AttentionPoint.getId();
        //获取附件列表
        model.addAttribute("fileType", fileType);
        model.addAttribute("fileList", workItemFileService.get(attentionPointId, fileType));
        return "attentionPointFile";
    }

    @RequestMapping("/exportExcel")
    public String exportExcel(Model model, AttentionPoint attentionPoint, HttpServletResponse response) {
        Map<String, Object> map = attentionPointService.exportExcel(attentionPoint, response);
        model.addAllAttributes(map);
        return "message";
    }
}
