package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.service.IItemTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/web/itemType")
public class WebItemTypeController extends BaseController{

    @Resource protected IItemTypeService itemTypeService;

    @RequestMapping("")
    public String index(Model model, HttpServletRequest request, Integer dataType){
        ItemType itemType = new ItemType();
        itemType.setDataType(dataType);
        model.addAttribute("itemType", itemType);
        return "itemTypes";
    }

}


