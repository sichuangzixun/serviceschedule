package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.model.entity.ShiftRecord;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkRecord;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IAttentionPointService;
import com.sichuang.schedule.service.IItemTypeService;
import com.sichuang.schedule.service.IShiftRecordService;
import com.sichuang.schedule.service.IWorkItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/web/shiftRecord")
public class WebShiftRecordController extends BaseController {

    @Resource
    protected IShiftRecordService shiftRecordService;
    @Resource
    protected IWorkItemService workItemService;
    @Resource
    protected IItemTypeService itemTypeService;
    @Resource
    private IAttentionPointService attentionPointService;

    @RequestMapping()
    public String index(Model model, HttpServletRequest request) {
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        return "shiftRecords";
    }

    @RequestMapping("/info")
    public String info(Model model, HttpServletRequest request, Integer shiftRecordId) {
        //查看无需验证交接状态和用户权限
        Map<String, Object> map = shiftRecordService.verifyShiftRecord(shiftRecordId, null);
        if (isSuccessByRootMap(map)) {
            ShiftRecord shiftRecord = (ShiftRecord) getDataByRootMap(map);
            model.addAttribute("shiftRecord", shiftRecord);
            model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
            model.addAttribute("ShiftStatusEnum", EnumManage.ShiftStatusEnum.getList());
            model.addAllAttributes(shiftRecordService.getDataByShiftRecord(shiftRecord));
            return "shiftRecordInfo";
        }
        model.addAllAttributes(map);
        return "message";
    }

    @RequestMapping("/add")
    public String add(Model model, HttpServletRequest request, ShiftRecord shiftRecord) {
        ShiftRecord currentShiftRecord = shiftRecordService.getCurrentByTime();
        if(currentShiftRecord != null){
           shiftRecord.setShiftType(currentShiftRecord.getShiftType());
           shiftRecord.setDutyDate(currentShiftRecord.getDutyDate());
        }else {
            shiftRecord.setDutyDate(new Date());
        }

        model.addAttribute("shiftRecord", shiftRecord);
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        //值班员名单
        List<ItemType> watcherList = itemTypeService.getList(EnumManage.DataTypeEnum.Watcher.getId());
        model.addAttribute("watcherList", watcherList);
        return "shiftRecordAdd";
    }

    @RequestMapping("/update")
    public String update(Model model, HttpServletRequest request, Integer shiftRecordId) {
        User userSession = getUserSession(request);
        Map<String, Object> map = shiftRecordService.viewShiftRecord(shiftRecordId, userSession);
        model.addAllAttributes(map);
        if(isSuccessByRootMap(map)){
            return "shiftRecordUpdate";
        }
        return "message";
    }

    @RequestMapping("/attention")
    public String attention(Model model, HttpServletRequest request, Integer shiftRecordId) {
        User userSession = getUserSession(request);
        Map<String, Object> map = shiftRecordService.viewShiftRecord(shiftRecordId, userSession);
        model.addAllAttributes(map);
        if(isSuccessByRootMap(map)){
            return "shiftRecordAttention";
        }
        return "message";
    }

    @RequestMapping("/item")
    public String item(Model model, HttpServletRequest request, Integer shiftRecordId) {
        User userSession = getUserSession(request);
        Map<String, Object> map = shiftRecordService.viewShiftRecord(shiftRecordId, userSession);
        model.addAllAttributes(map);
        if(isSuccessByRootMap(map)){
            return "shiftRecordItem";
        }
        return "message";
    }

    @RequestMapping("/exportExcel")
    public String exportExcelByScheduleIds(Model model, HttpServletRequest request, HttpServletResponse response, String shiftRecordIds, ShiftRecord shiftRecord, Date dutyDateEnd){
        Map<String, Object> map = shiftRecordService.exportExcel(response, shiftRecordIds, shiftRecord, dutyDateEnd);
        model.addAllAttributes(map);
        return "message";
    }
}
