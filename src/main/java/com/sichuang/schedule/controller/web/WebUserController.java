package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IAttentionPointService;
import com.sichuang.schedule.service.IShiftRecordService;
import com.sichuang.schedule.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/web/user")
public class WebUserController extends BaseController {
    @Resource
    private IUserService userService;
    @Resource
    private IShiftRecordService shiftRecordService;
    @Resource
    private IAttentionPointService attentionPointService;

    // web 主页面
    @RequestMapping()
    protected String index(Model model, HttpServletRequest request) {
        User userSession = getUserSession(request);
        model.addAttribute("user", userSession);
        return "index";
    }

    // web 首页
    @RequestMapping("/content")
    protected String content(Model model, HttpServletRequest request) {
        User userSession = getUserSession(request);
        model.addAttribute("user", userSession);
        model.addAttribute("attentionPointList",attentionPointService.getByScheduleDate(new Date(), null));
        model.addAttribute("previousShiftRecord", shiftRecordService.getPreviousShiftRecord(new Date()));
        model.addAttribute("currentShiftRecord", shiftRecordService.getCurrentShiftRecordData(new Date()));
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        model.addAttribute("ShiftStatusEnum", EnumManage.ShiftStatusEnum.getList());
        model.addAttribute("PointTypeEnum", EnumManage.PointTypeEnum.getList());
        return "content";
    }

    @RequestMapping("/changePassword")
    public String changePassword(Model model, HttpServletRequest request) {
        User user = getUserSession(request);
        model.addAttribute("loginName", user.getLoginName());
        return "changePassword";
    }

    // 用户登录
    @RequestMapping("/login")
    public String login(Model model, HttpServletRequest request) {
        // 返回登录界面
        return "login";
    }

    //用户退出登录
    @RequestMapping("/logout")
    public String logout(Model model, HttpServletRequest request) {
        Map<String, Object> map = userService.logout(request);
        model.addAllAttributes(map);
        return "login";
    }

    @RequestMapping("/list")
    public String list(Model model, HttpServletRequest request) {
        model.addAttribute("userType", EnumManage.UserTypeEnum.getList());
        return "users";
    }

    @RequestMapping("/add")
    public String add(Model model, HttpServletRequest request) {
        User user = new User();
        user.setUserType(EnumManage.UserTypeEnum.Normal.getId());
        model.addAttribute("user", user);
        model.addAttribute("userType", EnumManage.UserTypeEnum.getList());
        return "userEdit";
    }

    @RequestMapping("/update")
    public String update(Model model, HttpServletRequest request, Integer userId) {
        Map<String, Object> map = userService.verifyUser(userId);
        if (isSuccessByRootMap(map)) {
            User user = (User) getDataByRootMap(map);
            model.addAttribute("user", user);
            model.addAttribute("userType", EnumManage.UserTypeEnum.getList());
            return "userEdit";
        }
        model.addAllAttributes(map);
        return "message";
    }
}