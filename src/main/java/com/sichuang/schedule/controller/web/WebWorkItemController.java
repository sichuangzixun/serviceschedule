package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.model.entity.ShiftRecord;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkItem;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/web/workItem")
public class WebWorkItemController extends BaseController {

    @Resource
    protected IWorkItemService workItemService;
    @Resource
    protected IItemTypeService itemTypeService;
    @Resource
    protected IShiftRecordService shiftRecordService;
    @Resource
    protected IWorkItemFileService workItemFileService;

    @RequestMapping()
    public String index(Model model) {
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        int dataType = EnumManage.DataTypeEnum.Label.getId();
        model.addAttribute("itemTypeList", itemTypeService.getList(dataType));
        return "workItems";
    }

    @RequestMapping("/add")
    public String add(Model model, WorkItem workItem, HttpServletRequest request) {
        Map<String, Object> map = shiftRecordService.verifyShiftRecord(workItem.getAddShiftRecordId(), getUserSession(request));
        if (isErrorByRootMap(map)) {
            model.addAllAttributes(map);
            return "message";
        }
        workItem.setIsComplete(false);
        model.addAttribute("workItem", workItem);
        int dataType = EnumManage.DataTypeEnum.Label.getId();
        model.addAttribute("itemTypeList", itemTypeService.getList(dataType));
        return "workItemAdd";
    }

    @RequestMapping("/update")
    public String update(Model model, HttpServletRequest request, Integer workItemId) {
        //验证交接内容
        User userSession = getUserSession(request);
        Map<String, Object> map = workItemService.verifyShiftRecord(workItemId, userSession);
        model.addAllAttributes(map);
        if (isSuccessByRootMap(map)) {
            int dataType = EnumManage.DataTypeEnum.Label.getId();
            model.addAttribute("itemTypeList", itemTypeService.getList(dataType));
            dataType = EnumManage.DataTypeEnum.Watcher.getId();
            model.addAttribute("watcherList", itemTypeService.getList(dataType));
            return "workItemUpdate";
        }
        return "message";
    }

    @RequestMapping("/complete")
    public String complete(Model model, HttpServletRequest request, Integer workItemId, Integer shiftRecordId) {
        //验证交接内容
        User userSession = getUserSession(request);
        Map<String, Object> map = workItemService.verifyWorkItem(workItemId, null);
        model.addAllAttributes(map);
        if (isSuccessByRootMap(map)) {
            WorkItem workItem = (WorkItem) getDataByRootMap(map);
            if(workItem.getIsComplete() == false){
                //验证交接记录
                map = shiftRecordService.verifyShiftRecord(shiftRecordId, userSession);
                if (isSuccessByRootMap(map)) {
                    ShiftRecord shiftRecord = (ShiftRecord) getDataByRootMap(map);
                    model.addAttribute("shiftRecord", shiftRecord);
                    workItem.setIsComplete(true);
                    workItem.setEndTime(shiftRecord.getShiftTime());
                    workItem.setEndHandlerName(shiftRecord.getDutyOfficer());
                    model.addAttribute("workItem", workItem);
                    int dataType = EnumManage.DataTypeEnum.Watcher.getId();
                    model.addAttribute("watcherList", itemTypeService.getList(dataType));
                    return "workItemComplete";
                }else {
                    model.addAllAttributes(map);
                }
            }else {
                model.addAttribute("msg", "工作已完成！");
            }
        }

        return "message";
    }

    @RequestMapping("/handle")
    public String handler(Model model, HttpServletRequest request, Integer workItemId, Integer shiftRecordId) {
        //验证交接内容
        User userSession = getUserSession(request);
        Map<String, Object> map = workItemService.verifyWorkItem(workItemId, null);
        if (isSuccessByRootMap(map)) {
            WorkItem workItem = (WorkItem) getDataByRootMap(map);
            if(workItem.getIsComplete() == false){
                //验证交接记录
                map = shiftRecordService.verifyShiftRecord(shiftRecordId, userSession);
                if (isSuccessByRootMap(map)) {
                    ShiftRecord shiftRecord = (ShiftRecord) getDataByRootMap(map);
                    model.addAttribute("shiftRecord", shiftRecord);
                    model.addAttribute("workItem", workItem);
                    return "workItemHandle";
                }
            }else {
                map = getErrorResponse("已完成工作不能添加处理情况！");
            }

        }
        model.addAllAttributes(map);
        return "message";
    }

    @RequestMapping("/exportExcel")
    public String exportExcel(Model model, WorkItem workItem, Date addTimeEnd, Date endTimeEnd, HttpServletResponse response) {
        Map<String, Object> map = workItemService.exportExcel(workItem, addTimeEnd, endTimeEnd, response);
        model.addAllAttributes(map);
        return "message";
    }

    @RequestMapping("/info")
    public String info(Model model, HttpServletRequest request, Integer workItemId) {
        Map<String, Object> map = workItemService.verifyWorkItem(workItemId, null);
        if (isSuccessByRootMap(map)) {
            WorkItem workItem = (WorkItem) getDataByRootMap(map);
            //验证交接记录
            map = shiftRecordService.verifyShiftRecord(workItem.getAddShiftRecordId(), null);
            if (isSuccessByRootMap(map)) {
                ShiftRecord shiftRecord = (ShiftRecord) getDataByRootMap(map);

                model.addAttribute("workItem", workItem);
                model.addAttribute("shiftRecord", shiftRecord);
                model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());

                //获取附件列表
                int fileType = EnumManage.FileTypeEnum.WorkItem.getId();
                model.addAttribute("fileType", fileType);
                model.addAttribute("fileList", workItemFileService.get(workItemId, fileType));
                return "workItemFile";
            }
        }
        model.addAllAttributes(map);
        return "message";

    }

}
