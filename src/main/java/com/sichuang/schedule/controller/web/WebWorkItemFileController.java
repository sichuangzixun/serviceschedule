package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkItemFile;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IAttentionPointService;
import com.sichuang.schedule.service.IWorkItemFileService;
import com.sichuang.schedule.service.IWorkItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xs
 * @date 2018/9/12 16:18
 */
@Controller
@RequestMapping("/web/workItemFile")
public class WebWorkItemFileController extends BaseController{

    @Resource
    private IWorkItemFileService workItemFileService;
    @Resource
    private IAttentionPointService attentionPointService;
    @Resource
    private IWorkItemService workItemService;

    @RequestMapping("/add")
    public String add(Model model, HttpServletRequest request, WorkItemFile workItemFile){
        User userSession = getUserSession(request);
        if(workItemFile.getRecordId() != null ){
            Map<String,Object> map = getDefaultSuccessResponse();
            if(workItemFile.getFileType() != null){
                if(workItemFile.getFileType().equals(EnumManage.FileTypeEnum.AttentionPoint.getId())){
                    map = attentionPointService.verifyAttentionPoint(workItemFile.getRecordId(), userSession);
                }
                if(workItemFile.getFileType().equals(EnumManage.FileTypeEnum.WorkItem.getId())){
                    map = workItemService.verifyWorkItem(workItemFile.getRecordId(), userSession);
                }
            }else {
                map = getErrorResponse("操作失败，无法上传文件！");
            }
            if(isErrorByRootMap(map)){
                model.addAllAttributes(map);
                return "message";
            }

        }

        model.addAttribute("workItemFile", workItemFile);
        return "workItemFileAdd";
    }

    @RequestMapping("/upload")
    public String upload(Model model, HttpServletRequest request, WorkItemFile workItemFile) {
        Map<String, Object> map = workItemFileService.uploadWorkItem(request, workItemFile);
        model.addAllAttributes(map);
        return "message";
    }
}
