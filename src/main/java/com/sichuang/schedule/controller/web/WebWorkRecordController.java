package com.sichuang.schedule.controller.web;

import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.model.entity.ShiftRecord;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkRecord;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IItemTypeService;
import com.sichuang.schedule.service.IShiftRecordService;
import com.sichuang.schedule.service.IWorkItemFileService;
import com.sichuang.schedule.service.IWorkRecordService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/web/workRecord")
public class WebWorkRecordController extends BaseController {

    @Resource
    protected IWorkRecordService workRecordService;
    @Resource
    protected IItemTypeService itemTypeService;
    @Resource
    private IWorkItemFileService workItemFileService;
    @Resource
    private IShiftRecordService shiftRecordService;

    @RequestMapping()
    public String index(Model model, Date dutyDate) {
        //设置表头
        if(dutyDate == null){
            dutyDate = new Date();
        }
        DateTime monthDate = new DateTime(dutyDate);
        int end = monthDate.dayOfMonth().getMaximumValue();
        dutyDate = monthDate.withDayOfMonth(1).toDate();
        int month = monthDate.getMonthOfYear();
        int year = monthDate.getYear();
        model.addAttribute("end", end);
        model.addAttribute("dutyDate", dutyDate);
        model.addAttribute("month", month);
        model.addAttribute("year", year);

        //获取值班记录
        model.addAttribute("watcherList", workRecordService.getByMonth(dutyDate));
        return "workRecordSchedule";
    }

    @RequestMapping("/list")
    public String list(Model model) {
        model.addAttribute("watcherList", itemTypeService.getList(EnumManage.DataTypeEnum.Watcher.getId()));
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        return "workRecords";
    }

    @RequestMapping("/file")
    public String file(Model model) {
        model.addAttribute("fileList", workItemFileService.get(0, EnumManage.FileTypeEnum.WorkRecord.getId()));
        return "workRecordFile";
    }

    @RequestMapping("/add")
    public String add(Model model, HttpServletRequest request, WorkRecord workRecord) {
        User userSession = getUserSession(request);
        if(workRecord.getShiftRecordId() != null){
            Map<String, Object> map = shiftRecordService.verifyShiftRecord(workRecord.getShiftRecordId(), userSession);
            if(isErrorByRootMap(map)){
                model.addAllAttributes(map);
                return "message";
            }
            ShiftRecord shiftRecord = (ShiftRecord) getDataByRootMap(map);
            workRecord.setDutyDate(shiftRecord.getDutyDate());
            workRecord.setShiftType(shiftRecord.getShiftType());
        }
        workRecord.setIsManager(false);
        model.addAttribute("workRecord", workRecord);
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        //值班员名单
        List<ItemType> watcherList = itemTypeService.getList(EnumManage.DataTypeEnum.Watcher.getId());
        model.addAttribute("watcherList", watcherList);
        return "workRecordAdd";
    }

    @RequestMapping("/update")
    public String update(Model model, HttpServletRequest request, Integer workRecordId) {
        Map<String, Object> map = workRecordService.verifyWorkRecord(workRecordId, getUserSession(request));
        if (isErrorByRootMap(map)) {
            model.addAllAttributes(map);
            return "message";
        }
        WorkRecord workRecord = (WorkRecord) getDataByRootMap(map);
        model.addAttribute("workRecord", workRecord);
        model.addAttribute("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        return "workRecordUpdate";
    }

    /**
     * 导人排班表
     *
     * @param model
     * @param request
     * @param shiftRecord 值班月份
     * @return
     */
    @RequestMapping("/import")
    public String importDemandInfo(Model model, HttpServletRequest request, ShiftRecord shiftRecord) {
        User user = getUserSession(request);
        shiftRecord.setAddManName(user.getUserName());
        Map<String, Object> map = workRecordService.upload(request, shiftRecord);
        model.addAllAttributes(map);
        return "message";
    }

    /**
     * 上传EXCEL文件，导入工单页面
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/uploadExcel")
    public String uploadExcel(Model model, HttpServletRequest request, Date dutyDate) {
        model.addAttribute("dutyDate", dutyDate);
        return "workRecordUpload";
    }
}


