package com.sichuang.schedule.dao;

import com.sichuang.schedule.model.entity.AccessRecord;
import com.sichuang.schedule.model.entity.AccessRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AccessRecordMapper {
    Integer countByExample(AccessRecordExample example);

    int deleteByExample(AccessRecordExample example);

    int deleteByPrimaryKey(Integer accessId);

    int insert(AccessRecord record);

    int insertSelective(AccessRecord record);

    List<AccessRecord> selectByExample(AccessRecordExample example);

    AccessRecord selectByPrimaryKey(Integer accessId);

    int updateByExampleSelective(@Param("record") AccessRecord record, @Param("example") AccessRecordExample example);

    int updateByExample(@Param("record") AccessRecord record, @Param("example") AccessRecordExample example);

    int updateByPrimaryKeySelective(AccessRecord record);

    int updateByPrimaryKey(AccessRecord record);
}