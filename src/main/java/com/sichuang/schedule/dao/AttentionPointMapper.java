package com.sichuang.schedule.dao;

import com.sichuang.schedule.model.entity.AttentionPoint;
import com.sichuang.schedule.model.entity.AttentionPointExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AttentionPointMapper {
    Integer countByExample(AttentionPointExample example);

    int deleteByExample(AttentionPointExample example);

    int deleteByPrimaryKey(Integer attentionPointId);

    int insert(AttentionPoint record);

    int insertSelective(AttentionPoint record);

    List<AttentionPoint> selectByExample(AttentionPointExample example);

    AttentionPoint selectByPrimaryKey(Integer attentionPointId);

    int updateByExampleSelective(@Param("record") AttentionPoint record, @Param("example") AttentionPointExample example);

    int updateByExample(@Param("record") AttentionPoint record, @Param("example") AttentionPointExample example);

    int updateByPrimaryKeySelective(AttentionPoint record);

    int updateByPrimaryKey(AttentionPoint record);
}