package com.sichuang.schedule.dao;

import com.sichuang.schedule.model.entity.ShiftRecord;
import com.sichuang.schedule.model.entity.ShiftRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ShiftRecordMapper {
    Integer countByExample(ShiftRecordExample example);

    int deleteByExample(ShiftRecordExample example);

    int deleteByPrimaryKey(Integer shiftRecordId);

    int insert(ShiftRecord record);

    int insertSelective(ShiftRecord record);

    List<ShiftRecord> selectByExample(ShiftRecordExample example);

    ShiftRecord selectByPrimaryKey(Integer shiftRecordId);

    int updateByExampleSelective(@Param("record") ShiftRecord record, @Param("example") ShiftRecordExample example);

    int updateByExample(@Param("record") ShiftRecord record, @Param("example") ShiftRecordExample example);

    int updateByPrimaryKeySelective(ShiftRecord record);

    int updateByPrimaryKey(ShiftRecord record);
}