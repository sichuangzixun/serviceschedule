package com.sichuang.schedule.dao;

import com.sichuang.schedule.model.entity.WorkItemFile;
import com.sichuang.schedule.model.entity.WorkItemFileExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkItemFileMapper {
    Integer countByExample(WorkItemFileExample example);

    int deleteByExample(WorkItemFileExample example);

    int deleteByPrimaryKey(Integer fileId);

    int insert(WorkItemFile record);

    int insertSelective(WorkItemFile record);

    List<WorkItemFile> selectByExample(WorkItemFileExample example);

    WorkItemFile selectByPrimaryKey(Integer fileId);

    int updateByExampleSelective(@Param("record") WorkItemFile record, @Param("example") WorkItemFileExample example);

    int updateByExample(@Param("record") WorkItemFile record, @Param("example") WorkItemFileExample example);

    int updateByPrimaryKeySelective(WorkItemFile record);

    int updateByPrimaryKey(WorkItemFile record);
}