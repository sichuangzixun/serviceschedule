package com.sichuang.schedule.dao;

import com.sichuang.schedule.model.entity.WorkItem;
import com.sichuang.schedule.model.entity.WorkItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkItemMapper {
    Integer countByExample(WorkItemExample example);

    int deleteByExample(WorkItemExample example);

    int deleteByPrimaryKey(Integer workItemId);

    int insert(WorkItem record);

    int insertSelective(WorkItem record);

    List<WorkItem> selectByExampleWithBLOBs(WorkItemExample example);

    List<WorkItem> selectByExample(WorkItemExample example);

    WorkItem selectByPrimaryKey(Integer workItemId);

    int updateByExampleSelective(@Param("record") WorkItem record, @Param("example") WorkItemExample example);

    int updateByExampleWithBLOBs(@Param("record") WorkItem record, @Param("example") WorkItemExample example);

    int updateByExample(@Param("record") WorkItem record, @Param("example") WorkItemExample example);

    int updateByPrimaryKeySelective(WorkItem record);

    int updateByPrimaryKeyWithBLOBs(WorkItem record);

    int updateByPrimaryKey(WorkItem record);
}