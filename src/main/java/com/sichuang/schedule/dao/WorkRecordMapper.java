package com.sichuang.schedule.dao;

import com.sichuang.schedule.model.entity.WorkRecord;
import com.sichuang.schedule.model.entity.WorkRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WorkRecordMapper {
    Integer countByExample(WorkRecordExample example);

    int deleteByExample(WorkRecordExample example);

    int deleteByPrimaryKey(Integer workRecordId);

    int insert(WorkRecord record);

    int insertSelective(WorkRecord record);

    List<WorkRecord> selectByExample(WorkRecordExample example);

    WorkRecord selectByPrimaryKey(Integer workRecordId);

    int updateByExampleSelective(@Param("record") WorkRecord record, @Param("example") WorkRecordExample example);

    int updateByExample(@Param("record") WorkRecord record, @Param("example") WorkRecordExample example);

    int updateByPrimaryKeySelective(WorkRecord record);

    int updateByPrimaryKey(WorkRecord record);
}