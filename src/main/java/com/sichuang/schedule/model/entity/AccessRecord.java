package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class AccessRecord implements Serializable {
    /**   access_id **/
    private Integer accessId;

    /** url  access_url **/
    private String accessUrl;

    /** 参数  access_param **/
    private String accessParam;

    /** 用户id  access_user_id **/
    private Integer accessUserId;

    /** 用户名  access_user_name **/
    private String accessUserName;

    /** 访问时间  access_time **/
    private Date accessTime;

    /**   tableName: access_record   **/
    private static final long serialVersionUID = 1L;

    /**     access_id   **/
    public Integer getAccessId() {
        return accessId;
    }

    /**     access_id   **/
    public void setAccessId(Integer accessId) {
        this.accessId = accessId;
    }

    /**   url  access_url   **/
    public String getAccessUrl() {
        return accessUrl;
    }

    /**   url  access_url   **/
    public void setAccessUrl(String accessUrl) {
        this.accessUrl = accessUrl == null ? null : accessUrl.trim();
    }

    /**   参数  access_param   **/
    public String getAccessParam() {
        return accessParam;
    }

    /**   参数  access_param   **/
    public void setAccessParam(String accessParam) {
        this.accessParam = accessParam == null ? null : accessParam.trim();
    }

    /**   用户id  access_user_id   **/
    public Integer getAccessUserId() {
        return accessUserId;
    }

    /**   用户id  access_user_id   **/
    public void setAccessUserId(Integer accessUserId) {
        this.accessUserId = accessUserId;
    }

    /**   用户名  access_user_name   **/
    public String getAccessUserName() {
        return accessUserName;
    }

    /**   用户名  access_user_name   **/
    public void setAccessUserName(String accessUserName) {
        this.accessUserName = accessUserName == null ? null : accessUserName.trim();
    }

    /**   访问时间  access_time   **/
    public Date getAccessTime() {
        return accessTime;
    }

    /**   访问时间  access_time   **/
    public void setAccessTime(Date accessTime) {
        this.accessTime = accessTime;
    }
}