package com.sichuang.schedule.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AccessRecordExample {
    /**   tableName: access_record   **/
    protected String orderByClause;

    /**   tableName: access_record   **/
    protected boolean distinct;

    /**   tableName: access_record   **/
    protected List<Criteria> oredCriteria;

    /**   tableName: access_record   **/
    protected Page page;

    private Integer limit;

    private Integer offset;

    public AccessRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    /** access_record **/
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAccessIdIsNull() {
            addCriterion("access_id is null");
            return (Criteria) this;
        }

        public Criteria andAccessIdIsNotNull() {
            addCriterion("access_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccessIdEqualTo(Integer value) {
            addCriterion("access_id =", value, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdNotEqualTo(Integer value) {
            addCriterion("access_id <>", value, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdGreaterThan(Integer value) {
            addCriterion("access_id >", value, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("access_id >=", value, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdLessThan(Integer value) {
            addCriterion("access_id <", value, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdLessThanOrEqualTo(Integer value) {
            addCriterion("access_id <=", value, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdIn(List<Integer> values) {
            addCriterion("access_id in", values, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdNotIn(List<Integer> values) {
            addCriterion("access_id not in", values, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdBetween(Integer value1, Integer value2) {
            addCriterion("access_id between", value1, value2, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("access_id not between", value1, value2, "accessId");
            return (Criteria) this;
        }

        public Criteria andAccessUrlIsNull() {
            addCriterion("access_url is null");
            return (Criteria) this;
        }

        public Criteria andAccessUrlIsNotNull() {
            addCriterion("access_url is not null");
            return (Criteria) this;
        }

        public Criteria andAccessUrlEqualTo(String value) {
            addCriterion("access_url =", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlNotEqualTo(String value) {
            addCriterion("access_url <>", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlGreaterThan(String value) {
            addCriterion("access_url >", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlGreaterThanOrEqualTo(String value) {
            addCriterion("access_url >=", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlLessThan(String value) {
            addCriterion("access_url <", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlLessThanOrEqualTo(String value) {
            addCriterion("access_url <=", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlLike(String value) {
            addCriterion("access_url like", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlNotLike(String value) {
            addCriterion("access_url not like", value, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlIn(List<String> values) {
            addCriterion("access_url in", values, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlNotIn(List<String> values) {
            addCriterion("access_url not in", values, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlBetween(String value1, String value2) {
            addCriterion("access_url between", value1, value2, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessUrlNotBetween(String value1, String value2) {
            addCriterion("access_url not between", value1, value2, "accessUrl");
            return (Criteria) this;
        }

        public Criteria andAccessParamIsNull() {
            addCriterion("access_param is null");
            return (Criteria) this;
        }

        public Criteria andAccessParamIsNotNull() {
            addCriterion("access_param is not null");
            return (Criteria) this;
        }

        public Criteria andAccessParamEqualTo(String value) {
            addCriterion("access_param =", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamNotEqualTo(String value) {
            addCriterion("access_param <>", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamGreaterThan(String value) {
            addCriterion("access_param >", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamGreaterThanOrEqualTo(String value) {
            addCriterion("access_param >=", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamLessThan(String value) {
            addCriterion("access_param <", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamLessThanOrEqualTo(String value) {
            addCriterion("access_param <=", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamLike(String value) {
            addCriterion("access_param like", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamNotLike(String value) {
            addCriterion("access_param not like", value, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamIn(List<String> values) {
            addCriterion("access_param in", values, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamNotIn(List<String> values) {
            addCriterion("access_param not in", values, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamBetween(String value1, String value2) {
            addCriterion("access_param between", value1, value2, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessParamNotBetween(String value1, String value2) {
            addCriterion("access_param not between", value1, value2, "accessParam");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdIsNull() {
            addCriterion("access_user_id is null");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdIsNotNull() {
            addCriterion("access_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdEqualTo(Integer value) {
            addCriterion("access_user_id =", value, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdNotEqualTo(Integer value) {
            addCriterion("access_user_id <>", value, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdGreaterThan(Integer value) {
            addCriterion("access_user_id >", value, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("access_user_id >=", value, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdLessThan(Integer value) {
            addCriterion("access_user_id <", value, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("access_user_id <=", value, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdIn(List<Integer> values) {
            addCriterion("access_user_id in", values, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdNotIn(List<Integer> values) {
            addCriterion("access_user_id not in", values, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdBetween(Integer value1, Integer value2) {
            addCriterion("access_user_id between", value1, value2, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("access_user_id not between", value1, value2, "accessUserId");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameIsNull() {
            addCriterion("access_user_name is null");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameIsNotNull() {
            addCriterion("access_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameEqualTo(String value) {
            addCriterion("access_user_name =", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameNotEqualTo(String value) {
            addCriterion("access_user_name <>", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameGreaterThan(String value) {
            addCriterion("access_user_name >", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("access_user_name >=", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameLessThan(String value) {
            addCriterion("access_user_name <", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameLessThanOrEqualTo(String value) {
            addCriterion("access_user_name <=", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameLike(String value) {
            addCriterion("access_user_name like", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameNotLike(String value) {
            addCriterion("access_user_name not like", value, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameIn(List<String> values) {
            addCriterion("access_user_name in", values, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameNotIn(List<String> values) {
            addCriterion("access_user_name not in", values, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameBetween(String value1, String value2) {
            addCriterion("access_user_name between", value1, value2, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessUserNameNotBetween(String value1, String value2) {
            addCriterion("access_user_name not between", value1, value2, "accessUserName");
            return (Criteria) this;
        }

        public Criteria andAccessTimeIsNull() {
            addCriterion("access_time is null");
            return (Criteria) this;
        }

        public Criteria andAccessTimeIsNotNull() {
            addCriterion("access_time is not null");
            return (Criteria) this;
        }

        public Criteria andAccessTimeEqualTo(Date value) {
            addCriterion("access_time =", value, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeNotEqualTo(Date value) {
            addCriterion("access_time <>", value, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeGreaterThan(Date value) {
            addCriterion("access_time >", value, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("access_time >=", value, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeLessThan(Date value) {
            addCriterion("access_time <", value, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeLessThanOrEqualTo(Date value) {
            addCriterion("access_time <=", value, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeIn(List<Date> values) {
            addCriterion("access_time in", values, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeNotIn(List<Date> values) {
            addCriterion("access_time not in", values, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeBetween(Date value1, Date value2) {
            addCriterion("access_time between", value1, value2, "accessTime");
            return (Criteria) this;
        }

        public Criteria andAccessTimeNotBetween(Date value1, Date value2) {
            addCriterion("access_time not between", value1, value2, "accessTime");
            return (Criteria) this;
        }
    }

    /**  tableName: access_record   **/
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /** access_record **/
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}