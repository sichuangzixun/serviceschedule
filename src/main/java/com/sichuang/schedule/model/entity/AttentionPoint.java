package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class AttentionPoint implements Serializable {
    /**   attention_point_id **/
    private Integer attentionPointId;

    /** 录入日期  begin_date **/
    private Date beginDate;

    /** 终止日期  end_date **/
    private Date endDate;

    /** 类型：1-长期，2-短期  point_type **/
    private Integer pointType;

    /** 注意事项内容  point_content **/
    private String pointContent;

    /** 录入班次：0为无  add_shift_type **/
    private Integer addShiftType;

    /** 添加时间  add_time **/
    private Date addTime;

    /** 添加人  add_man_name **/
    private String addManName;

    /** 是否已删除：0-否，1-是  is_delete **/
    private Boolean isDelete;

    /** 标签id  item_type_id **/
    private Integer itemTypeId;

    /** 标签  item_type_names **/
    private String itemTypeNames;

    /** 终止理由  end_information **/
    private String endInformation;

    /** 交接班次id  add_shift_record_id **/
    private Integer addShiftRecordId;

    /**   tableName: attention_point   **/
    private static final long serialVersionUID = 1L;

    /**     attention_point_id   **/
    public Integer getAttentionPointId() {
        return attentionPointId;
    }

    /**     attention_point_id   **/
    public void setAttentionPointId(Integer attentionPointId) {
        this.attentionPointId = attentionPointId;
    }

    /**   录入日期  begin_date   **/
    public Date getBeginDate() {
        return beginDate;
    }

    /**   录入日期  begin_date   **/
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    /**   终止日期  end_date   **/
    public Date getEndDate() {
        return endDate;
    }

    /**   终止日期  end_date   **/
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**   类型：1-长期，2-短期  point_type   **/
    public Integer getPointType() {
        return pointType;
    }

    /**   类型：1-长期，2-短期  point_type   **/
    public void setPointType(Integer pointType) {
        this.pointType = pointType;
    }

    /**   注意事项内容  point_content   **/
    public String getPointContent() {
        return pointContent;
    }

    /**   注意事项内容  point_content   **/
    public void setPointContent(String pointContent) {
        this.pointContent = pointContent == null ? null : pointContent.trim();
    }

    /**   录入班次：0为无  add_shift_type   **/
    public Integer getAddShiftType() {
        return addShiftType;
    }

    /**   录入班次：0为无  add_shift_type   **/
    public void setAddShiftType(Integer addShiftType) {
        this.addShiftType = addShiftType;
    }

    /**   添加时间  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   添加时间  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加人  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }

    /**   是否已删除：0-否，1-是  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否已删除：0-否，1-是  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   标签id  item_type_id   **/
    public Integer getItemTypeId() {
        return itemTypeId;
    }

    /**   标签id  item_type_id   **/
    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    /**   标签  item_type_names   **/
    public String getItemTypeNames() {
        return itemTypeNames;
    }

    /**   标签  item_type_names   **/
    public void setItemTypeNames(String itemTypeNames) {
        this.itemTypeNames = itemTypeNames == null ? null : itemTypeNames.trim();
    }

    /**   终止理由  end_information   **/
    public String getEndInformation() {
        return endInformation;
    }

    /**   终止理由  end_information   **/
    public void setEndInformation(String endInformation) {
        this.endInformation = endInformation == null ? null : endInformation.trim();
    }

    /**   交接班次id  add_shift_record_id   **/
    public Integer getAddShiftRecordId() {
        return addShiftRecordId;
    }

    /**   交接班次id  add_shift_record_id   **/
    public void setAddShiftRecordId(Integer addShiftRecordId) {
        this.addShiftRecordId = addShiftRecordId;
    }
}