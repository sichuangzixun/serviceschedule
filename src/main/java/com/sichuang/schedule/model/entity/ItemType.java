package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class ItemType implements Serializable {
    /**   type_id **/
    private Integer typeId;

    /** 名称  type_name **/
    private String typeName;

    /** 是否删除：0-否，1-是  is_delete **/
    private Boolean isDelete;

    /** 基础数据类型：1-标签，2-值班员  data_type **/
    private Integer dataType;

    /** 添加时间  add_time **/
    private Date addTime;

    /** 添加人  add_man_name **/
    private String addManName;

    /**   tableName: item_type   **/
    private static final long serialVersionUID = 1L;

    /**     type_id   **/
    public Integer getTypeId() {
        return typeId;
    }

    /**     type_id   **/
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    /**   名称  type_name   **/
    public String getTypeName() {
        return typeName;
    }

    /**   名称  type_name   **/
    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }

    /**   是否删除：0-否，1-是  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否删除：0-否，1-是  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   基础数据类型：1-标签，2-值班员  data_type   **/
    public Integer getDataType() {
        return dataType;
    }

    /**   基础数据类型：1-标签，2-值班员  data_type   **/
    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    /**   添加时间  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   添加时间  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加人  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }
}