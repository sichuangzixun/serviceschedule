package com.sichuang.schedule.model.entity;

import java.io.Serializable;

public final class Page implements Serializable {

	/**
	 * 默认的序列化版本 id.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 分页查询开始记录位置.
	 */
	private int begin = 0;
	/**
	 * 分页查看下结束位置.
	 */
	private int end;
	/**
	 * 每页显示记录数默认20，长度小于等于0，不分页查询全部，只有一页
	 */
	private int length = 20;
	/**
	 * 查询结果总记录数.
	 */
	private int totalRecords;
	/**
	 * 当前页码.从一开始
	 */
	private int pageNo = 1;
	/**
	 * 总共页数.
	 */
	private int pageCount;

	/**
	 * 构造函数.
	 *
	 * @param pageNo
	 * @param length
	 */
	public Page(int pageNo, int length) {
		pageNo = pageNo > 0 ? pageNo : 1;
		this.begin = (pageNo - 1) * length;
		this.length = length;
		this.end = this.begin + this.length;
		this.pageNo = pageNo;
	}

	/**
	 * @param pageNo
	 * @param length
	 * @param totalRecords
	 */
	public Page(int pageNo, int length, int totalRecords) {
		this(pageNo, length);
		this.totalRecords = totalRecords;
	}

	/**
	 * 设置页数，自动计算数据范围.
	 *
	 */
	public Page(int pageNo) {
		this(pageNo, 20);
	}

	public Page() {
	}

	public int getBegin() {
		return begin;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setBegin(int begin) {
		this.begin = begin;
		if (this.length > 0) {
			this.pageNo = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
		}else {
			// 查询长度小于等于0，查询全部，不分页只有一页
			this.pageNo = 1;
		}
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
		if (this.length > 0) {
			if (this.begin != 0) {
				this.pageNo = (int) Math.floor((this.begin * 1.0d) / this.length) + 1;
			}
		} else {
			// 查询长度小于等于0，查询全部，不分页只有一页
			this.pageNo = 1;
		}
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
		if (this.length > 0) {
			this.pageCount = (int) Math.floor((this.totalRecords * 1.0d) / this.length);
			if (this.totalRecords % this.length != 0) {
				this.pageCount++;
			}
		} else {
			// 查询长度小于等于0，查询全部，不分页只有一页
			this.pageCount = 1;
		}
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
		pageNo = pageNo > 0 ? pageNo : 1;
		this.begin = this.length * (pageNo - 1);
		this.end = this.length * pageNo;
	}

	public int getPageCount() {
		if (pageCount == 0) {
			return 1;
		}
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
}