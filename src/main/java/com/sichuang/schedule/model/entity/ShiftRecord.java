package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class ShiftRecord implements Serializable {
    /**   shift_record_id **/
    private Integer shiftRecordId;

    /** 值班日期。不能为空  duty_date **/
    private Date dutyDate;

    /** 交接时间。白班没有交接时间。  shift_time **/
    private Date shiftTime;

    /** 值班班次:1-白班，2-早班，3-夜班，4-宵班。不能为空  shift_type **/
    private Integer shiftType;

    /** 添加时间。不能为空  add_time **/
    private Date addTime;

    /** 添加人。不能为空  add_man_name **/
    private String addManName;

    /** 是否已删除：0-否，1-是。不能为空  is_delete **/
    private Boolean isDelete;

    /** 现场负责人  duty_officer **/
    private String dutyOfficer;

    /** 交接状态：1-未交接，2-已交接  shift_status **/
    private Integer shiftStatus;

    /** 全部值班员  workers **/
    private String workers;

    /**   tableName: shift_record   **/
    private static final long serialVersionUID = 1L;

    /**     shift_record_id   **/
    public Integer getShiftRecordId() {
        return shiftRecordId;
    }

    /**     shift_record_id   **/
    public void setShiftRecordId(Integer shiftRecordId) {
        this.shiftRecordId = shiftRecordId;
    }

    /**   值班日期。不能为空  duty_date   **/
    public Date getDutyDate() {
        return dutyDate;
    }

    /**   值班日期。不能为空  duty_date   **/
    public void setDutyDate(Date dutyDate) {
        this.dutyDate = dutyDate;
    }

    /**   交接时间。白班没有交接时间。  shift_time   **/
    public Date getShiftTime() {
        return shiftTime;
    }

    /**   交接时间。白班没有交接时间。  shift_time   **/
    public void setShiftTime(Date shiftTime) {
        this.shiftTime = shiftTime;
    }

    /**   值班班次:1-白班，2-早班，3-夜班，4-宵班。不能为空  shift_type   **/
    public Integer getShiftType() {
        return shiftType;
    }

    /**   值班班次:1-白班，2-早班，3-夜班，4-宵班。不能为空  shift_type   **/
    public void setShiftType(Integer shiftType) {
        this.shiftType = shiftType;
    }

    /**   添加时间。不能为空  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   添加时间。不能为空  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加人。不能为空  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人。不能为空  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }

    /**   是否已删除：0-否，1-是。不能为空  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否已删除：0-否，1-是。不能为空  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   现场负责人  duty_officer   **/
    public String getDutyOfficer() {
        return dutyOfficer;
    }

    /**   现场负责人  duty_officer   **/
    public void setDutyOfficer(String dutyOfficer) {
        this.dutyOfficer = dutyOfficer == null ? null : dutyOfficer.trim();
    }

    /**   交接状态：1-未交接，2-已交接  shift_status   **/
    public Integer getShiftStatus() {
        return shiftStatus;
    }

    /**   交接状态：1-未交接，2-已交接  shift_status   **/
    public void setShiftStatus(Integer shiftStatus) {
        this.shiftStatus = shiftStatus;
    }

    /**   全部值班员  workers   **/
    public String getWorkers() {
        return workers;
    }

    /**   全部值班员  workers   **/
    public void setWorkers(String workers) {
        this.workers = workers == null ? null : workers.trim();
    }
}