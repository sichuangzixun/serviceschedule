package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    /**   user_id **/
    private Integer userId;

    /** 用户名  login_name **/
    private String loginName;

    /** 用户密码  password **/
    private String password;

    /** 用户姓名  user_name **/
    private String userName;

    /** 用户类型：1-管理员，2-普通用户  user_type **/
    private Integer userType;

    /** 是否删除：0-否，1-是  is_delete **/
    private Boolean isDelete;

    /** 添加时间  add_time **/
    private Date addTime;

    /** 添加人  add_man_name **/
    private String addManName;

    /**   tableName: user   **/
    private static final long serialVersionUID = 1L;

    /**     user_id   **/
    public Integer getUserId() {
        return userId;
    }

    /**     user_id   **/
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**   用户名  login_name   **/
    public String getLoginName() {
        return loginName;
    }

    /**   用户名  login_name   **/
    public void setLoginName(String loginName) {
        this.loginName = loginName == null ? null : loginName.trim();
    }

    /**   用户密码  password   **/
    public String getPassword() {
        return password;
    }

    /**   用户密码  password   **/
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**   用户姓名  user_name   **/
    public String getUserName() {
        return userName;
    }

    /**   用户姓名  user_name   **/
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**   用户类型：1-管理员，2-普通用户  user_type   **/
    public Integer getUserType() {
        return userType;
    }

    /**   用户类型：1-管理员，2-普通用户  user_type   **/
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**   是否删除：0-否，1-是  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否删除：0-否，1-是  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   添加时间  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   添加时间  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加人  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }
}