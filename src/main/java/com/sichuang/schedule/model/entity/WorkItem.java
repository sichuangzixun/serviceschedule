package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class WorkItem implements Serializable {
    /**   work_item_id **/
    private Integer workItemId;

    /** 工作内容  work_content **/
    private String workContent;

    /** 是否已完成  is_complete **/
    private Boolean isComplete;

    /** 完成班次：0为无。  end_shift_type **/
    private Integer endShiftType;

    /** 完成时间  end_time **/
    private Date endTime;

    /** 完成人  end_handler_name **/
    private String endHandlerName;

    /** 录入班次:0为无  add_shift_type **/
    private Integer addShiftType;

    /** 录入时间  add_time **/
    private Date addTime;

    /** 添加人  add_man_name **/
    private String addManName;

    /** 标签id  item_type_id **/
    private Integer itemTypeId;

    /** 标签：诉求、停电、其他  item_type_names **/
    private String itemTypeNames;

    /** 是否已删除：0-否，1-是  is_delete **/
    private Boolean isDelete;

    /** 交接班次id  add_shift_record_id **/
    private Integer addShiftRecordId;

    /** 交接状态：0-未交接，1-已交接  is_shift **/
    private Boolean isShift;

    /** 处理情况。每个班次输入叠加。格式："日期班次：处理情况。"  handle_information **/
    private String handleInformation;

    /**   tableName: work_item   **/
    private static final long serialVersionUID = 1L;

    /**     work_item_id   **/
    public Integer getWorkItemId() {
        return workItemId;
    }

    /**     work_item_id   **/
    public void setWorkItemId(Integer workItemId) {
        this.workItemId = workItemId;
    }

    /**   工作内容  work_content   **/
    public String getWorkContent() {
        return workContent;
    }

    /**   工作内容  work_content   **/
    public void setWorkContent(String workContent) {
        this.workContent = workContent == null ? null : workContent.trim();
    }

    /**   是否已完成  is_complete   **/
    public Boolean getIsComplete() {
        return isComplete;
    }

    /**   是否已完成  is_complete   **/
    public void setIsComplete(Boolean isComplete) {
        this.isComplete = isComplete;
    }

    /**   完成班次：0为无。  end_shift_type   **/
    public Integer getEndShiftType() {
        return endShiftType;
    }

    /**   完成班次：0为无。  end_shift_type   **/
    public void setEndShiftType(Integer endShiftType) {
        this.endShiftType = endShiftType;
    }

    /**   完成时间  end_time   **/
    public Date getEndTime() {
        return endTime;
    }

    /**   完成时间  end_time   **/
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**   完成人  end_handler_name   **/
    public String getEndHandlerName() {
        return endHandlerName;
    }

    /**   完成人  end_handler_name   **/
    public void setEndHandlerName(String endHandlerName) {
        this.endHandlerName = endHandlerName == null ? null : endHandlerName.trim();
    }

    /**   录入班次:0为无  add_shift_type   **/
    public Integer getAddShiftType() {
        return addShiftType;
    }

    /**   录入班次:0为无  add_shift_type   **/
    public void setAddShiftType(Integer addShiftType) {
        this.addShiftType = addShiftType;
    }

    /**   录入时间  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   录入时间  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加人  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }

    /**   标签id  item_type_id   **/
    public Integer getItemTypeId() {
        return itemTypeId;
    }

    /**   标签id  item_type_id   **/
    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    /**   标签：诉求、停电、其他  item_type_names   **/
    public String getItemTypeNames() {
        return itemTypeNames;
    }

    /**   标签：诉求、停电、其他  item_type_names   **/
    public void setItemTypeNames(String itemTypeNames) {
        this.itemTypeNames = itemTypeNames == null ? null : itemTypeNames.trim();
    }

    /**   是否已删除：0-否，1-是  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否已删除：0-否，1-是  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   交接班次id  add_shift_record_id   **/
    public Integer getAddShiftRecordId() {
        return addShiftRecordId;
    }

    /**   交接班次id  add_shift_record_id   **/
    public void setAddShiftRecordId(Integer addShiftRecordId) {
        this.addShiftRecordId = addShiftRecordId;
    }

    /**   交接状态：0-未交接，1-已交接  is_shift   **/
    public Boolean getIsShift() {
        return isShift;
    }

    /**   交接状态：0-未交接，1-已交接  is_shift   **/
    public void setIsShift(Boolean isShift) {
        this.isShift = isShift;
    }

    /**   处理情况。每个班次输入叠加。格式："日期班次：处理情况。"  handle_information   **/
    public String getHandleInformation() {
        return handleInformation;
    }

    /**   处理情况。每个班次输入叠加。格式："日期班次：处理情况。"  handle_information   **/
    public void setHandleInformation(String handleInformation) {
        this.handleInformation = handleInformation == null ? null : handleInformation.trim();
    }
}