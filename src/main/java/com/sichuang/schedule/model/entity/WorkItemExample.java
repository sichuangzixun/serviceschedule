package com.sichuang.schedule.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkItemExample {
    /**   tableName: work_item   **/
    protected String orderByClause;

    /**   tableName: work_item   **/
    protected boolean distinct;

    /**   tableName: work_item   **/
    protected List<Criteria> oredCriteria;

    /**   tableName: work_item   **/
    protected Page page;

    private Integer limit;

    private Integer offset;

    public WorkItemExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    /** work_item **/
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andWorkItemIdIsNull() {
            addCriterion("work_item_id is null");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdIsNotNull() {
            addCriterion("work_item_id is not null");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdEqualTo(Integer value) {
            addCriterion("work_item_id =", value, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdNotEqualTo(Integer value) {
            addCriterion("work_item_id <>", value, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdGreaterThan(Integer value) {
            addCriterion("work_item_id >", value, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("work_item_id >=", value, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdLessThan(Integer value) {
            addCriterion("work_item_id <", value, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdLessThanOrEqualTo(Integer value) {
            addCriterion("work_item_id <=", value, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdIn(List<Integer> values) {
            addCriterion("work_item_id in", values, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdNotIn(List<Integer> values) {
            addCriterion("work_item_id not in", values, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdBetween(Integer value1, Integer value2) {
            addCriterion("work_item_id between", value1, value2, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkItemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("work_item_id not between", value1, value2, "workItemId");
            return (Criteria) this;
        }

        public Criteria andWorkContentIsNull() {
            addCriterion("work_content is null");
            return (Criteria) this;
        }

        public Criteria andWorkContentIsNotNull() {
            addCriterion("work_content is not null");
            return (Criteria) this;
        }

        public Criteria andWorkContentEqualTo(String value) {
            addCriterion("work_content =", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentNotEqualTo(String value) {
            addCriterion("work_content <>", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentGreaterThan(String value) {
            addCriterion("work_content >", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentGreaterThanOrEqualTo(String value) {
            addCriterion("work_content >=", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentLessThan(String value) {
            addCriterion("work_content <", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentLessThanOrEqualTo(String value) {
            addCriterion("work_content <=", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentLike(String value) {
            addCriterion("work_content like", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentNotLike(String value) {
            addCriterion("work_content not like", value, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentIn(List<String> values) {
            addCriterion("work_content in", values, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentNotIn(List<String> values) {
            addCriterion("work_content not in", values, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentBetween(String value1, String value2) {
            addCriterion("work_content between", value1, value2, "workContent");
            return (Criteria) this;
        }

        public Criteria andWorkContentNotBetween(String value1, String value2) {
            addCriterion("work_content not between", value1, value2, "workContent");
            return (Criteria) this;
        }

        public Criteria andIsCompleteIsNull() {
            addCriterion("is_complete is null");
            return (Criteria) this;
        }

        public Criteria andIsCompleteIsNotNull() {
            addCriterion("is_complete is not null");
            return (Criteria) this;
        }

        public Criteria andIsCompleteEqualTo(Boolean value) {
            addCriterion("is_complete =", value, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteNotEqualTo(Boolean value) {
            addCriterion("is_complete <>", value, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteGreaterThan(Boolean value) {
            addCriterion("is_complete >", value, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_complete >=", value, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteLessThan(Boolean value) {
            addCriterion("is_complete <", value, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteLessThanOrEqualTo(Boolean value) {
            addCriterion("is_complete <=", value, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteIn(List<Boolean> values) {
            addCriterion("is_complete in", values, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteNotIn(List<Boolean> values) {
            addCriterion("is_complete not in", values, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteBetween(Boolean value1, Boolean value2) {
            addCriterion("is_complete between", value1, value2, "isComplete");
            return (Criteria) this;
        }

        public Criteria andIsCompleteNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_complete not between", value1, value2, "isComplete");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeIsNull() {
            addCriterion("end_shift_type is null");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeIsNotNull() {
            addCriterion("end_shift_type is not null");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeEqualTo(Integer value) {
            addCriterion("end_shift_type =", value, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeNotEqualTo(Integer value) {
            addCriterion("end_shift_type <>", value, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeGreaterThan(Integer value) {
            addCriterion("end_shift_type >", value, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("end_shift_type >=", value, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeLessThan(Integer value) {
            addCriterion("end_shift_type <", value, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeLessThanOrEqualTo(Integer value) {
            addCriterion("end_shift_type <=", value, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeIn(List<Integer> values) {
            addCriterion("end_shift_type in", values, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeNotIn(List<Integer> values) {
            addCriterion("end_shift_type not in", values, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeBetween(Integer value1, Integer value2) {
            addCriterion("end_shift_type between", value1, value2, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndShiftTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("end_shift_type not between", value1, value2, "endShiftType");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameIsNull() {
            addCriterion("end_handler_name is null");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameIsNotNull() {
            addCriterion("end_handler_name is not null");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameEqualTo(String value) {
            addCriterion("end_handler_name =", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameNotEqualTo(String value) {
            addCriterion("end_handler_name <>", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameGreaterThan(String value) {
            addCriterion("end_handler_name >", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameGreaterThanOrEqualTo(String value) {
            addCriterion("end_handler_name >=", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameLessThan(String value) {
            addCriterion("end_handler_name <", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameLessThanOrEqualTo(String value) {
            addCriterion("end_handler_name <=", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameLike(String value) {
            addCriterion("end_handler_name like", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameNotLike(String value) {
            addCriterion("end_handler_name not like", value, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameIn(List<String> values) {
            addCriterion("end_handler_name in", values, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameNotIn(List<String> values) {
            addCriterion("end_handler_name not in", values, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameBetween(String value1, String value2) {
            addCriterion("end_handler_name between", value1, value2, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andEndHandlerNameNotBetween(String value1, String value2) {
            addCriterion("end_handler_name not between", value1, value2, "endHandlerName");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeIsNull() {
            addCriterion("add_shift_type is null");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeIsNotNull() {
            addCriterion("add_shift_type is not null");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeEqualTo(Integer value) {
            addCriterion("add_shift_type =", value, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeNotEqualTo(Integer value) {
            addCriterion("add_shift_type <>", value, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeGreaterThan(Integer value) {
            addCriterion("add_shift_type >", value, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("add_shift_type >=", value, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeLessThan(Integer value) {
            addCriterion("add_shift_type <", value, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeLessThanOrEqualTo(Integer value) {
            addCriterion("add_shift_type <=", value, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeIn(List<Integer> values) {
            addCriterion("add_shift_type in", values, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeNotIn(List<Integer> values) {
            addCriterion("add_shift_type not in", values, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeBetween(Integer value1, Integer value2) {
            addCriterion("add_shift_type between", value1, value2, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddShiftTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("add_shift_type not between", value1, value2, "addShiftType");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddManNameIsNull() {
            addCriterion("add_man_name is null");
            return (Criteria) this;
        }

        public Criteria andAddManNameIsNotNull() {
            addCriterion("add_man_name is not null");
            return (Criteria) this;
        }

        public Criteria andAddManNameEqualTo(String value) {
            addCriterion("add_man_name =", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotEqualTo(String value) {
            addCriterion("add_man_name <>", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameGreaterThan(String value) {
            addCriterion("add_man_name >", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameGreaterThanOrEqualTo(String value) {
            addCriterion("add_man_name >=", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameLessThan(String value) {
            addCriterion("add_man_name <", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameLessThanOrEqualTo(String value) {
            addCriterion("add_man_name <=", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameLike(String value) {
            addCriterion("add_man_name like", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotLike(String value) {
            addCriterion("add_man_name not like", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameIn(List<String> values) {
            addCriterion("add_man_name in", values, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotIn(List<String> values) {
            addCriterion("add_man_name not in", values, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameBetween(String value1, String value2) {
            addCriterion("add_man_name between", value1, value2, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotBetween(String value1, String value2) {
            addCriterion("add_man_name not between", value1, value2, "addManName");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdIsNull() {
            addCriterion("item_type_id is null");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdIsNotNull() {
            addCriterion("item_type_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdEqualTo(Integer value) {
            addCriterion("item_type_id =", value, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdNotEqualTo(Integer value) {
            addCriterion("item_type_id <>", value, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdGreaterThan(Integer value) {
            addCriterion("item_type_id >", value, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("item_type_id >=", value, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdLessThan(Integer value) {
            addCriterion("item_type_id <", value, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdLessThanOrEqualTo(Integer value) {
            addCriterion("item_type_id <=", value, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdIn(List<Integer> values) {
            addCriterion("item_type_id in", values, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdNotIn(List<Integer> values) {
            addCriterion("item_type_id not in", values, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdBetween(Integer value1, Integer value2) {
            addCriterion("item_type_id between", value1, value2, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("item_type_id not between", value1, value2, "itemTypeId");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesIsNull() {
            addCriterion("item_type_names is null");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesIsNotNull() {
            addCriterion("item_type_names is not null");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesEqualTo(String value) {
            addCriterion("item_type_names =", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesNotEqualTo(String value) {
            addCriterion("item_type_names <>", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesGreaterThan(String value) {
            addCriterion("item_type_names >", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesGreaterThanOrEqualTo(String value) {
            addCriterion("item_type_names >=", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesLessThan(String value) {
            addCriterion("item_type_names <", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesLessThanOrEqualTo(String value) {
            addCriterion("item_type_names <=", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesLike(String value) {
            addCriterion("item_type_names like", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesNotLike(String value) {
            addCriterion("item_type_names not like", value, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesIn(List<String> values) {
            addCriterion("item_type_names in", values, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesNotIn(List<String> values) {
            addCriterion("item_type_names not in", values, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesBetween(String value1, String value2) {
            addCriterion("item_type_names between", value1, value2, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andItemTypeNamesNotBetween(String value1, String value2) {
            addCriterion("item_type_names not between", value1, value2, "itemTypeNames");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("is_delete is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("is_delete is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Boolean value) {
            addCriterion("is_delete =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Boolean value) {
            addCriterion("is_delete <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Boolean value) {
            addCriterion("is_delete >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_delete >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Boolean value) {
            addCriterion("is_delete <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Boolean value) {
            addCriterion("is_delete <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Boolean> values) {
            addCriterion("is_delete in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Boolean> values) {
            addCriterion("is_delete not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete not between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdIsNull() {
            addCriterion("add_shift_record_id is null");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdIsNotNull() {
            addCriterion("add_shift_record_id is not null");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdEqualTo(Integer value) {
            addCriterion("add_shift_record_id =", value, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdNotEqualTo(Integer value) {
            addCriterion("add_shift_record_id <>", value, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdGreaterThan(Integer value) {
            addCriterion("add_shift_record_id >", value, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("add_shift_record_id >=", value, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdLessThan(Integer value) {
            addCriterion("add_shift_record_id <", value, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("add_shift_record_id <=", value, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdIn(List<Integer> values) {
            addCriterion("add_shift_record_id in", values, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdNotIn(List<Integer> values) {
            addCriterion("add_shift_record_id not in", values, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("add_shift_record_id between", value1, value2, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andAddShiftRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("add_shift_record_id not between", value1, value2, "addShiftRecordId");
            return (Criteria) this;
        }

        public Criteria andIsShiftIsNull() {
            addCriterion("is_shift is null");
            return (Criteria) this;
        }

        public Criteria andIsShiftIsNotNull() {
            addCriterion("is_shift is not null");
            return (Criteria) this;
        }

        public Criteria andIsShiftEqualTo(Boolean value) {
            addCriterion("is_shift =", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftNotEqualTo(Boolean value) {
            addCriterion("is_shift <>", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftGreaterThan(Boolean value) {
            addCriterion("is_shift >", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_shift >=", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftLessThan(Boolean value) {
            addCriterion("is_shift <", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftLessThanOrEqualTo(Boolean value) {
            addCriterion("is_shift <=", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftIn(List<Boolean> values) {
            addCriterion("is_shift in", values, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftNotIn(List<Boolean> values) {
            addCriterion("is_shift not in", values, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftBetween(Boolean value1, Boolean value2) {
            addCriterion("is_shift between", value1, value2, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_shift not between", value1, value2, "isShift");
            return (Criteria) this;
        }
    }

    /**  tableName: work_item   **/
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /** work_item **/
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}