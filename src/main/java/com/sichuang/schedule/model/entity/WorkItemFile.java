package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class WorkItemFile implements Serializable {
    /** 项目资料ID  file_id **/
    private Integer fileId;

    /** 资料名称  file_name **/
    private String fileName;

    /** /file/type/+文件名（用户id+recordId+时间戳+批量上传序号）  file_url **/
    private String fileUrl;

    /** 上传时间  add_time **/
    private Date addTime;

    /** 添加人姓名  add_man_name **/
    private String addManName;

    /** 是否删除：0-未删除，1-已删除  is_delete **/
    private Boolean isDelete;

    /** 交接班次id  add_shift_record_id **/
    private Integer addShiftRecordId;

    /** 关联记录id。work_item_id 、 attention_point_id  record_id **/
    private Integer recordId;

    /** 类型：1-工单，2-通知, 3-排班表  file_type **/
    private Integer fileType;

    /** 交接状态：0-未交接，1-已交接  is_shift **/
    private Boolean isShift;

    /**   tableName: work_item_file   **/
    private static final long serialVersionUID = 1L;

    /**   项目资料ID  file_id   **/
    public Integer getFileId() {
        return fileId;
    }

    /**   项目资料ID  file_id   **/
    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    /**   资料名称  file_name   **/
    public String getFileName() {
        return fileName;
    }

    /**   资料名称  file_name   **/
    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }

    /**   /file/type/+文件名（用户id+recordId+时间戳+批量上传序号）  file_url   **/
    public String getFileUrl() {
        return fileUrl;
    }

    /**   /file/type/+文件名（用户id+recordId+时间戳+批量上传序号）  file_url   **/
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    /**   上传时间  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   上传时间  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加人姓名  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人姓名  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }

    /**   是否删除：0-未删除，1-已删除  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否删除：0-未删除，1-已删除  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   交接班次id  add_shift_record_id   **/
    public Integer getAddShiftRecordId() {
        return addShiftRecordId;
    }

    /**   交接班次id  add_shift_record_id   **/
    public void setAddShiftRecordId(Integer addShiftRecordId) {
        this.addShiftRecordId = addShiftRecordId;
    }

    /**   关联记录id。work_item_id 、 attention_point_id  record_id   **/
    public Integer getRecordId() {
        return recordId;
    }

    /**   关联记录id。work_item_id 、 attention_point_id  record_id   **/
    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    /**   类型：1-工单，2-通知, 3-排班表  file_type   **/
    public Integer getFileType() {
        return fileType;
    }

    /**   类型：1-工单，2-通知, 3-排班表  file_type   **/
    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    /**   交接状态：0-未交接，1-已交接  is_shift   **/
    public Boolean getIsShift() {
        return isShift;
    }

    /**   交接状态：0-未交接，1-已交接  is_shift   **/
    public void setIsShift(Boolean isShift) {
        this.isShift = isShift;
    }
}