package com.sichuang.schedule.model.entity;

import java.io.Serializable;
import java.util.Date;

public class WorkRecord implements Serializable {
    /** 当天交接班记录id。  work_record_id **/
    private Integer workRecordId;

    /** 值班员。不能为空  watcher **/
    private String watcher;

    /** 值班班次：0-无，1-白班，2-早班，3-夜班，4-宵班。不能为空  shift_type **/
    private Integer shiftType;

    /** 添加时间。不能为空  add_time **/
    private Date addTime;

    /** 助理负责人：0-否，1-是。不能为空  is_manager **/
    private Boolean isManager;

    /** 添加人。不能为空  add_man_name **/
    private String addManName;

    /** 是否已删除：0-否，1-是。不能为空  is_delete **/
    private Boolean isDelete;

    /** 排班记录id。不能为空  shift_record_id **/
    private Integer shiftRecordId;

    /** 值班日期。不能为空  duty_date **/
    private Date dutyDate;

    /** 交接状态：0-未交接，1-已交接  is_shift **/
    private Boolean isShift;

    /**   tableName: work_record   **/
    private static final long serialVersionUID = 1L;

    /**   当天交接班记录id。  work_record_id   **/
    public Integer getWorkRecordId() {
        return workRecordId;
    }

    /**   当天交接班记录id。  work_record_id   **/
    public void setWorkRecordId(Integer workRecordId) {
        this.workRecordId = workRecordId;
    }

    /**   值班员。不能为空  watcher   **/
    public String getWatcher() {
        return watcher;
    }

    /**   值班员。不能为空  watcher   **/
    public void setWatcher(String watcher) {
        this.watcher = watcher == null ? null : watcher.trim();
    }

    /**   值班班次：0-无，1-白班，2-早班，3-夜班，4-宵班。不能为空  shift_type   **/
    public Integer getShiftType() {
        return shiftType;
    }

    /**   值班班次：0-无，1-白班，2-早班，3-夜班，4-宵班。不能为空  shift_type   **/
    public void setShiftType(Integer shiftType) {
        this.shiftType = shiftType;
    }

    /**   添加时间。不能为空  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   添加时间。不能为空  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   助理负责人：0-否，1-是。不能为空  is_manager   **/
    public Boolean getIsManager() {
        return isManager;
    }

    /**   助理负责人：0-否，1-是。不能为空  is_manager   **/
    public void setIsManager(Boolean isManager) {
        this.isManager = isManager;
    }

    /**   添加人。不能为空  add_man_name   **/
    public String getAddManName() {
        return addManName;
    }

    /**   添加人。不能为空  add_man_name   **/
    public void setAddManName(String addManName) {
        this.addManName = addManName == null ? null : addManName.trim();
    }

    /**   是否已删除：0-否，1-是。不能为空  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   是否已删除：0-否，1-是。不能为空  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   排班记录id。不能为空  shift_record_id   **/
    public Integer getShiftRecordId() {
        return shiftRecordId;
    }

    /**   排班记录id。不能为空  shift_record_id   **/
    public void setShiftRecordId(Integer shiftRecordId) {
        this.shiftRecordId = shiftRecordId;
    }

    /**   值班日期。不能为空  duty_date   **/
    public Date getDutyDate() {
        return dutyDate;
    }

    /**   值班日期。不能为空  duty_date   **/
    public void setDutyDate(Date dutyDate) {
        this.dutyDate = dutyDate;
    }

    /**   交接状态：0-未交接，1-已交接  is_shift   **/
    public Boolean getIsShift() {
        return isShift;
    }

    /**   交接状态：0-未交接，1-已交接  is_shift   **/
    public void setIsShift(Boolean isShift) {
        this.isShift = isShift;
    }
}