package com.sichuang.schedule.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkRecordExample {
    /**   tableName: work_record   **/
    protected String orderByClause;

    /**   tableName: work_record   **/
    protected boolean distinct;

    /**   tableName: work_record   **/
    protected List<Criteria> oredCriteria;

    /**   tableName: work_record   **/
    protected Page page;

    private Integer limit;

    private Integer offset;

    public WorkRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    /** work_record **/
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andWorkRecordIdIsNull() {
            addCriterion("work_record_id is null");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdIsNotNull() {
            addCriterion("work_record_id is not null");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdEqualTo(Integer value) {
            addCriterion("work_record_id =", value, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdNotEqualTo(Integer value) {
            addCriterion("work_record_id <>", value, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdGreaterThan(Integer value) {
            addCriterion("work_record_id >", value, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("work_record_id >=", value, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdLessThan(Integer value) {
            addCriterion("work_record_id <", value, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("work_record_id <=", value, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdIn(List<Integer> values) {
            addCriterion("work_record_id in", values, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdNotIn(List<Integer> values) {
            addCriterion("work_record_id not in", values, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("work_record_id between", value1, value2, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWorkRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("work_record_id not between", value1, value2, "workRecordId");
            return (Criteria) this;
        }

        public Criteria andWatcherIsNull() {
            addCriterion("watcher is null");
            return (Criteria) this;
        }

        public Criteria andWatcherIsNotNull() {
            addCriterion("watcher is not null");
            return (Criteria) this;
        }

        public Criteria andWatcherEqualTo(String value) {
            addCriterion("watcher =", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherNotEqualTo(String value) {
            addCriterion("watcher <>", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherGreaterThan(String value) {
            addCriterion("watcher >", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherGreaterThanOrEqualTo(String value) {
            addCriterion("watcher >=", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherLessThan(String value) {
            addCriterion("watcher <", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherLessThanOrEqualTo(String value) {
            addCriterion("watcher <=", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherLike(String value) {
            addCriterion("watcher like", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherNotLike(String value) {
            addCriterion("watcher not like", value, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherIn(List<String> values) {
            addCriterion("watcher in", values, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherNotIn(List<String> values) {
            addCriterion("watcher not in", values, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherBetween(String value1, String value2) {
            addCriterion("watcher between", value1, value2, "watcher");
            return (Criteria) this;
        }

        public Criteria andWatcherNotBetween(String value1, String value2) {
            addCriterion("watcher not between", value1, value2, "watcher");
            return (Criteria) this;
        }

        public Criteria andShiftTypeIsNull() {
            addCriterion("shift_type is null");
            return (Criteria) this;
        }

        public Criteria andShiftTypeIsNotNull() {
            addCriterion("shift_type is not null");
            return (Criteria) this;
        }

        public Criteria andShiftTypeEqualTo(Integer value) {
            addCriterion("shift_type =", value, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeNotEqualTo(Integer value) {
            addCriterion("shift_type <>", value, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeGreaterThan(Integer value) {
            addCriterion("shift_type >", value, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("shift_type >=", value, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeLessThan(Integer value) {
            addCriterion("shift_type <", value, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeLessThanOrEqualTo(Integer value) {
            addCriterion("shift_type <=", value, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeIn(List<Integer> values) {
            addCriterion("shift_type in", values, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeNotIn(List<Integer> values) {
            addCriterion("shift_type not in", values, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeBetween(Integer value1, Integer value2) {
            addCriterion("shift_type between", value1, value2, "shiftType");
            return (Criteria) this;
        }

        public Criteria andShiftTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("shift_type not between", value1, value2, "shiftType");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andIsManagerIsNull() {
            addCriterion("is_manager is null");
            return (Criteria) this;
        }

        public Criteria andIsManagerIsNotNull() {
            addCriterion("is_manager is not null");
            return (Criteria) this;
        }

        public Criteria andIsManagerEqualTo(Boolean value) {
            addCriterion("is_manager =", value, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerNotEqualTo(Boolean value) {
            addCriterion("is_manager <>", value, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerGreaterThan(Boolean value) {
            addCriterion("is_manager >", value, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_manager >=", value, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerLessThan(Boolean value) {
            addCriterion("is_manager <", value, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerLessThanOrEqualTo(Boolean value) {
            addCriterion("is_manager <=", value, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerIn(List<Boolean> values) {
            addCriterion("is_manager in", values, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerNotIn(List<Boolean> values) {
            addCriterion("is_manager not in", values, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerBetween(Boolean value1, Boolean value2) {
            addCriterion("is_manager between", value1, value2, "isManager");
            return (Criteria) this;
        }

        public Criteria andIsManagerNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_manager not between", value1, value2, "isManager");
            return (Criteria) this;
        }

        public Criteria andAddManNameIsNull() {
            addCriterion("add_man_name is null");
            return (Criteria) this;
        }

        public Criteria andAddManNameIsNotNull() {
            addCriterion("add_man_name is not null");
            return (Criteria) this;
        }

        public Criteria andAddManNameEqualTo(String value) {
            addCriterion("add_man_name =", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotEqualTo(String value) {
            addCriterion("add_man_name <>", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameGreaterThan(String value) {
            addCriterion("add_man_name >", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameGreaterThanOrEqualTo(String value) {
            addCriterion("add_man_name >=", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameLessThan(String value) {
            addCriterion("add_man_name <", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameLessThanOrEqualTo(String value) {
            addCriterion("add_man_name <=", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameLike(String value) {
            addCriterion("add_man_name like", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotLike(String value) {
            addCriterion("add_man_name not like", value, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameIn(List<String> values) {
            addCriterion("add_man_name in", values, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotIn(List<String> values) {
            addCriterion("add_man_name not in", values, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameBetween(String value1, String value2) {
            addCriterion("add_man_name between", value1, value2, "addManName");
            return (Criteria) this;
        }

        public Criteria andAddManNameNotBetween(String value1, String value2) {
            addCriterion("add_man_name not between", value1, value2, "addManName");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("is_delete is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("is_delete is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Boolean value) {
            addCriterion("is_delete =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Boolean value) {
            addCriterion("is_delete <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Boolean value) {
            addCriterion("is_delete >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_delete >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Boolean value) {
            addCriterion("is_delete <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Boolean value) {
            addCriterion("is_delete <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Boolean> values) {
            addCriterion("is_delete in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Boolean> values) {
            addCriterion("is_delete not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete not between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdIsNull() {
            addCriterion("shift_record_id is null");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdIsNotNull() {
            addCriterion("shift_record_id is not null");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdEqualTo(Integer value) {
            addCriterion("shift_record_id =", value, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdNotEqualTo(Integer value) {
            addCriterion("shift_record_id <>", value, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdGreaterThan(Integer value) {
            addCriterion("shift_record_id >", value, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("shift_record_id >=", value, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdLessThan(Integer value) {
            addCriterion("shift_record_id <", value, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("shift_record_id <=", value, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdIn(List<Integer> values) {
            addCriterion("shift_record_id in", values, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdNotIn(List<Integer> values) {
            addCriterion("shift_record_id not in", values, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("shift_record_id between", value1, value2, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andShiftRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("shift_record_id not between", value1, value2, "shiftRecordId");
            return (Criteria) this;
        }

        public Criteria andDutyDateIsNull() {
            addCriterion("duty_date is null");
            return (Criteria) this;
        }

        public Criteria andDutyDateIsNotNull() {
            addCriterion("duty_date is not null");
            return (Criteria) this;
        }

        public Criteria andDutyDateEqualTo(Date value) {
            addCriterion("duty_date =", value, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateNotEqualTo(Date value) {
            addCriterion("duty_date <>", value, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateGreaterThan(Date value) {
            addCriterion("duty_date >", value, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateGreaterThanOrEqualTo(Date value) {
            addCriterion("duty_date >=", value, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateLessThan(Date value) {
            addCriterion("duty_date <", value, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateLessThanOrEqualTo(Date value) {
            addCriterion("duty_date <=", value, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateIn(List<Date> values) {
            addCriterion("duty_date in", values, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateNotIn(List<Date> values) {
            addCriterion("duty_date not in", values, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateBetween(Date value1, Date value2) {
            addCriterion("duty_date between", value1, value2, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andDutyDateNotBetween(Date value1, Date value2) {
            addCriterion("duty_date not between", value1, value2, "dutyDate");
            return (Criteria) this;
        }

        public Criteria andIsShiftIsNull() {
            addCriterion("is_shift is null");
            return (Criteria) this;
        }

        public Criteria andIsShiftIsNotNull() {
            addCriterion("is_shift is not null");
            return (Criteria) this;
        }

        public Criteria andIsShiftEqualTo(Boolean value) {
            addCriterion("is_shift =", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftNotEqualTo(Boolean value) {
            addCriterion("is_shift <>", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftGreaterThan(Boolean value) {
            addCriterion("is_shift >", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_shift >=", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftLessThan(Boolean value) {
            addCriterion("is_shift <", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftLessThanOrEqualTo(Boolean value) {
            addCriterion("is_shift <=", value, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftIn(List<Boolean> values) {
            addCriterion("is_shift in", values, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftNotIn(List<Boolean> values) {
            addCriterion("is_shift not in", values, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftBetween(Boolean value1, Boolean value2) {
            addCriterion("is_shift between", value1, value2, "isShift");
            return (Criteria) this;
        }

        public Criteria andIsShiftNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_shift not between", value1, value2, "isShift");
            return (Criteria) this;
        }
    }

    /**  tableName: work_record   **/
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /** work_record **/
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}