package com.sichuang.schedule.other;

import java.util.LinkedHashMap;
import java.util.Map;

public class EnumManage {

    /**
     * 值班班次：0-无，1-白班，2-早班，3-夜班，4-宵班
     */
    public static enum ShiftTypeEnum {
        Null("", 0), Day("白", 1), Morning("早", 2), Night("夜", 3), MidNight("宵", 4);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private ShiftTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (ShiftTypeEnum e : ShiftTypeEnum.values()) {
                map.put(e.getId(), e.getName() + "班");
            }
            return map;
        }

        //交接班次
        public static Map<Integer, String> getShiftList() {
            Map<Integer, String> map = EnumManage.ShiftTypeEnum.getList();
            map.remove(0);
            return map;
        }

        public static String getName(int id) {
            for (ShiftTypeEnum e : ShiftTypeEnum.values()) {
                if (e.getId() == id) {
                    return e.getName();
                }
            }
            return "";
        }

        public static int getId(String name) {
            for (ShiftTypeEnum e : ShiftTypeEnum.values()) {
                if (e.getName().equals(name)) {
                    return e.getId();
                }
            }
            return 0;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    /**
     * 注意事项类型：1-长期知识，2-短期知识
     */
    public static enum PointTypeEnum {
        LongTerm("长期", 1), ShortTerm("短期", 2);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private PointTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (PointTypeEnum e : PointTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }


    /**
     * 交接状态：1-未交接，2-已交接
     */
    public static enum ShiftStatusEnum {
        NeverShift("未交接", 1), Shift("已交接", 2);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private ShiftStatusEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (ShiftStatusEnum e : ShiftStatusEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    /**
     * 用户类型：1-管理员，2-普通用户
     */
    public static enum UserTypeEnum {
        Administrator("管理员", 1), Normal("普通用户", 2);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private UserTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (UserTypeEnum e : UserTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    /**
     * 工作内容：1-分类，2-标签
     */
    public static enum DataTypeEnum {
        Label("标签", 1), Watcher("值班员", 2);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private DataTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (DataTypeEnum e : DataTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    /**
     * 更新记录类型：1-交接班记录,2-交接记录，3-交接内容，4-值班记录，5-通知, 6-用户,7-工作内容分类/标签,8-值班员名单。
     */
    public static enum RecordTypeEnum {
        DaySchedule("排班表", 1), ShiftRecord("交接记录", 2), WorkItem("交接内容", 3), WorkRecord("值班记录", 4), AttentionPoint("通知", 5),
        User("用户", 6), ItemType("工作内容分类/标签", 7), Watcher("值班员名单", 8);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private RecordTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (RecordTypeEnum e : RecordTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    /**
     * 布尔类型
     */
    public static enum BooleanTypeEnum {
        False("否", false), True("是", true);
        // 成员变量
        private String name;
        private boolean id;

        // 构造方法
        private BooleanTypeEnum(String name, boolean id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Boolean, String> getList() {
            Map<Boolean, String> map = new LinkedHashMap<>();
            for (BooleanTypeEnum e : BooleanTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public boolean getId() {
            return id;
        }
    }

    /**
     * 附件类型：1-工单，2-通知，3-排班表
     */
    public static enum FileTypeEnum {
        WorkItem("工单", 1), AttentionPoint("通知", 2), WorkRecord("排班表", 3);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private FileTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (FileTypeEnum e : FileTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }
}
