package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.AccessRecord;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xs
 * @date 2018/9/20 15:51
 */
public interface IAccessRecordService {

    /**
     * 保存访问记录
     * @param request
     * @return
     */
    int add(HttpServletRequest request);
}
