package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IAttentionPointService {

    /**
     * 添加注意事项
     *
     * @param attentionPoint
     * @return
     */
    Map<String, Object> add(AttentionPoint attentionPoint);


    /**
     * 修改注意事项
     *
     * @param attentionPoint
     * @return
     */
    Map<String, Object> update(AttentionPoint attentionPoint, User userSession);


    /**
     * 获取查询条件
     *
     * @param attentionPoint
     * @return
     */
    AttentionPointExample getSearchExampleByModel(AttentionPoint attentionPoint);

    /**
     * 分页获取注意事项
     *
     * @param attentionPoint
     * @param page
     * @return
     */
    Map<String, Object> getByModelAndPage(AttentionPoint attentionPoint, Page page);

    /**
     * 按交接日期和类型获取注意事项和分页信息
     * @param scheduleDate
     * @param pointType
     * @return
     */
    Map<String, Object> getByScheduleDateAndPage(Date scheduleDate, Integer pointType);

    /**
     * 按交接日期和类型获取注意事项
     *
     * @param scheduleDate
     * @param pointType null,长期,短期
     * @return
     */
    List<AttentionPoint> getByScheduleDate(Date scheduleDate, Integer pointType);

    /**
     * 验证注意事项
     *
     * @param attentionPointId
     * @return
     */
    Map<String, Object> verifyAttentionPoint(Integer attentionPointId, User userSession);

    /**
     * 批量移除注意事项
     * @param attentionPointIds
     * @return
     */
    Map<String, Object> removeByAttentionPointIds(String attentionPointIds, User userSession);

    /**
     * 验证提交表单信息
     * @param attentionPoint
     * @return
     */
    Map<String, Object> verifyFormData(AttentionPoint attentionPoint);

    /**
     * 导出历史通知内容表
     * @param attentionPoint
     * @param response
     * @return
     */
    Map<String, Object> exportExcel(AttentionPoint attentionPoint, HttpServletResponse response);
}
