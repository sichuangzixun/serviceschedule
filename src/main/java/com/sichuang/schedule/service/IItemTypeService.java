package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;

import java.util.List;
import java.util.Map;

public interface IItemTypeService {

    /**
     * 获取全部分类
     * @return
     */
    Map<String, Object> getByPage(Integer dataType, Page page);

    /**
     * 获取分类列表
     * @return
     */
    List<ItemType> getList(Integer dataType);

    /**
     * 添加数据
     * @param itemType
     * @return
     */
    Map<String,Object> add(ItemType itemType);

    /**
     * 批量添加。空格隔开
     * @param itemType
     * @return
     */
    Map<String,Object> addTypes(ItemType itemType);

    /**
     * 批量删除数据
     * @param typeIds
     * @return
     */
    Map<String, Object> removeByTypeIds(String typeIds, User userSession);

    /**
     * 根据分类名称获取分类
     * @param itemTypeName
     * @param dataType
     * @return
     */
    ItemType getByItemTypeName(String itemTypeName, int dataType);

}
