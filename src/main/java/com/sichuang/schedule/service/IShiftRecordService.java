package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IShiftRecordService {

    /**
     * 搜索交接内容
     *
     * @param shiftRecord
     * @param dutyDateEnd
     * @return
     */
    ShiftRecordExample getSearchExampleByModel(ShiftRecord shiftRecord, Date dutyDateEnd);

    /**
     * 分页搜索交接记录
     *
     * @param shiftRecord
     * @param dutyDateEnd
     * @param page
     * @return
     */
    Map<String, Object> getByModelAndPage(ShiftRecord shiftRecord, Date dutyDateEnd, Page page);

    /**
     * 添加交接记录
     *
     * @param shiftRecord
     * @return
     */
    Map<String, Object> add(ShiftRecord shiftRecord);

    /**
     * 页面添加交接记录、现场负责人、值班员
     * @param shiftRecord
     * @return
     */
    Map<String, Object> addShiftRecord(ShiftRecord shiftRecord);

    /**
     * 修改交接记录
     *
     * @param shiftRecord
     * @return
     */
    Map<String, Object> update(ShiftRecord shiftRecord, User userSession);


    /**
     * 验证交接记录是否存在
     *
     * @param shiftRecordId
     * @return
     */
    Map<String, Object> verifyShiftRecord(Integer shiftRecordId, User userSession);

    /**
     * 获取班次信息
     * @param shiftRecordId
     * @param userSession
     * @return
     */
    Map<String, Object> viewShiftRecord(Integer shiftRecordId, User userSession);
    
    /**
     * 批量插入
     * @param watcherList 值班员
     * @param shiftRecord dutyDate 值班月份
     * @return
     */
    int insertRecords(List<List<String>> watcherList, ShiftRecord shiftRecord) throws Exception;


    /**
     * 按月添加值班班次
     * @param shiftRecord dutyDate 值班月份
     * @return
     */
    Map<String, Object> addByMonth(List<List<String>> watcherList, ShiftRecord shiftRecord);

    /**
     * 按天添加四个班次的值班记录
     *
     * @param shiftRecord
     * @return
     */
    Map<String, Object> addByDay(List<List<String>> watcherList, ShiftRecord shiftRecord);


    /**
     * 按班次添加值班班次和值班记录
     * @param watcherList
     * @param shiftRecord
     * @return
     */
    Map<String, Object> addShiftRecordAndWorkRecord(List<List<String>> watcherList, ShiftRecord shiftRecord);

    /**
     * 按交接时间和交接类型获取交接记录
     *
     * @param dutyDate
     * @param shiftType
     * @return
     */
    ShiftRecord getByDutyDateAndShiftType(Date dutyDate, int shiftType);


    /**
     * 获取当前时间上一班次遗留工作，未完成
     *
     * @param shiftTime
     * @return
     */
    Map<String, Object> getPreviousShiftRecord(Date shiftTime);

    /**
     * 获取当前时间当前班次及添加的工作事项
     *
     * @param shiftTime
     * @return
     */
    Map<String, Object> getCurrentShiftRecordData(Date shiftTime);

    /**
     * 获取当前时间当前班次
     *
     * @param shiftTime
     * @return
     */
    List<ShiftRecord> getCurrentShiftRecord(Date shiftTime);


    /**
     * 按照交接班次设置交接时间
     *
     * @param shiftRecord 值班日期、交接班次
     * @return
     */
    ShiftRecord setShiftTimeByShiftType(ShiftRecord shiftRecord);

    /**
     * 更新交接记录状态
     *
     * @param shiftRecordIds 交接记录id
     * @param updateStatus   更新后的状态
     * @param shiftStatus    更新前的状态，可为空
     * @return
     */
    Map<String, Object> updateShiftStatus(String shiftRecordIds, int updateStatus, Integer shiftStatus);


    /**
     * 修改未交接为已交接
     *
     * @param shiftRecordIds
     * @return
     */
    Map<String, Object> endShift(String shiftRecordIds, User userSession);

    /**
     * 移除班次
     * @param shiftRecordIds
     * @param userSession
     * @return
     */
    Map<String, Object> remove(String shiftRecordIds, User userSession);

    /**
     * 批量导出班次交接内容
     *
     * @param shiftRecordIds
     * @param shiftRecord
     * @param dutyDateEnd
     * @return
     */
    Map<String, Object> exportExcel(HttpServletResponse response, String shiftRecordIds, ShiftRecord shiftRecord, Date dutyDateEnd);

    /**
     * 批量导出历史班次交接内容
     *
     * @param shiftRecords
     * @param response
     * @return
     */
    Map<String, Object> exportExcelByShiftRecords( List<ShiftRecord> shiftRecords, HttpServletResponse response);

    /**
     * 导出交接班记录
     * @param shiftRecord
     * @param row excel容器模板
     * @param shiftTime 时间格式样式
     * @return
     */
    Map<String, Object> exportExcelByShiftRecord(ShiftRecord shiftRecord, Row row, CellStyle shiftTime);

    /**
     * 获取交班记录
     * @param shiftRecord
     * @return
     */
    Map<String, Object> getDataByShiftRecord(ShiftRecord shiftRecord);

    /**
     * 跟当前时间匹配班次、值班日期
     * @return
     */
    ShiftRecord getCurrentByTime();
}
