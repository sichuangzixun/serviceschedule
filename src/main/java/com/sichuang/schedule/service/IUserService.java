package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IUserService {

    /**
     * 登陆
     * @param user
     * @return
     */
    public Map<String, Object> login(HttpServletRequest request, User user);

    /**
     * 用户退出登录
     * @return
     */
    public Map<String, Object> logout(HttpServletRequest request) ;

    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     */
    public Map<String, Object> changePassword(HttpServletRequest request, String oldPassword, String newPassword);

    /**
     * 获取用户列表
     * @return
     */
    Map<String, Object> search(Page page);

    /**
     * 批量删除用户
     * @param userIds
     * @return
     */
    Map<String, Object> removeByUserIds(String userIds, User userSession);


    /**
     * 保存用户
     * @param user
     * @return
     */
    Map<String, Object> save(User user, User userSession);

    /**
     * 登录账号不能重复
     * @param loginName
     * @return
     */
    int countByLoginName(String loginName);

    /**
     * 验证用户是否存在
     * @param userId
     * @return
     */
    Map<String, Object> verifyUser(Integer userId);

}
