package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.WorkItem;
import com.sichuang.schedule.model.entity.WorkItemFile;
import com.sichuang.schedule.model.entity.WorkItemFileExample;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/7/23 15:29
 */
public interface IWorkItemFileService {

    /**
     * 上传文件
     *
     * @param request
     * @param workItemFile
     * @return
     */
    Map<String, Object> uploadWorkItem(HttpServletRequest request, WorkItemFile workItemFile);

    /**
     * 上传文件
     * @param request
     * @param workItemFile
     * @return
     */
    Map<String, Object> uploadFile(HttpServletRequest request, WorkItemFile workItemFile);

    /**
     * 添加文件
     * @param workItemFile
     * @return
     */
    Map<String, Object> add(WorkItemFile workItemFile);

    /**
     * 获取附件列表
     *
     * @param recordId
     * @param fileType 1-工单，2-通知
     * @return
     */
    List<WorkItemFile> get(int recordId, int fileType);

    /**
     * 批量移除附件
     *
     * @param fileIds
     * @param userSession
     * @return
     */
    Map<String, Object> removeByFileIds(String fileIds, User userSession);

    /**
     * 批量删除附件
     * @param workItemIdList
     * @param userSession
     * @return
     */
    Map<String, Object> removeByWorkItemIds(List<Integer> workItemIdList, User userSession);

    /**
     * 删除附件
     * @param shiftRecordIdList
     * @param userSession
     * @return
     */
    Map<String, Object> removeByShiftRecordIds(List<Integer> shiftRecordIdList, User userSession);

    /**
     * 改为已交接
     *
     * @param shiftRecordIdList
     * @return
     */
    Map<String, Object> shiftByShiftRecordIds(List<Integer> shiftRecordIdList);


    /**
     * 移除附件
     * @param example
     * @return
     */
    Map<String, Object> removeByExample(WorkItemFileExample example);


    /**
     * 搜索可移除附件
     * @param idList
     * @param type 1-scheduleIdList,2-workItemIdList,3-fileIdList
     * @param userSession
     * @return
     */
    WorkItemFileExample getExampleByIds(List<Integer> idList, int type, User userSession);

}
