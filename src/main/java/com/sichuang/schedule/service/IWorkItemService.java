package com.sichuang.schedule.service;

import com.alibaba.fastjson.JSONObject;
import com.sichuang.schedule.model.entity.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IWorkItemService {

    /**
     * 分页搜索工作事项及交接信息
     * @param workItem
     * @param page
     * @return
     */
    Map<String, Object> getByModelAndPage(WorkItem workItem, Date addTimeEnd, Date endTimeEnd, Page page);

    /**
     * 联合搜索交接内容
     * @param workItem
     * @param addTimeEnd
     * @param endTimeEnd
     * @param page
     * @return
     */
    Map<String, Object> getWorkItem(WorkItem workItem, Date addTimeEnd, Date endTimeEnd, Page page);

    /**
     * 获取查询条件
     * @param workItem
     * @return
     */
    WorkItemExample getSearchExampleByModel(WorkItem workItem, Date addTimeEnd, Date endTimeEnd);

    /**
     * 获取工作列表
     * @param isComplete
     * @return
     */
    List<WorkItem> getByIsCompleteAndShiftRecord(boolean isComplete, Date shiftTime, int shiftType);


    /**
     * 添加工作事项
     * @param workItem
     * @return
     */
    Map<String, Object> add(WorkItem workItem);

    /**
     * 添加工作事项
     * @param workItem
     * @return
     */
    Map<String, Object> addWorkItem(WorkItem workItem, User userSession);

    /**
     * 修改工作事项
     * @param workItem
     * @return
     */
    Map<String, Object> update(WorkItem workItem, User userSession);

    /**
     * 验证工作事项情况
     * @param workItemId
     * @return
     */
    Map<String, Object> verifyWorkItem(Integer workItemId, User userSession);

    /**
     * 移除工作事项
     * @param workItemIds
     * @return
     */
    Map<String, Object> removeByWorkItemIds(String workItemIds, User userSession);


    /**
     * 批量标记为已完成
     * @param workItemIds
     * @return
     */
    Map<String, Object> completeByWorkItemIds(String workItemIds, User userSession);

    /**
     * 根据交接记录获取工作事项
     * @param shiftRecordId
     * @return
     */
    List<WorkItem> getByShiftRecordId(Boolean isComplete, int shiftRecordId);

    /**
     * 按交接时间获取该班次工作事项
     * @param isComplete
     * @param shiftTime
     * @return
     */
    List<WorkItem> getByShiftTime(Boolean isComplete, Date shiftTime);

    /**
     * 按搜索条件导出交接记录
     *
     * @param workItem
     * @param addTimeEnd
     * @param endTimeEnd
     * @param response
     * @return
     */
    Map<String, Object> exportExcel(WorkItem workItem, Date addTimeEnd, Date endTimeEnd, HttpServletResponse response);


    /**
     * 保存交接内容
     * @param shiftRecordId
     * @param workItem
     * @return
     */
    Map<String, Object> complete(WorkItem workItem, Integer shiftRecordId, User userSession);

    /**
     * 改为已交接
     * @param shiftRecordIdList
     * @return
     */
    Map<String, Object> shiftByShiftRecordIds(List<Integer> shiftRecordIdList);

    /**
     * 按完成时间标记已完成
     * @param shiftRecord
     * @return
     */
    Map<String, Object> completeByShiftTime(ShiftRecord shiftRecord);

    /**
     * 标记已完成或增加处理情况
     * @param workItem
     * @return
     */
    Map<String, Object> handle(WorkItem workItem, Integer shiftRecordId, User userSession);


    /**
     * 验证班次
     * @param workItemId
     * @param userSession
     * @return
     */
    Map<String, Object> verifyShiftRecord(Integer workItemId, User userSession);

}
