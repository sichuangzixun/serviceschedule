package com.sichuang.schedule.service;

import com.sichuang.schedule.model.entity.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IWorkRecordService {

    /**
     * 添加工作记录
     * @param workRecord
     * @return
     */
    Map<String, Object> add(WorkRecord workRecord);

    /**
     * 添加值班记录，如果是助理负责人，更新交接记录助理负责人
     * @param workRecord
     * @return
     */
    Map<String, Object> addWorkRecord(WorkRecord workRecord);

    /**
     * 修改值班员工作记录（不能修改值班日期和姓名）
     * @param workRecord
     * @return
     */
    Map<String, Object> update(WorkRecord workRecord, User userSession);

    /**
     * 获取工作记录
     * @param workRecord
     * @param page
     * @return
     */
    Map<String, Object> getByModelAndPage(WorkRecord workRecord, Date dutyDateEnd, Page page);


    /**
     * 获取搜索条件
     * @param workRecord
     * @return
     */
    WorkRecordExample getSearchExampleByModel(WorkRecord workRecord, Date dutyDateEnd);

    /**
     * 验证工作记录是否存在
     * @param workRecordId
     * @return
     */
    Map<String, Object> verifyWorkRecord(Integer workRecordId, User userSession);

    /**
     * 移除工作记录
     * @param workRecordIds
     * @return
     */
    Map<String, Object> removeByWorkRecordIds(String workRecordIds, User userSession);

    /**
     * 按值班班次删除值班记录
     * @param shiftRecordIdList
     * @return
     */
    Map<String, Object> removeByShiftRecordIds(List<Integer> shiftRecordIdList, User userSession);

    /**
     * 移除整个月份的值班记录
     * @param dutyDate
     * @return
     */
    Map<String, Object> removeByMonth(Date dutyDate);

    /**
     * 按值班员搜索
     * @param shiftRecordId
     * @param watcher
     * @return
     */
    WorkRecord getByWatcherAndShiftRecordId(int shiftRecordId, String watcher);

    /**
     * 判断是否更新交接记录助理负责人
     * @param watcher 值班员
     * @param shiftRecordId 原记录
     * @return
     */
    Map<String, Object> updateManager(String watcher, int shiftRecordId, User userSession);

    /**
     * 获取班次值班员。负责人为第一个
     * @param shiftRecordId
     * @return
     */
    List<WorkRecord> getByShiftRecordId(int shiftRecordId, Boolean isManager);

    /**
     * 导入排班表
     * @param filename
     * @return
     */
    Map<String, Object> importExcel(String filename, ShiftRecord shiftRecord);

    /**
     * 上传EXCEL文件
     * @param request
     * @return
     */
    Map<String, Object> upload(HttpServletRequest request, ShiftRecord shiftRecord);

    /**
     * 保存值班表导入文件
     * @param fileUrl
     * @return
     */
    Map<String, Object> addWorkRecordFile(Map<String, Object> fileUrl, User userSession);

    /**
     * 按月份获取值班记录
     * @param dutyDate
     * @return
     */
    List<List<String>> getByMonth(Date dutyDate);

    /**
     * 按值班员获取值班记录
     * @param watcher
     * @param dutyDate
     * @return
     */
    List<String> getByWatcher(String watcher, Date dutyDate);

    /**
     * 按月份和值班员获取值班记录
     * @param watcher
     * @param month
     * @return
     */
    List<WorkRecord> getByMonthAndWatcher(String watcher, Date month);

    /**
     * 验证值班班次，如无则添加值班班次
     * @param dutyDate
     * @param shiftType
     * @return shiftRecord
     */
    Map<String, Object> verifyShiftRecord(Date dutyDate, int shiftType, String userName);

    /**
     * 修改交接状态
     * @param shiftRecordIdList
     * @return
     */
    Map<String, Object> shiftByShiftRecordIds(List<Integer> shiftRecordIdList);
}
