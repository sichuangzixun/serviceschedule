package com.sichuang.schedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.dao.AccessRecordMapper;
import com.sichuang.schedule.model.entity.AccessRecord;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.service.IAccessRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author xs
 * @date 2018/9/20 15:51
 */
@Service
public class AccessRecordService implements IAccessRecordService{

    @Resource private AccessRecordMapper accessRecordMapper;

    @Override
    public int add(HttpServletRequest request) {
        AccessRecord accessRecord = new AccessRecord();
        accessRecord.setAccessUrl(request.getRequestURI());
        String param = JSON.toJSONString(request.getParameterMap());
        if(param.length() > 255){
            param = param.substring(0,255);
        }
        accessRecord.setAccessParam(param);
        User userSession = BaseController.getUserSession(request);
        if(userSession != null){
            accessRecord.setAccessUserId(userSession.getUserId());
            accessRecord.setAccessUserName(userSession.getLoginName());
        }
        accessRecord.setAccessTime(new Date());
        return accessRecordMapper.insertSelective(accessRecord);
    }
}
