package com.sichuang.schedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.sichuang.schedule.common.*;
import com.sichuang.schedule.dao.AttentionPointMapper;
import com.sichuang.schedule.model.entity.*;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IAttentionPointService;
import com.sichuang.schedule.service.IShiftRecordService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service("attentionPointService")
public class AttentionPointService implements IAttentionPointService {

    @Resource
    private AttentionPointMapper attentionPointMapper;
    @Resource
    private IShiftRecordService shiftRecordService;

    @Override
    public Map<String, Object> add(AttentionPoint attentionPoint) {
        //验证添加班次
        if(attentionPoint.getAddShiftRecordId() != null){
            Map<String, Object> map = shiftRecordService.verifyShiftRecord(attentionPoint.getAddShiftRecordId(), null);
            if(MyResponse.isErrorByRootMap(map)){
                return map;
            }
            ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
            attentionPoint.setAddShiftType(shiftRecord.getShiftType());
        }

        //验证表单信息
        Map<String, Object> map = verifyFormData(attentionPoint);
        if(MyResponse.isSuccessByRootMap(map)){
            attentionPoint = (AttentionPoint) MyResponse.getDataByRootMap(map);
        }else {
            return map;
        }

        //设置系统信息
        attentionPoint.setIsDelete(false);
        attentionPoint.setAddTime(new Date());
        attentionPoint.setAttentionPointId(null);
        int count = attentionPointMapper.insertSelective(attentionPoint);
        if (count == 1) {
            return MyResponse.getSuccessResponse(attentionPoint);
        }
        return MyResponse.getSQLErrorResponse();
    }


    @Override
    public Map<String, Object> update(AttentionPoint attentionPoint, User userSession) {
        Map<String, Object> map;
        if(StringUtil.isEmpty(attentionPoint.getEndInformation())){
            //验证表单信息
            map = verifyFormData(attentionPoint);
            if(MyResponse.isSuccessByRootMap(map)){
                attentionPoint = (AttentionPoint) MyResponse.getDataByRootMap(map);
            }else {
                return map;
            }
        }else {
            //更新结束日期为当前时间
            attentionPoint.setEndDate(new Date());
        }

        //验证注意事项是否存在
        map = verifyAttentionPoint(attentionPoint.getAttentionPointId(), userSession);
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }

        int count = attentionPointMapper.updateByPrimaryKeySelective(attentionPoint);
        if (count == 1) {
            return MyResponse.getSQLSuccessResponse();
        }
        return MyResponse.getSQLErrorResponse();
    }


    @Override
    public AttentionPointExample getSearchExampleByModel(AttentionPoint attentionPoint) {
        AttentionPointExample example = new AttentionPointExample();
        AttentionPointExample.Criteria criteria = example.createCriteria();
        if (attentionPoint.getIsDelete() != null) {
            criteria.andIsDeleteEqualTo(attentionPoint.getIsDelete());
        }
        if (attentionPoint.getPointType() != null) {
            criteria.andPointTypeEqualTo(attentionPoint.getPointType());
        }
        //开始日期搜索范围
        if (attentionPoint.getBeginDate() != null) {
            criteria.andBeginDateGreaterThanOrEqualTo(DateTimeModel.getFirstTimeOfDay(attentionPoint.getBeginDate()));
        }
        //开始日期搜索范围
        if (attentionPoint.getEndDate() != null) {
            criteria.andBeginDateLessThanOrEqualTo(DateTimeModel.getLastTimeOfDay(attentionPoint.getEndDate()));
        }
        if(attentionPoint.getAddShiftRecordId() != null){
            criteria.andAddShiftRecordIdEqualTo(attentionPoint.getAddShiftRecordId());
        }
        return example;
    }

    @Override
    public Map<String, Object> getByModelAndPage(AttentionPoint attentionPoint, Page page) {
        AttentionPointExample example = getSearchExampleByModel(attentionPoint);
        example.setPage(page);
        example.setOrderByClause("begin_date desc");
        if (page.getPageNo() == 1 || page.getTotalRecords() == 0) {
            page.setTotalRecords(attentionPointMapper.countByExample(example));
        }
        List<AttentionPoint> attentionPointList = new ArrayList<>();
        if (page.getTotalRecords() > 0) {
            attentionPointList = attentionPointMapper.selectByExample(example);
        }
        return MyResponse.getResponse(true, page, attentionPointList);
    }

    @Override
    public Map<String, Object> getByScheduleDateAndPage(Date scheduleDate, Integer pointType) {
        List<AttentionPoint> attentionPointList = getByScheduleDate(scheduleDate, pointType);
        Page page = new Page();
        page.setTotalRecords(attentionPointList.size());
        return MyResponse.getResponse(true, page, attentionPointList);
    }

    @Override
    public List<AttentionPoint> getByScheduleDate(Date scheduleDate, Integer pointType) {
        AttentionPointExample example = new AttentionPointExample();
//        Date time = DateTimeModel.getFirstTimeOfDay(scheduleDate);
        //在日期范围内
        AttentionPointExample.Criteria criteriaRange = example.createCriteria();
        criteriaRange.andIsDeleteEqualTo(false)
                .andBeginDateLessThanOrEqualTo(scheduleDate)
                .andEndDateGreaterThanOrEqualTo(scheduleDate);

        /*//结束日期为空
        AttentionPointExample.Criteria criteria = example.or();
        criteria.andIsDeleteEqualTo(false)
                .andBeginDateLessThanOrEqualTo(time)
                .andEndDateIsNull();*/

        if (pointType != null) {
            criteriaRange.andPointTypeEqualTo(pointType);
//            criteria.andPointTypeEqualTo(pointType);
        }

        example.setOrderByClause("point_type desc, end_date asc");
        return attentionPointMapper.selectByExample(example);
    }

    @Override
    public Map<String, Object> verifyAttentionPoint(Integer attentionPointId, User userSession) {
        if (attentionPointId == null || attentionPointId <= 0) {
            return MyResponse.getErrorResponse("操作失败，请选择注意事项！");
        }
        AttentionPoint attentionPoint = attentionPointMapper.selectByPrimaryKey(attentionPointId);
        if (attentionPoint == null || attentionPoint.getIsDelete()) {
            return MyResponse.getErrorResponse("操作失败，注意事项不存在！");
        }
        if(userSession != null) {
            if (attentionPoint.getEndDate().before(new Date()) && userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
                return MyResponse.getErrorResponse("操作失败，注意事项已结束！");
            }
        }
        return MyResponse.getSuccessResponse(attentionPoint);
    }

    @Override
    public Map<String, Object> removeByAttentionPointIds(String attentionPointIds, User userSession) {
        if(StringUtil.isEmpty(attentionPointIds)){
            return MyResponse.getErrorResponse("请选择通知！");
        }
        List<Integer> attentionPointIdList = StringUtil.parseIdList(attentionPointIds);
        AttentionPointExample example = new AttentionPointExample();
        AttentionPointExample.Criteria criteria = example.createCriteria().andIsDeleteEqualTo(false)
                .andAttentionPointIdIn(attentionPointIdList);
        //普通用户只能删除未结束通知
        if(userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())){
            criteria.andEndDateGreaterThanOrEqualTo(new Date());
        }
        AttentionPoint attentionPoint = new AttentionPoint();
        attentionPoint.setIsDelete(true);
        int count = attentionPointMapper.updateByExampleSelective(attentionPoint, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> verifyFormData(AttentionPoint attentionPoint) {
        if (StringUtil.isEmpty(attentionPoint.getPointContent())) {
            return MyResponse.getErrorResponse("操作失败，注意事项内容不能为空！");
        }
        if (attentionPoint.getBeginDate() == null) {
            return MyResponse.getErrorResponse("操作失败，开始日期不能为空！");
        }
        if (attentionPoint.getEndDate() != null && attentionPoint.getEndDate().before(attentionPoint.getBeginDate())) {
            return MyResponse.getErrorResponse("操作失败，结束日期不能早于开始日期！");
        }
        //设置开始日期时间为00:00:00
        DateTime dateTime = new DateTime(attentionPoint.getBeginDate());
        dateTime = dateTime.withTime(0,0,0,0);
        attentionPoint.setBeginDate(dateTime.toDate());
        //设置结束日期时间为23:59:59
        dateTime = new DateTime(attentionPoint.getEndDate());
        Date endDate = dateTime.withTime(23,59,59,0).toDate();
        attentionPoint.setEndDate(endDate);
        return MyResponse.getSuccessResponse(attentionPoint);
    }

    @Override
    public Map<String, Object> exportExcel(AttentionPoint attentionPoint, HttpServletResponse response) {
        Map<String, Object> map = getByModelAndPage(attentionPoint, new Page(1, 60000));
        List<AttentionPoint> attentionPointList = (List<AttentionPoint>) MyResponse.getDataByRootMap(map);
        LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String, String>();
        fieldMap.put("pointType#"+ JSON.toJSONString(EnumManage.PointTypeEnum.getList()), "类型");
        fieldMap.put("beginDate#"+DateTimeModel.DATE_TYPE_2, "录入日期");
        fieldMap.put("addShiftType#" + JSON.toJSONString(EnumManage.ShiftTypeEnum.getList()), "录入班次");
        fieldMap.put("itemTypeNames", "标签");
        fieldMap.put("pointContent", "通知内容");
        fieldMap.put("endDate#"+DateTimeModel.DATE_TYPE_2, "终止日期");
        fieldMap.put("endInformation", "终止理由");

        try {
            ExcelUtils.listToExcel(attentionPointList, fieldMap, "历史通知内容表", response);
        } catch (ExcelException e) {
            e.printStackTrace();
            return MyResponse.getErrorResponse();
        }
        return MyResponse.getSuccessResponse();
    }
}
