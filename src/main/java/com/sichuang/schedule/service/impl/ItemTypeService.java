package com.sichuang.schedule.service.impl;

import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.common.StringUtil;
import com.sichuang.schedule.dao.ItemTypeMapper;
import com.sichuang.schedule.model.entity.ItemType;
import com.sichuang.schedule.model.entity.ItemTypeExample;
import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IItemTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("itemTypeService")
public class ItemTypeService implements IItemTypeService {

    @Resource
    private ItemTypeMapper itemTypeMapper;

    @Override
    public Map<String, Object> getByPage(Integer dataType, Page page) {
        ItemTypeExample example = new ItemTypeExample();
        example.setPage(page);
        example.setOrderByClause("add_time asc");
        ItemTypeExample.Criteria criteria = example.createCriteria().andIsDeleteEqualTo(false);
        if (dataType != null) {
            criteria.andDataTypeEqualTo(dataType);
        }
        List<ItemType> itemTypeList = itemTypeMapper.selectByExample(example);
        page.setTotalRecords(itemTypeMapper.countByExample(example));
        return MyResponse.getResponse(true, page, itemTypeList);
    }

    @Override
    public List<ItemType> getList(Integer dataType) {
        Page page = new Page();
        page.setLength(0);
        Map<String, Object> map = getByPage(dataType, page);
        List<ItemType> itemTypeList = (List<ItemType>) MyResponse.getDataByRootMap(map);
        return itemTypeList;
    }

    @Override
    public Map<String, Object> add(ItemType itemType) {

        ItemType verifyItemType = getByItemTypeName(itemType.getTypeName(), itemType.getDataType());
        if (verifyItemType != null) {
            return MyResponse.getErrorResponse("操作失败，内容不能重复！");
        }
        itemType.setAddTime(new Date());
        itemType.setIsDelete(false);
        itemType.setTypeId(null);
        int count = itemTypeMapper.insertSelective(itemType);
        if (count == 1) {
            return MyResponse.getSQLSuccessResponse();
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    @Transactional
    public Map<String, Object> addTypes(ItemType itemType) {
        if (StringUtil.isEmpty(itemType.getTypeName())) {
            return MyResponse.getErrorResponse("操作失败，请输入内容！");
        }
        if (itemType.getDataType() == null) {
            return MyResponse.getErrorResponse("操作失败，类型不能为空！");
        }
        String[] watcherList = itemType.getTypeName().split(" ");
        int count = 0;
        for (String watcher : watcherList) {
            if (StringUtil.isEmpty(watcher) == false) {
                itemType.setTypeName(watcher);
                //添加值班记录
                Map<String, Object> map = add(itemType);
                if (MyResponse.isErrorByRootMap(map)) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return map;
                }
                count++;
            }

        }
        return MyResponse.getSuccessResponse("成功添加" + count + "个记录！");
    }

    @Override
    public Map<String, Object> removeByTypeIds(String typeIds, User userSession) {
        if (StringUtil.isEmpty(typeIds)) {
            return MyResponse.getErrorResponse("请选择值班员/标签！");
        }
        List<Integer> itemTypeIdList = StringUtil.parseIdList(typeIds);
        ItemTypeExample example = new ItemTypeExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andTypeIdIn(itemTypeIdList);
        ItemType itemType = new ItemType();
        itemType.setIsDelete(true);
        int count = itemTypeMapper.updateByExampleSelective(itemType, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public ItemType getByItemTypeName(String itemTypeName, int dataType) {
        ItemTypeExample example = new ItemTypeExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andTypeNameEqualTo(itemTypeName)
                .andDataTypeEqualTo(dataType);
        List<ItemType> itemTypeList = itemTypeMapper.selectByExample(example);
        if (itemTypeList.isEmpty() == false) {
            return itemTypeList.get(0);
        }
        return null;
    }
}
