package com.sichuang.schedule.service.impl;

import com.sichuang.schedule.base.dao.BShiftRecordMapper;
import com.sichuang.schedule.common.DateTimeModel;
import com.sichuang.schedule.common.ExcelUtils;
import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.common.StringUtil;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.dao.ShiftRecordMapper;
import com.sichuang.schedule.model.entity.*;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("shiftRecordService")
public class ShiftRecordService implements IShiftRecordService {
    @Resource
    private ShiftRecordMapper shiftRecordMapper;
    @Resource
    private IWorkItemService workItemService;
    @Resource
    private IWorkRecordService workRecordService;
    @Resource
    private IWorkItemFileService workItemFileService;
    @Resource
    private IAttentionPointService attentionPointService;
    @Resource
    private BShiftRecordMapper bShiftRecordMapper;

    @Override
    public ShiftRecordExample getSearchExampleByModel(ShiftRecord shiftRecord, Date dutyDateEnd) {
        ShiftRecordExample example = new ShiftRecordExample();
        ShiftRecordExample.Criteria criteria = example.createCriteria();
        if (shiftRecord.getDutyDate() != null) {
            criteria.andDutyDateGreaterThanOrEqualTo(DateTimeModel.getFirstTimeOfDay(shiftRecord.getDutyDate()));
        }
        if (dutyDateEnd != null) {
            criteria.andDutyDateLessThanOrEqualTo(DateTimeModel.getLastTimeOfDay(dutyDateEnd));
        }
        if (shiftRecord.getIsDelete() != null) {
            criteria.andIsDeleteEqualTo(shiftRecord.getIsDelete());
        }
        if (shiftRecord.getShiftType() != null) {
            criteria.andShiftTypeEqualTo(shiftRecord.getShiftType());
        }
        return example;
    }

    @Override
    public Map<String, Object> getByModelAndPage(ShiftRecord shiftRecord, Date dutyDateEnd, Page page) {
        ShiftRecordExample example = getSearchExampleByModel(shiftRecord, dutyDateEnd);
        example.setPage(page);
        example.setOrderByClause("duty_date desc, shift_type desc");
        if (page.getPageNo() == 1 || page.getTotalRecords() == 0) {
            page.setTotalRecords(shiftRecordMapper.countByExample(example));
        }
        List<ShiftRecord> shiftRecordList = new ArrayList<>();
        if (page.getTotalRecords() > 0) {
            shiftRecordList = shiftRecordMapper.selectByExample(example);
        }
        return MyResponse.getResponse(true, page, shiftRecordList);
    }

    @Override
    public Map<String, Object> add(ShiftRecord shiftRecord) {
        if (shiftRecord.getShiftType() == null) {
            return MyResponse.getErrorResponse("操作失败，值班班次不能为空！");
        }
        if (shiftRecord.getDutyDate() == null) {
            return MyResponse.getErrorResponse("操作失败，值班日期不能为空！");
        }
        ShiftRecord shiftRecordInfo = getByDutyDateAndShiftType(shiftRecord.getDutyDate(), shiftRecord.getShiftType());
        if (shiftRecordInfo != null) {
            return MyResponse.getErrorResponse("操作失败，不能重复添加班次！");
        }
        //值班日期设置时分秒
        DateTime dutyDate = new DateTime(shiftRecord.getDutyDate());
        dutyDate = dutyDate.withTime(0, 0, 0, 0);
        shiftRecord.setDutyDate(dutyDate.toDate());
        //按交接班次设置交接时间
        shiftRecord = setShiftTimeByShiftType(shiftRecord);
        //设置系统信息
        shiftRecord.setShiftStatus(EnumManage.ShiftStatusEnum.NeverShift.getId());
        shiftRecord.setAddTime(new Date());
        shiftRecord.setShiftRecordId(null);
        shiftRecord.setIsDelete(false);
        int count = shiftRecordMapper.insertSelective(shiftRecord);
        if (count == 1) {
            return MyResponse.getSuccessResponse(shiftRecord);
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    @Transactional
    public Map<String, Object> addShiftRecord(ShiftRecord shiftRecord) {
        //添加班次
        Map<String, Object> map = add(shiftRecord);
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }
        WorkRecord workRecord = new WorkRecord();
        workRecord.setDutyDate(shiftRecord.getDutyDate());
        workRecord.setShiftType(shiftRecord.getShiftType());
        workRecord.setShiftRecordId(shiftRecord.getShiftRecordId());
        workRecord.setAddManName(shiftRecord.getAddManName());
        //添加现场负责人
        if (StringUtil.isEmpty(shiftRecord.getDutyOfficer()) == false) {
            workRecord.setWatcher(shiftRecord.getDutyOfficer());
            workRecord.setIsManager(true);
            map = workRecordService.add(workRecord);
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            }
        }
        //添加值班员
        if (StringUtil.isEmpty(shiftRecord.getWorkers()) == false) {
            workRecord.setWatcher(shiftRecord.getWorkers());
            map = workRecordService.addWorkRecord(workRecord);
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            }
        }

        return MyResponse.getSuccessResponse(shiftRecord);
    }

    @Override
    public Map<String, Object> update(ShiftRecord shiftRecord, User userSession) {
        Map<String, Object> map = verifyShiftRecord(shiftRecord.getShiftRecordId(), userSession);
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }
        shiftRecord.setDutyDate(null);
        shiftRecord.setShiftType(null);

        int count = shiftRecordMapper.updateByPrimaryKeySelective(shiftRecord);
        if (count == 1) {
            return MyResponse.getSQLSuccessResponse();
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    public Map<String, Object> verifyShiftRecord(Integer shiftRecordId, User userSession) {
        if (shiftRecordId == null || shiftRecordId <= 0) {
            return MyResponse.getErrorResponse("操作失败，请选择交接班次！");
        }
        ShiftRecord shiftRecord = shiftRecordMapper.selectByPrimaryKey(shiftRecordId);
        if (shiftRecord == null || shiftRecord.getIsDelete()) {
            return MyResponse.getErrorResponse("操作失败，交接班次不存在！");
        }
        if (userSession != null) {
            if (shiftRecord.getShiftStatus().equals(EnumManage.ShiftStatusEnum.Shift.getId()) && userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
                return MyResponse.getErrorResponse("操作失败，当前状态无法修改交接班次！");
            }
        }
        return MyResponse.getSuccessResponse(shiftRecord);
    }

    @Override
    public Map<String, Object> viewShiftRecord(Integer shiftRecordId, User userSession) {
        Map<String, Object> map = verifyShiftRecord(shiftRecordId, userSession);
        if (MyResponse.isSuccessByRootMap(map)) {
            ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
            map.put("shiftRecord", shiftRecord);
            map.put("ShiftTypeEnum", EnumManage.ShiftTypeEnum.getShiftList());
        }
        return map;
    }

    @Override
    @Transactional
    public int insertRecords(List<List<String>> watcherList, ShiftRecord shiftRecord) throws Exception {
        //1、按月份生成值班班次记录
        Map<String, Object> map = addByMonth(watcherList, shiftRecord);
        if (MyResponse.isErrorByRootMap(map)) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new Exception(MyResponse.getMsgByRootMap(map));
        }
        return Integer.parseInt(MyResponse.getDataByRootMap(map).toString());
    }

    @Override
    @Transactional
    public Map<String, Object> addByMonth(List<List<String>> watcherList, ShiftRecord shiftRecord) {
        DateTime month = new DateTime(shiftRecord.getDutyDate());
        int end = month.dayOfMonth().getMaximumValue();
        month = month.withTime(0, 0, 0, 0);
        int count = 0;
        //按月份保存每一天的值班记录
        Map<String, Object> map;
        for (int i = 1; i <= end; i++) {
            month = month.withDayOfMonth(i);
            shiftRecord.setDutyDate(month.toDate());
            map = addByDay(watcherList, shiftRecord);
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            } else {
                count = count + Integer.parseInt(MyResponse.getDataByRootMap(map).toString());
            }
        }
        return MyResponse.getSuccessResponse(count);
    }


    @Override
    @Transactional
    public Map<String, Object> addByDay(List<List<String>> watcherList, ShiftRecord shiftRecord) {

        //交接状态默认为未开始。
        shiftRecord.setShiftStatus(EnumManage.ShiftStatusEnum.NeverShift.getId());
        int count = 0;
        //添加四个班次的值班班次和值班记录
        for (EnumManage.ShiftTypeEnum e : EnumManage.ShiftTypeEnum.values()) {
            if (e.getId() > 0) {
                shiftRecord.setShiftType(e.getId());
                Map<String, Object> map = addShiftRecordAndWorkRecord(watcherList, shiftRecord);
                if (MyResponse.isErrorByRootMap(map)) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return map;
                } else {
                    count = count + Integer.parseInt(MyResponse.getDataByRootMap(map).toString());
                }
            }
        }
        return MyResponse.getSuccessResponse(count);
    }


    @Override
    @Transactional
    public Map<String, Object> addShiftRecordAndWorkRecord(List<List<String>> watcherList, ShiftRecord shiftRecord) {
        //验证是否已添加班次。
        Map<String, Object> map = workRecordService.verifyShiftRecord(shiftRecord.getDutyDate(), shiftRecord.getShiftType(), shiftRecord.getAddManName());
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }
        ShiftRecord shiftRecordInfo = (ShiftRecord) MyResponse.getDataByRootMap(map);

        int count = 0;
        if (shiftRecordInfo != null) {
            //未开始才能修改
            if (shiftRecordInfo.getShiftStatus().equals(EnumManage.ShiftStatusEnum.NeverShift.getId()) == false) {
                return MyResponse.getSuccessResponse(count);
            }
            shiftRecord = shiftRecordInfo;
            List<Integer> shiftRecordIdList = new ArrayList<>();
            shiftRecordIdList.add(shiftRecordInfo.getShiftRecordId());
            workRecordService.removeByShiftRecordIds(shiftRecordIdList, null);
        }

        //值班记录信息
        WorkRecord workRecord = new WorkRecord();
        workRecord.setShiftRecordId(shiftRecord.getShiftRecordId());
        workRecord.setDutyDate(shiftRecord.getDutyDate());
        workRecord.setShiftType(shiftRecord.getShiftType());
        workRecord.setAddManName(shiftRecord.getAddManName());
        workRecord.setIsManager(false);
        //值班日期
        DateTime dutyDate = new DateTime(shiftRecord.getDutyDate());
        int day = dutyDate.getDayOfMonth();
        //列数=日期+1
        int j = day + 1;
        int len = watcherList.size();

        for (int i = 0; i < len; i++) {
            List<String> watcherInfo = (List<String>) watcherList.get(i);
            if (watcherInfo.size() >= j) {
                String watcher = watcherInfo.get(1);
                int shiftType = Integer.parseInt(watcherInfo.get(j));
                //如果班次为当前班次，值班员不为空。则添加值班记录
                if (StringUtil.isEmpty(watcher) == false && shiftType == shiftRecord.getShiftType().intValue()) {
                    workRecord.setWatcher(watcher);
                    map = workRecordService.add(workRecord);
                    if (MyResponse.isErrorByRootMap(map)) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return map;
                    }
                    count++;
                }
            }
        }

        return MyResponse.getSuccessResponse(count);
    }

    @Override
    public ShiftRecord getByDutyDateAndShiftType(Date dutyDate, int shiftType) {
        ShiftRecordExample example = new ShiftRecordExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andShiftTypeEqualTo(shiftType)
                .andDutyDateEqualTo(DateTimeModel.getFirstTimeOfDay(dutyDate));
        List<ShiftRecord> shiftRecordList = shiftRecordMapper.selectByExample(example);
        if (shiftRecordList.isEmpty() == false) {
            return shiftRecordList.get(0);
        }
        return null;
    }


    @Override
    public Map<String, Object> getPreviousShiftRecord(Date shiftTime) {
        //获取上一班次
        ShiftRecordExample example = new ShiftRecordExample();
        example.createCriteria().andShiftTimeLessThanOrEqualTo(shiftTime)
                .andIsDeleteEqualTo(false);
        example.setOrderByClause("shift_time desc");
        example.setLimit(1);
        List<ShiftRecord> shiftRecordList = shiftRecordMapper.selectByExample(example);
        if (shiftRecordList.isEmpty() == false) {
            Map<String, Object> shiftRecordData = new HashMap<>();
            ShiftRecord shiftRecord = shiftRecordList.get(0);
            shiftRecordData.put("shiftRecord", shiftRecord);
            //获取交接内容
            List<WorkItem> workItemList = workItemService.getByShiftTime(null, shiftRecord.getShiftTime());
            shiftRecordData.put("workItemList", workItemList);
            return shiftRecordData;
        }
        return null;
    }

    @Override
    public Map<String, Object> getCurrentShiftRecordData(Date shiftTime) {
        List<ShiftRecord> shiftRecordList = getCurrentShiftRecord(shiftTime);
        if (shiftRecordList.isEmpty() == false) {
            Map<String, Object> shiftRecordData = new HashMap<>();
            ShiftRecord shiftRecord = shiftRecordList.get(0);
            shiftRecordData.put("shiftRecord", shiftRecord);
            //获取交接内容
            List<WorkItem> workItemList = workItemService.getByShiftRecordId(null, shiftRecord.getShiftRecordId());
            shiftRecordData.put("workItemList", workItemList);
            return shiftRecordData;
        }
        return null;
    }

    @Override
    public List<ShiftRecord> getCurrentShiftRecord(Date shiftTime) {
        ShiftRecordExample example = new ShiftRecordExample();
        //获取当前班次
        example.createCriteria().andShiftTimeGreaterThanOrEqualTo(shiftTime)
                .andIsDeleteEqualTo(false);
        example.setOrderByClause("shift_time asc");
        example.setLimit(1);
        List<ShiftRecord> shiftRecordList = shiftRecordMapper.selectByExample(example);
        return shiftRecordList;
    }


    @Override
    public ShiftRecord setShiftTimeByShiftType(ShiftRecord shiftRecord) {
        DateTime dateTime = new DateTime(shiftRecord.getDutyDate());
        Integer shiftType = shiftRecord.getShiftType();

        //白班交接时间为空。
        if (shiftType.equals(EnumManage.ShiftTypeEnum.Day.getId())) {
            shiftRecord.setShiftTime(null);
        }
        //早班交接时间 当天15:30
        if (shiftType.equals(EnumManage.ShiftTypeEnum.Morning.getId())) {
            shiftRecord.setShiftTime(dateTime.withTime(15, 30, 0, 0).toDate());
        }
        //夜班交接时间 当天22:30
        if (shiftType.equals(EnumManage.ShiftTypeEnum.Night.getId())) {
            shiftRecord.setShiftTime(dateTime.withTime(22, 30, 0, 0).toDate());
        }
        //宵班交接时间 第二天08:10
        if (shiftType.equals(EnumManage.ShiftTypeEnum.MidNight.getId())) {
            dateTime = dateTime.plusDays(1);
            dateTime = dateTime.withTime(8, 10, 0, 0);
            shiftRecord.setShiftTime(dateTime.toDate());
        }
        return shiftRecord;
    }


    @Override
    public Map<String, Object> updateShiftStatus(String shiftRecordIds, int updateStatus, Integer shiftStatus) {
        if (StringUtil.isEmpty(shiftRecordIds)) {
            return MyResponse.getErrorResponse("请选择交接记录！");
        }
        List<Integer> shiftRecordIdList = StringUtil.parseIdList(shiftRecordIds);
        ShiftRecordExample example = new ShiftRecordExample();
        ShiftRecordExample.Criteria criteria = example.createCriteria()
                .andShiftRecordIdIn(shiftRecordIdList)
                .andIsDeleteEqualTo(false);
        //判断是否有状态条件
        if (shiftStatus != null) {
            criteria.andShiftStatusEqualTo(shiftStatus);
        }
        //根据条件更新状态
        ShiftRecord shiftRecord = new ShiftRecord();
        shiftRecord.setShiftStatus(updateStatus);
        int count = shiftRecordMapper.updateByExampleSelective(shiftRecord, example);
        if (count > 0) {
            return MyResponse.getResponse(count);
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    public Map<String, Object> endShift(String shiftRecordIds, User userSession) {
        int updateStatus = EnumManage.ShiftStatusEnum.Shift.getId();
        int shiftStatus = EnumManage.ShiftStatusEnum.NeverShift.getId();
        Map<String, Object> map = updateShiftStatus(shiftRecordIds, updateStatus, shiftStatus);
        if (MyResponse.isSuccessByRootMap(map)) {
            List<Integer> shiftRecordIdList = StringUtil.parseIdList(shiftRecordIds);
            //修改交接内容为已交接
            workItemService.shiftByShiftRecordIds(shiftRecordIdList);
            workItemFileService.shiftByShiftRecordIds(shiftRecordIdList);
            if (shiftRecordIdList.size() == 1) {
                //将交接时间晚于完成时间的工作比较为已完成
                ShiftRecord shiftRecord = shiftRecordMapper.selectByPrimaryKey(shiftRecordIdList.get(0));
                workItemService.completeByShiftTime(shiftRecord);
            }
        }
        return map;
    }

    @Override
    @Transactional
    public Map<String, Object> remove(String shiftRecordIds, User userSession) {
        if (StringUtil.isEmpty(shiftRecordIds)) {
            return MyResponse.getErrorResponse("请选择交接记录！");
        }
        List<Integer> shiftRecordIdList = StringUtil.parseIdList(shiftRecordIds);
        ShiftRecordExample example = new ShiftRecordExample();
        ShiftRecordExample.Criteria criteria = example.createCriteria()
                .andShiftRecordIdIn(shiftRecordIdList)
                .andIsDeleteEqualTo(false);
        //普通用户只能移除未交接班次
        if (userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
            criteria.andShiftStatusEqualTo(EnumManage.ShiftStatusEnum.NeverShift.getId());
        }
        //根据条件更新状态
        ShiftRecord shiftRecord = new ShiftRecord();
        shiftRecord.setIsDelete(true);
        int count = shiftRecordMapper.updateByExampleSelective(shiftRecord, example);

        if (count > 0) {
            //删除值班记录
            workRecordService.removeByShiftRecordIds(shiftRecordIdList, userSession);
            //删除附件
            workItemFileService.removeByShiftRecordIds(shiftRecordIdList, userSession);
        }
        return MyResponse.getResponse(count);

    }

    @Override
    public Map<String, Object> exportExcel(HttpServletResponse response, String shiftRecordIds, ShiftRecord shiftRecord, Date dutyDateEnd) {
        ShiftRecordExample example = new ShiftRecordExample();
        if (StringUtil.isEmpty(shiftRecordIds) == false) {
            example.createCriteria().andShiftRecordIdIn(StringUtil.parseIdList(shiftRecordIds)).andIsDeleteEqualTo(false);
        } else {
            example = getSearchExampleByModel(shiftRecord, dutyDateEnd);
        }
        example.setOrderByClause("shift_time desc");
        //一次最多导出35天记录
        Page page = new Page(1, 35);
        example.setPage(page);
        List<ShiftRecord> shiftRecords = shiftRecordMapper.selectByExample(example);
        return exportExcelByShiftRecords(shiftRecords, response);
    }

    @Override
    public Map<String, Object> exportExcelByShiftRecords(List<ShiftRecord> shiftRecords, HttpServletResponse response) {
        if (shiftRecords.isEmpty()) {
            return MyResponse.getErrorResponse("操作失败，没有找到符合条件的交接班记录！");
        }

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet();
            //第一行设置标题
            Row row = sheet.createRow(0);
            List<String> title = BaseController.getShiftRecordExcelTitle();
            for (int i = 0; i < title.size(); i++) {
                row.createCell(i).setCellValue(title.get(i));
            }
            //设置交接时间格式
            CellStyle shiftTime = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            shiftTime.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-MM-dd  hh:mm"));
            //从第二行开始输出数据
            int rowIndex = 1;
            //导出
            for (int j = 0; j < shiftRecords.size(); j++) {
                if (shiftRecords.get(j).getShiftTime() != null) {
                    //修改日期
                    row = sheet.createRow(rowIndex);
                    Map<String, Object> map = exportExcelByShiftRecord(shiftRecords.get(j), row, shiftTime);
                    if (MyResponse.isErrorByRootMap(map)) {
                        return map;
                    }
                    rowIndex++;
                }

            }

            //以输出流的形式返回，由用户自由保存
            String fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()).toString();
            response.reset();
            OutputStream out = response.getOutputStream();
            response.setContentType("application/ms-excel;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename="
                    .concat(String.valueOf(URLEncoder.encode("历史交接班记录" + fileName + ".xlsx", "UTF-8"))));
            workbook.write(out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return MyResponse.getSQLSuccessResponse();
    }

    @Override
    public Map<String, Object> exportExcelByShiftRecord(ShiftRecord shiftRecord, Row row, CellStyle shiftTime) {
        Map<String, Object> result = getDataByShiftRecord(shiftRecord);

        //修改日期
        row.createCell(0).setCellStyle(shiftTime);
        row.getCell(0).setCellValue(shiftRecord.getShiftTime());//交接时间
        row.createCell(1).setCellValue(EnumManage.ShiftTypeEnum.getName(shiftRecord.getShiftType()) + "班");//班次
        row.createCell(2).setCellValue(shiftRecord.getDutyOfficer());//现场负责人
        row.createCell(3).setCellValue(shiftRecord.getWorkers());//值班员
        //交接内容
        List<WorkItem> workItemList = (List<WorkItem>) result.get("workItemList");
        StringBuffer workItemStrings = new StringBuffer();
        for (int i = 0; i < workItemList.size(); i++) {
            workItemStrings.append((i + 1) + "、" + workItemList.get(i).getWorkContent() + "\n" + workItemList.get(i).getHandleInformation() + "\n");
        }
        row.createCell(4).setCellValue(workItemStrings.toString());
//        长期知识
        List<AttentionPoint> longAttentionPointList = (List<AttentionPoint>) result.get("longAttentionPoints");
        StringBuffer longAttentionPointStrings = new StringBuffer();
        for (int i = 0; i < longAttentionPointList.size(); i++) {
            longAttentionPointStrings.append((i + 1) + "、" + longAttentionPointList.get(i).getPointContent() + "\n");
        }
        row.createCell(5).setCellValue(longAttentionPointStrings.toString());
//        短期知识
        List<AttentionPoint> shortAttentionPointList = (List<AttentionPoint>) result.get("shortAttentionPoints");
        StringBuffer shortAttentionPointStrings = new StringBuffer();
        for (int i = 0; i < shortAttentionPointList.size(); i++) {
            shortAttentionPointStrings.append((i + 1) + "、" + shortAttentionPointList.get(i).getPointContent() + "\n");
        }
        row.createCell(6).setCellValue(shortAttentionPointStrings.toString());

        return MyResponse.getSQLSuccessResponse();
    }

    @Override
    public Map<String, Object> getDataByShiftRecord(ShiftRecord shiftRecord) {
        Map<String, Object> map = new HashMap<>();
        if (shiftRecord.getShiftTime() != null) {
            //获取交接时间未完成的工作事项
            map.put("workItemList", workItemService.getByShiftTime(null, shiftRecord.getShiftTime()));
            //获取交接时间为结束的通知
            map.put("longAttentionPoints", attentionPointService.getByScheduleDate(shiftRecord.getShiftTime(), EnumManage.PointTypeEnum.LongTerm.getId()));
            map.put("shortAttentionPoints", attentionPointService.getByScheduleDate(shiftRecord.getShiftTime(), EnumManage.PointTypeEnum.ShortTerm.getId()));
        }
        return map;
    }

    @Override
    public ShiftRecord getCurrentByTime() {
        DateTime dateTime = new DateTime(new Date());
        ShiftRecord shiftRecord = new ShiftRecord();
        DateTime dutyDate = dateTime.withTime(0, 0, 0, 0);
        //昨天宵班08:10
        if (dateTime.isBefore(dateTime.withTime(8, 10, 0, 0))) {
            shiftRecord.setShiftType(EnumManage.ShiftTypeEnum.MidNight.getId());
            dutyDate = dutyDate.minusDays(1);
            shiftRecord.setDutyDate(dutyDate.toDate());
            return shiftRecord;
        }
        //当天早班 当天15:30
        if (dateTime.isBefore(dateTime.withTime(15, 30, 0, 0))) {
            shiftRecord.setShiftType(EnumManage.ShiftTypeEnum.Morning.getId());
            shiftRecord.setDutyDate(dutyDate.toDate());
            return shiftRecord;
        }
        //当天夜班 当天22:30
        if (dateTime.isBefore(dateTime.withTime(22, 30, 0, 0))) {
            shiftRecord.setShiftType(EnumManage.ShiftTypeEnum.Night.getId());
            shiftRecord.setDutyDate(dutyDate.toDate());
            return shiftRecord;
        }

        //当天宵班
        shiftRecord.setShiftType(EnumManage.ShiftTypeEnum.MidNight.getId());
        shiftRecord.setDutyDate(dutyDate.toDate());
        return shiftRecord;
    }

}
