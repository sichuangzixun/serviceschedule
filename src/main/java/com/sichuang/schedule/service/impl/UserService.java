package com.sichuang.schedule.service.impl;

import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.common.PasswordEncrypt;
import com.sichuang.schedule.common.StringUtil;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.dao.UserMapper;
import com.sichuang.schedule.model.entity.Page;
import com.sichuang.schedule.model.entity.User;
import com.sichuang.schedule.model.entity.UserExample;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("userService")
public class UserService implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public Map<String, Object> login(HttpServletRequest request, User user) {
        String password = PasswordEncrypt.getPassword(user.getPassword());
        UserExample example = new UserExample();
        example.createCriteria().andLoginNameEqualTo(user.getLoginName())
                .andPasswordEqualTo(password);
        // TODO Auto-generated method stub
        List<User> users = userMapper.selectByExample(example);
        if (users.isEmpty()) {
            return MyResponse.getErrorResponse("账号或密码错误！");
        }
        BaseController.saveUserSession(request, users.get(0));
        return MyResponse.getResponse(true, users.get(0));
    }

    @Override
    public Map<String, Object> logout(HttpServletRequest request) {
        // TODO Auto-generated method stub
        HttpSession httpSession = request.getSession();
        //清空对应的session
        httpSession.invalidate();
        return MyResponse.getSuccessResponse("您已退出登录");
    }

    @Override
    public Map<String, Object> changePassword(HttpServletRequest request, String oldPassword,
                                              String newPassword) {
        // TODO Auto-generated method stub
        User userInfo = BaseController.getUserSession(request);
        oldPassword = PasswordEncrypt.getPassword(oldPassword);
        if (userInfo.getPassword().equals(oldPassword) == false) {
            return MyResponse.getResponse(false, "旧密码输入错误，请重新输入！");
        }
        newPassword = PasswordEncrypt.getPassword(newPassword);
        if (userInfo.getPassword().equals(newPassword)) {
            return MyResponse.getErrorResponse("新密码与旧密码相同，请重新输入！");
        }
        //更新密码
        User user = new User();
        user.setUserId(userInfo.getUserId());
        user.setPassword(newPassword);
        int count = userMapper.updateByPrimaryKeySelective(user);
        if (count == 1) {
            return MyResponse.getResponse(true, "修改密码成功！");
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    public Map<String, Object> search(Page page) {
        UserExample example = new UserExample();
        example.createCriteria().andIsDeleteEqualTo(false);
        example.setPage(page);
        example.setOrderByClause("user_type asc, add_time desc");
        List<User> users = userMapper.selectByExample(example);
        page.setTotalRecords(userMapper.countByExample(example));
        return MyResponse.getResponse(true, page, users);
    }

    @Override
    public Map<String, Object> removeByUserIds(String userIds, User userSession) {
        if(StringUtil.isEmpty(userIds)){
            return MyResponse.getErrorResponse("请选择用户！");
        }
        List<Integer> userIdList = StringUtil.parseIdList(userIds);
        UserExample example = new UserExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andUserTypeEqualTo(EnumManage.UserTypeEnum.Normal.getId())
                .andUserIdIn(userIdList);
        User user = new User();
        user.setIsDelete(true);
        int count = userMapper.updateByExampleSelective(user, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> save(User user, User userSession) {
        int result = 0;

        if(StringUtil.isEmpty(user.getUserName())){
            return MyResponse.getErrorResponse("操作失败，用户姓名/昵称不能为空！");
        }
        String content = "用户管理";
        String userName = userSession.getUserName();

        if(user.getUserId() != null && user.getUserId() > 0){

            String password = null;
            if(StringUtil.isEmpty(user.getPassword()) == false){
                //密码加密
                password = PasswordEncrypt.getPassword(user.getPassword());
            }
            user.setPassword(password);
            result = userMapper.updateByPrimaryKeySelective(user);
            content = "修改用户";
        }else{
            if(StringUtil.isEmpty(user.getPassword())){
                return MyResponse.getErrorResponse("操作失败，密码不能为空！");
            }
            int count = countByLoginName(user.getLoginName());
            if(count > 0){
                return MyResponse.getErrorResponse("操作失败，登录账号已存在，请重新填写！");
            }

            String password = PasswordEncrypt.getPassword(user.getPassword());
            user.setPassword(password);
            user.setUserType(EnumManage.UserTypeEnum.Normal.getId());
            user.setAddTime(new Date());
            user.setAddManName(userSession.getUserName());
            result = userMapper.insertSelective(user);
            content = "添加用户";
        }
        if (result == 1) {
            return MyResponse.getSuccessResponse(user);
        }
        return MyResponse.getSQLErrorResponse();

    }

    @Override
    public int countByLoginName(String loginName) {
        UserExample example = new UserExample();
        example.createCriteria().andLoginNameEqualTo(loginName).andIsDeleteEqualTo(false);
        return userMapper.countByExample(example);
    }

    @Override
    public Map<String, Object> verifyUser(Integer userId) {
        if(userId == null || userId<=0){
            return MyResponse.getErrorResponse("操作失败，请选择用户！");
        }
        User user = userMapper.selectByPrimaryKey(userId);
        if(user == null || user.getIsDelete() ){
            return MyResponse.getErrorResponse("操作失败，用户不存在！");
        }
        return MyResponse.getSuccessResponse(user);
    }

}