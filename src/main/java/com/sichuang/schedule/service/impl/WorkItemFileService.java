package com.sichuang.schedule.service.impl;

import com.sichuang.schedule.common.FileUtils;
import com.sichuang.schedule.common.MyResponse;
import com.sichuang.schedule.common.StringUtil;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.dao.WorkItemFileMapper;
import com.sichuang.schedule.model.entity.*;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.IAttentionPointService;
import com.sichuang.schedule.service.IShiftRecordService;
import com.sichuang.schedule.service.IWorkItemFileService;
import com.sichuang.schedule.service.IWorkItemService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/7/23 15:35
 */
@Service("workItemFileService")
public class WorkItemFileService implements IWorkItemFileService {

    @Resource
    private WorkItemFileMapper workItemFileMapper;
    @Resource
    private IShiftRecordService shiftRecordService;
    @Resource
    private IWorkItemService workItemService;
    @Resource
    private IAttentionPointService attentionPointService;

    @Value("#{configProperties['filepath']}")
    private String filepath;

    @Override
    public Map<String, Object> uploadWorkItem(HttpServletRequest request, WorkItemFile workItemFile) {
        if (workItemFile.getFileType() == null) {
            return MyResponse.getErrorResponse("文件类型不能为空！");
        }
        if (workItemFile.getRecordId() == null) {
            return MyResponse.getErrorResponse("上传文件失败！");
        }
        User userSession = BaseController.getUserSession(request);
        String directory = "";
        Map<String, Object> map;
        if (workItemFile.getFileType().equals(EnumManage.FileTypeEnum.WorkItem.getId())) {
            map = workItemService.verifyWorkItem(workItemFile.getRecordId(), userSession);
            if (MyResponse.isErrorByRootMap(map)) {
                return map;
            }
            WorkItem workItem = (WorkItem) MyResponse.getDataByRootMap(map);
            //验证班次
            map = shiftRecordService.verifyShiftRecord(workItem.getAddShiftRecordId(), userSession);
            if (MyResponse.isErrorByRootMap(map)) {
                return map;
            }
            workItemFile.setAddShiftRecordId(workItem.getAddShiftRecordId());
            directory = "WorkItem/";
        }
        if (workItemFile.getFileType().equals(EnumManage.FileTypeEnum.AttentionPoint.getId())) {
            map = attentionPointService.verifyAttentionPoint(workItemFile.getRecordId(), userSession);
            if (MyResponse.isErrorByRootMap(map)) {
                return map;
            }
            directory = "AttentionPoint/";
        }

        //设置交接内容信息
        workItemFile.setAddManName(userSession.getUserName());

        //上传文件
        String fileName = "/file/" + directory + userSession.getUserId() + "_" + workItemFile.getRecordId();
        workItemFile.setFileName(fileName);
        return uploadFile(request, workItemFile);
    }

    @Override
    public Map<String, Object> uploadFile(HttpServletRequest request, WorkItemFile workItemFile) {
        String fileName = workItemFile.getFileName() + "_" + new Date().getTime();
        Map<String, Object> map = FileUtils.fileUpload(request, fileName, filepath);
        if (MyResponse.isSuccessByRootMap(map)) {
            List<Map<String, Object>> fileList = (List<Map<String, Object>>) MyResponse.getDataByRootMap(map);
            //添加文件记录
            for (int i = 0; i < fileList.size(); i++) {
                Map<String, Object> fileInfo = fileList.get(i);
                workItemFile.setFileUrl(fileInfo.get("newFilePath").toString());
                workItemFile.setFileName(fileInfo.get("oldFileName").toString());
                add(workItemFile);
            }
            return MyResponse.getSQLSuccessResponse();
        }
        return map;
    }

    @Override
    public Map<String, Object> add(WorkItemFile workItemFile) {
        //设置系统信息
        workItemFile.setAddTime(new Date());
        workItemFile.setIsDelete(false);

        int count = workItemFileMapper.insertSelective(workItemFile);
        if (count != 1) {
            return MyResponse.getErrorResponse("未知原因！");
        }
        return MyResponse.getSQLSuccessResponse();
    }

    @Override
    public List<WorkItemFile> get(int recordId, int fileType) {
        WorkItemFileExample example = new WorkItemFileExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andFileTypeEqualTo(fileType)
                .andRecordIdEqualTo(recordId);
        return workItemFileMapper.selectByExample(example);
    }

    @Override
    public Map<String, Object> removeByFileIds(String fileIds, User userSession) {
        if (StringUtil.isEmpty(fileIds)) {
            return MyResponse.getErrorResponse("请选择交接内容附件！");
        }
        //获取ID
        List<Integer> fileIdList = StringUtil.parseIdList(fileIds);
        WorkItemFileExample example = getExampleByIds(fileIdList, 2, userSession);
        return removeByExample(example);
    }

    @Override
    public Map<String, Object> removeByWorkItemIds(List<Integer> workItemIdList, User userSession) {
        WorkItemFileExample example = getExampleByIds(workItemIdList, 1, userSession);
        return removeByExample(example);
    }

    @Override
    public Map<String, Object> removeByShiftRecordIds(List<Integer> shiftRecordIdList, User userSession) {
        WorkItemFileExample example = getExampleByIds(shiftRecordIdList, 3, userSession);
        return removeByExample(example);
    }

    @Override
    public Map<String, Object> shiftByShiftRecordIds(List<Integer> shiftRecordIdList) {
        WorkItemFileExample example = new WorkItemFileExample();
        example.createCriteria().andAddShiftRecordIdIn(shiftRecordIdList)
                .andIsDeleteEqualTo(false);
        WorkItemFile workItemFile = new WorkItemFile();
        workItemFile.setIsShift(true);
        int count = workItemFileMapper.updateByExampleSelective(workItemFile, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> removeByExample(WorkItemFileExample example) {
        //获取文件路径
        List<WorkItemFile> workItemFileList = workItemFileMapper.selectByExample(example);
        int count = 0;
        if (workItemFileList.isEmpty() == false) {
            //删除记录
            WorkItemFile workItemFile = new WorkItemFile();
            workItemFile.setIsDelete(true);
            count = workItemFileMapper.updateByExampleSelective(workItemFile, example);

            //删除文件
            for (int i = 0; i < workItemFileList.size(); i++) {
                FileUtils.deleteFile(filepath + workItemFileList.get(i).getFileUrl());
            }
        }

        return MyResponse.getResponse(count);
    }

    @Override
    public WorkItemFileExample getExampleByIds(List<Integer> idList, int type, User userSession) {
        //获取搜索条件
        WorkItemFileExample example = new WorkItemFileExample();
        WorkItemFileExample.Criteria criteria = example.createCriteria()
                .andIsDeleteEqualTo(false);
        //普通用户只能删除未交接工作
        if (userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
            criteria.andIsShiftEqualTo(false);
        }
        if (idList.isEmpty() == false) {
            switch (type) {
                case 1:
                    criteria.andRecordIdIn(idList);
                    break;
                case 2:
                    criteria.andFileIdIn(idList);
                    break;
                case 3:
                    criteria.andAddShiftRecordIdIn(idList);
                    break;
            }
        }

        return example;
    }
}
