package com.sichuang.schedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.sichuang.schedule.base.dao.BWorkItemMapper;
import com.sichuang.schedule.common.*;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.dao.WorkItemMapper;
import com.sichuang.schedule.model.entity.*;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.common.ExcelUtils;
import com.sichuang.schedule.service.IShiftRecordService;
import com.sichuang.schedule.service.IWorkItemFileService;
import com.sichuang.schedule.service.IWorkItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.List;

@Service("workItemService")
public class WorkItemService implements IWorkItemService {

    @Resource
    private WorkItemMapper workItemMapper;
    @Resource
    private IShiftRecordService shiftRecordService;
    @Resource
    private BWorkItemMapper bWorkItemMapper;
    @Resource
    private IWorkItemFileService workItemFileService;


    @Override
    public Map<String, Object> getByModelAndPage(WorkItem workItem, Date addTimeEnd, Date endTimeEnd, Page page) {
        WorkItemExample example = getSearchExampleByModel(workItem,addTimeEnd, endTimeEnd);
        example.setPage(page);
        example.setOrderByClause("add_time desc");
        if (page.getPageNo() == 1 || page.getTotalRecords() == 0) {
            page.setTotalRecords(workItemMapper.countByExample(example));
        }
        List<WorkItem> workItemList = new ArrayList<>();
        if (page.getTotalRecords() > 0) {
            workItemList = workItemMapper.selectByExampleWithBLOBs(example);
        }
        return MyResponse.getResponse(true, page, workItemList);
    }

    @Override
    public Map<String, Object> getWorkItem(WorkItem workItem, Date addTimeEnd, Date endTimeEnd, Page page) {
        WorkItemExample example = getSearchExampleByModel(workItem, addTimeEnd, endTimeEnd);
        example.setPage(page);

        List<WorkItem> workItemList = new ArrayList<>();

        if (page.getPageNo() == 1 || page.getTotalRecords() == 0) {
            page.setTotalRecords(workItemMapper.countByExample(example));
        }
        if (page.getTotalRecords() > 0) {
            example.setOrderByClause("add_time desc");
            workItemList = workItemMapper.selectByExample(example);
        }

        return MyResponse.getResponse(true, page, workItemList);
    }

    @Override
    public WorkItemExample getSearchExampleByModel(WorkItem workItem, Date addTimeEnd, Date endTimeEnd) {
        WorkItemExample example = new WorkItemExample();
        WorkItemExample.Criteria criteria = example.createCriteria();
        if (workItem.getIsDelete() != null) {
            criteria.andIsDeleteEqualTo(workItem.getIsDelete());
        }
        if (workItem.getIsComplete() != null) {
            criteria.andIsCompleteEqualTo(workItem.getIsComplete());
        }
        if (StringUtil.isEmpty(workItem.getWorkContent()) == false) {
            criteria.andWorkContentLike("%" + workItem.getWorkContent() + "%");
        }
        if (workItem.getAddShiftType() != null) {
            criteria.andAddShiftTypeEqualTo(workItem.getAddShiftType());
        }
        if (workItem.getEndShiftType() != null) {
            criteria.andEndShiftTypeEqualTo(workItem.getEndShiftType());
        }
        if (StringUtil.isEmpty(workItem.getItemTypeNames()) == false) {
            criteria.andItemTypeNamesEqualTo(workItem.getItemTypeNames());
        }
        if (workItem.getAddTime() != null) {
            criteria.andAddTimeGreaterThanOrEqualTo(DateTimeModel.getFirstTimeOfDay(workItem.getAddTime()));
        }
        if (addTimeEnd != null) {
            criteria.andAddTimeLessThanOrEqualTo(DateTimeModel.getLastTimeOfDay(addTimeEnd));
        }
        if (workItem.getEndTime() != null) {
            criteria.andEndTimeGreaterThanOrEqualTo(DateTimeModel.getFirstTimeOfDay(workItem.getEndTime()));
        }
        if (endTimeEnd != null) {
            criteria.andEndTimeLessThanOrEqualTo(DateTimeModel.getLastTimeOfDay(endTimeEnd));
        }
        return example;
    }

    @Override
    public List<WorkItem> getByIsCompleteAndShiftRecord(boolean isComplete, Date dutyDate, int shiftType) {

        ShiftRecord shiftRecord = shiftRecordService.getByDutyDateAndShiftType(dutyDate, shiftType);
        if (shiftRecord != null) {
            List<WorkItem> workItemList = getByShiftRecordId(isComplete, shiftRecord.getShiftRecordId());
            return workItemList;
        }
        return new ArrayList<>();
    }


    @Override
    public Map<String, Object> add(WorkItem workItem) {
        if (workItem.getAddShiftRecordId() == null) {
            return MyResponse.getErrorResponse("请选择交接记录！");
        }
        //检查字段
        if (StringUtil.isEmpty(workItem.getWorkContent())) {
            return MyResponse.getErrorResponse("工作事项内容不能为空！");
        }
        //设置系统默认值
        workItem.setIsDelete(false);
        workItem.setAddTime(new Date());
        workItem.setWorkItemId(null);
        int count = workItemMapper.insertSelective(workItem);
        if (count == 1) {
            return MyResponse.getSuccessResponse(workItem);
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    @Transactional
    public Map<String, Object> addWorkItem(WorkItem workItem, User userSession) {
        //验证交接记录
        Map<String, Object> map = shiftRecordService.verifyShiftRecord(workItem.getAddShiftRecordId(), userSession);
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }
        ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
        workItem.setAddShiftType(shiftRecord.getShiftType());
        //处理情况单独更新
        String handleInformation = workItem.getHandleInformation().toString();
        if(StringUtil.isEmpty(workItem.getHandleInformation()) == false ){
            workItem.setHandleInformation("");
        }
        //添加工作内容
        map = add(workItem);
        if (MyResponse.isErrorByRootMap(map) ) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }else {
            //更新处理情况
           if(StringUtil.isEmpty(handleInformation) == false){
               workItem.setHandleInformation(handleInformation);
               map = handle(workItem, shiftRecord.getShiftRecordId(), userSession);
               if(MyResponse.isErrorByRootMap(map)){
                   TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
               }
           }
        }
        return map;
    }

    @Override
    @Transactional
    public Map<String, Object> update(WorkItem workItem, User userSession) {
        //检查字段
        if (StringUtil.isEmpty(workItem.getWorkContent())) {
            return MyResponse.getErrorResponse("工作事项内容不能为空！");
        }
        //验证工作事项
        Map<String, Object> map = verifyShiftRecord(workItem.getWorkItemId(), userSession);
        if (MyResponse.isErrorByRootMap(map) ) {
            return map;
        }
        workItem.setAddShiftRecordId(null);
        int count = workItemMapper.updateByPrimaryKeySelective(workItem);
        if (count == 1) {
            return MyResponse.getSQLSuccessResponse();
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    public Map<String, Object> verifyWorkItem(Integer workItemId, User userSession) {
        if (workItemId == null || workItemId <= 0) {
            return MyResponse.getErrorResponse("请选择交接内容！");
        }
        WorkItem workItem = workItemMapper.selectByPrimaryKey(workItemId);
        if (workItem == null || workItem.getIsDelete()) {
            return MyResponse.getErrorResponse("操作失败，交接内容不存在！");
        }
        if (userSession != null) {
            if (workItem.getIsShift() && userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
                return MyResponse.getErrorResponse("操作失败，已交接工作事项不能修改！");
            }
        }
        return MyResponse.getSuccessResponse(workItem);
    }

    @Override
    public Map<String, Object> removeByWorkItemIds(String workItemIds, User userSession) {
        if(StringUtil.isEmpty(workItemIds)){
            return MyResponse.getErrorResponse("请选择工作事项！");
        }
        List<Integer> workItemIdList = StringUtil.parseIdList(workItemIds);
        WorkItemExample example = new WorkItemExample();
        WorkItemExample.Criteria criteria = example.createCriteria().andWorkItemIdIn(workItemIdList)
                .andIsDeleteEqualTo(false);
        //普通用户只能删除未交接工作
        if (userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
            criteria.andIsShiftEqualTo(false);
        }
        WorkItem workItem = new WorkItem();
        workItem.setIsDelete(true);
        int count = workItemMapper.updateByExampleSelective(workItem, example);
        if(count > 0){
            //删除附件
            workItemFileService.removeByWorkItemIds(workItemIdList, userSession);
        }
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> completeByWorkItemIds(String workItemIds, User userSession) {
        if(StringUtil.isEmpty(workItemIds)){
            return MyResponse.getErrorResponse("请选择交接内容！");
        }
        List<Integer> workItemIdList = StringUtil.parseIdList(workItemIds);
        WorkItemExample example = new WorkItemExample();
        WorkItemExample.Criteria criteria = example.createCriteria().andWorkItemIdIn(workItemIdList)
                .andIsCompleteEqualTo(false)
                .andIsDeleteEqualTo(false);

        //普通用户只能删除未交接工作
        if (userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
            criteria.andIsShiftEqualTo(false);
        }
        WorkItem workItem = new WorkItem();
        workItem.setIsComplete(true);
        workItem.setEndTime(new Date());
        workItem.setEndHandlerName(userSession.getUserName());
        int count = workItemMapper.updateByExampleSelective(workItem, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public List<WorkItem> getByShiftRecordId(Boolean isComplete, int shiftRecordId) {
        WorkItemExample example = new WorkItemExample();
        WorkItemExample.Criteria criteria = example.createCriteria().andAddShiftRecordIdEqualTo(shiftRecordId)
                .andIsDeleteEqualTo(false);
        if (isComplete != null) {
            criteria.andIsCompleteEqualTo(isComplete);
        }
        example.setOrderByClause("item_type_id asc");
        return workItemMapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<WorkItem> getByShiftTime(Boolean isComplete, Date shiftTime) {
        if(shiftTime == null){
            return new ArrayList<>();
        }
        WorkItemExample example = new WorkItemExample();

        WorkItemExample.Criteria criteria = example.createCriteria().andAddTimeLessThanOrEqualTo(shiftTime)
                .andEndTimeGreaterThan(shiftTime)
                .andIsDeleteEqualTo(false);

        WorkItemExample.Criteria endCriteria = example.createCriteria().andAddTimeLessThanOrEqualTo(shiftTime)
                .andEndTimeIsNull()
                .andIsDeleteEqualTo(false);
        if (isComplete != null) {
            criteria.andIsCompleteEqualTo(isComplete);
            endCriteria.andIsCompleteEqualTo(isComplete);
        }
        example.or(endCriteria);
        example.setOrderByClause("item_type_id asc");
        return workItemMapper.selectByExampleWithBLOBs(example);
    }

    public Map<String, Object> exportExcel(WorkItem workItem, Date addTimeEnd, Date endTimeEnd, HttpServletResponse response) {

        Map<String, Object> map = getByModelAndPage(workItem, addTimeEnd, endTimeEnd, new Page(1, 60000));
        List<WorkItem> workItems = (List<WorkItem>) MyResponse.getDataByRootMap(map);
        if (workItems.isEmpty()) {
            return MyResponse.getErrorResponse("操作失败，没有找到符合条件的交接内容！");
        }
        LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String, String>();
        fieldMap.put("addTime#"+DateTimeModel.DATE_TYPE_4, "录入时间");
        fieldMap.put("addShiftType#" + JSON.toJSONString(EnumManage.ShiftTypeEnum.getList()), "录入班次");
        fieldMap.put("endTime#"+DateTimeModel.DATE_TYPE_4, "完成时间");
        fieldMap.put("endShiftType#" + JSON.toJSONString(EnumManage.ShiftTypeEnum.getList()), "完成班次");
        fieldMap.put("endHandlerName", "完成人");
        fieldMap.put("itemTypeNames", "标签");
        fieldMap.put("workContent", "工作内容");
        fieldMap.put("handleInformation", "处理情况");
        fieldMap.put("isComplete#"+JSON.toJSONString(EnumManage.BooleanTypeEnum.getList()), "完成状态");

        try {
            ExcelUtils.listToExcel(workItems, fieldMap, "历史工作内容表", response);
        } catch (ExcelException e) {
            e.printStackTrace();
            return MyResponse.getErrorResponse();
        }
        return MyResponse.getSuccessResponse();
    }

    @Override
    @Transactional
    public Map<String, Object> complete(WorkItem workItem, Integer shiftRecordId, User userSession) {
        if(workItem.getEndTime() == null){
            return MyResponse.getErrorResponse("操作失败，完成时间不能为空！");
        }
        Map<String, Object> map = verifyWorkItem(workItem.getWorkItemId(), null);
        if(MyResponse.isErrorByRootMap(map)){
            return map;
        }
        map = shiftRecordService.viewShiftRecord(shiftRecordId, userSession);
        if(MyResponse.isErrorByRootMap(map)){
            return map;
        }
        //设置工作已完成，完成班次
        ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
        workItem.setIsComplete(true);
        workItem.setEndShiftType(shiftRecord.getShiftType());
        //处理情况累加
        if(StringUtil.isEmpty(workItem.getHandleInformation()) == false){
            map = handle(workItem, shiftRecordId, userSession);
            if(MyResponse.isErrorByRootMap(map)){
                return map;
            }
            workItem.setHandleInformation(null);
        }
        int count = workItemMapper.updateByPrimaryKeySelective(workItem);
        if(count == 1){
            return MyResponse.getSuccessResponse(workItem);
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    public Map<String, Object> shiftByShiftRecordIds(List<Integer> shiftRecordIdList) {
        WorkItemExample example = new WorkItemExample();
        example.createCriteria().andAddShiftRecordIdIn(shiftRecordIdList)
                .andIsDeleteEqualTo(false)
                .andIsShiftEqualTo(false);
        WorkItem workItem = new WorkItem();
        workItem.setIsShift(true);
        int count = workItemMapper.updateByExampleSelective(workItem, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> completeByShiftTime(ShiftRecord shiftRecord) {
        WorkItemExample example = new WorkItemExample();
        example.createCriteria().andEndTimeLessThanOrEqualTo(shiftRecord.getShiftTime())
                .andIsDeleteEqualTo(false)
                .andIsCompleteEqualTo(false);
        WorkItem workItem = new WorkItem();
        workItem.setIsComplete(true);
        workItem.setEndShiftType(shiftRecord.getShiftType());
        int count = workItemMapper.updateByExampleSelective(workItem, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> handle(WorkItem workItem, Integer shiftRecordId, User userSession) {
        if(StringUtil.isEmpty(workItem.getHandleInformation()) ){
            return MyResponse.getErrorResponse("操作失败，处理情况不能为空！");
        }
        //验证处理班次及权限
        Map<String, Object> map = shiftRecordService.verifyShiftRecord(shiftRecordId, userSession);
        if(MyResponse.isErrorByRootMap(map)){
            return map;
        }
        ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
        //验证工作内容
        map = verifyWorkItem(workItem.getWorkItemId(), null);
        if(MyResponse.isErrorByRootMap(map)){
            return map;
        }
        String shiftTime = DateTimeModel.getDateString(BaseController.TimeFormat, shiftRecord.getShiftTime());
        String handleInformation = shiftTime + " " + EnumManage.ShiftTypeEnum.getName(shiftRecord.getShiftType()) + "班：" + workItem.getHandleInformation();
        workItem.setHandleInformation(handleInformation);
        int count = bWorkItemMapper.updateHandleInformation(workItem);
        if(count == 1){
            return MyResponse.getSuccessResponse(workItem);
        }
        return MyResponse.getErrorResponse("更新处理情况失败！");
    }

    @Override
    public Map<String, Object> verifyShiftRecord(Integer workItemId, User userSession) {
        Map<String, Object> map = verifyWorkItem(workItemId, userSession);
        if (MyResponse.isSuccessByRootMap(map)) {
            WorkItem workItem = (WorkItem) MyResponse.getDataByRootMap(map);
            //验证交接记录
            map = shiftRecordService.verifyShiftRecord(workItem.getAddShiftRecordId(), userSession);
            if (MyResponse.isSuccessByRootMap(map)) {
                ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
                map.put("shiftRecord", shiftRecord);
                map.put("workItem", workItem);
            }
        }

        return map;
    }

}
