package com.sichuang.schedule.service.impl;

import com.sichuang.schedule.base.dao.BShiftRecordMapper;
import com.sichuang.schedule.common.*;
import com.sichuang.schedule.controller.BaseController;
import com.sichuang.schedule.dao.WorkRecordMapper;
import com.sichuang.schedule.model.entity.*;
import com.sichuang.schedule.other.EnumManage;
import com.sichuang.schedule.service.*;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("workRecordService")
public class WorkRecordService implements IWorkRecordService {

    @Resource
    private WorkRecordMapper workRecordMapper;
    @Resource
    private IShiftRecordService shiftRecordService;
    @Resource
    private IWorkItemFileService workItemFileService;
    @Resource
    private IItemTypeService itemTypeService;
    @Resource
    private BShiftRecordMapper bShiftRecordMapper;
    @Value("#{configProperties['filepath']}")
    private String filepath;

    @Override
    @Transactional
    public Map<String, Object> add(WorkRecord workRecord) {
        if (StringUtil.isEmpty(workRecord.getWatcher())) {
            return MyResponse.getErrorResponse("操作失败，值班员姓名不能为空！");
        }
        if (workRecord.getShiftType() == null) {
            return MyResponse.getErrorResponse("操作失败，请选择值班班次！");
        }
        if (workRecord.getDutyDate() == null) {
            return MyResponse.getErrorResponse("操作失败，值班日期不能为空！");
        }

        //值班日期设置时分秒
        DateTime dutyDate = new DateTime(workRecord.getDutyDate());
        dutyDate = dutyDate.withTime(0,0,0,0);
        workRecord.setDutyDate(dutyDate.toDate());

        //验证值班班次
        if(workRecord.getShiftRecordId() == null){
            Map<String, Object> map = verifyShiftRecord(workRecord.getDutyDate(), workRecord.getShiftType(), workRecord.getAddManName());
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            }
            ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
            //更新值班班次
            workRecord.setShiftRecordId(shiftRecord.getShiftRecordId());
        }


        //验证值班员
        WorkRecord verifyWatcher = getByWatcherAndShiftRecordId(workRecord.getShiftRecordId(), workRecord.getWatcher());
        if (verifyWatcher != null) {
            return MyResponse.getErrorResponse("操作失败，该值班员不能重复添加"+workRecord.getWatcher()+workRecord.getDutyDate().toString()+ EnumManage.ShiftTypeEnum.getName(workRecord.getShiftType()));
        }
        //设置默认信息
        workRecord.setIsShift(false);
        workRecord.setAddTime(new Date());
        workRecord.setIsDelete(false);
        workRecord.setWorkRecordId(null);
        int count = workRecordMapper.insertSelective(workRecord);
        if (count == 1) {
            if(workRecord.getIsManager() == false) {
                //更新值班员名单
                List<Integer> shiftRecordIdList = new ArrayList<>();
                shiftRecordIdList.add(workRecord.getShiftRecordId());
                bShiftRecordMapper.updateWorkers(shiftRecordIdList);
            }
            return MyResponse.getSuccessResponse(workRecord);
        }
        return MyResponse.getSQLErrorResponse();
    }

    @Override
    @Transactional
    public Map<String, Object> addWorkRecord(WorkRecord workRecord) {
        if (StringUtil.isEmpty(workRecord.getWatcher())) {
            return MyResponse.getErrorResponse("操作失败，值班员姓名不能为空！");
        }
        //批量添加值班员。默认为普通值班员
        workRecord.setIsManager(false);
        String[] watcherList = workRecord.getWatcher().split(",");
        for (String watcher : watcherList) {
            workRecord.setWatcher(watcher);
            //添加值班记录
            Map<String, Object> map = add(workRecord);
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            }
        }
        return MyResponse.getSQLSuccessResponse();
    }

    @Override
    @Transactional
    public Map<String, Object> update(WorkRecord workRecord, User userSession) {
        if (workRecord.getShiftType() == null) {
            return MyResponse.getErrorResponse("操作失败，请选择值班班次！");
        }
        //验证工作记录是否存在
        Map<String, Object> map = verifyWorkRecord(workRecord.getWorkRecordId(), userSession);
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }
        WorkRecord workRecordInfo = (WorkRecord) MyResponse.getDataByRootMap(map);
        //验证值班班次
        map = verifyShiftRecord(workRecordInfo.getDutyDate(), workRecord.getShiftType(), userSession.getUserName());
        if (MyResponse.isErrorByRootMap(map)) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return map;
        }
        ShiftRecord shiftRecord = (ShiftRecord) MyResponse.getDataByRootMap(map);
        //更新值班班次
        workRecord.setShiftRecordId(shiftRecord.getShiftRecordId());

        if (workRecord.getIsManager()) {
            //更新值班班次现场负责人
            map = updateManager(workRecordInfo.getWatcher(), workRecordInfo.getShiftRecordId(), userSession);
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            }
        }
        //值班员姓名和值班日期不能修改
        workRecord.setWatcher(null);
        workRecord.setDutyDate(null);
        int count = workRecordMapper.updateByPrimaryKeySelective(workRecord);
        if (count != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return MyResponse.getSQLErrorResponse();
        }
        //更新值班员名单
        List<Integer> shiftRecordIdList = new ArrayList<>();
        shiftRecordIdList.add(workRecord.getShiftRecordId());
        bShiftRecordMapper.updateWorkers(shiftRecordIdList);
        return MyResponse.getSQLSuccessResponse();
    }

    @Override
    public Map<String, Object> getByModelAndPage(WorkRecord workRecord, Date dutyDateEnd, Page page) {
        workRecord.setIsDelete(false);
        WorkRecordExample example = getSearchExampleByModel(workRecord, dutyDateEnd);
        example.setPage(page);
        example.setOrderByClause("duty_date desc, shift_type desc");
        if (page.getPageNo() == 1 || page.getTotalRecords() == 0) {
            page.setTotalRecords(workRecordMapper.countByExample(example));
        }
        List<WorkRecord> workRecordList = new ArrayList<>();
        if (page.getTotalRecords() > 0) {
            workRecordList = workRecordMapper.selectByExample(example);
        }

        return MyResponse.getResponse(true, page, workRecordList);
    }

    @Override
    public WorkRecordExample getSearchExampleByModel(WorkRecord workRecord, Date dutyDateEnd) {
        WorkRecordExample example = new WorkRecordExample();
        WorkRecordExample.Criteria criteria = example.createCriteria();
        if (workRecord.getShiftType() != null) {
            criteria.andShiftTypeEqualTo(workRecord.getShiftType());
        }
        if (workRecord.getIsDelete() != null) {
            criteria.andIsDeleteEqualTo(workRecord.getIsDelete());
        }
        if (StringUtil.isEmpty(workRecord.getWatcher()) == false) {
            criteria.andWatcherEqualTo(workRecord.getWatcher());
        }
        if (workRecord.getDutyDate() != null) {
            criteria.andDutyDateGreaterThanOrEqualTo(DateTimeModel.getFirstTimeOfDay(workRecord.getDutyDate()));
        }
        if (dutyDateEnd != null) {
            criteria.andDutyDateLessThanOrEqualTo(DateTimeModel.getLastTimeOfDay(dutyDateEnd));
        }
        if(workRecord.getShiftRecordId() != null){
            criteria.andShiftRecordIdEqualTo(workRecord.getShiftRecordId());
        }
        return example;
    }

    @Override
    public Map<String, Object> verifyWorkRecord(Integer workRecordId, User userSession) {
        if (workRecordId == null || workRecordId <= 0) {
            return MyResponse.getErrorResponse("请选择值班员记录！");
        }
        WorkRecord workRecord = workRecordMapper.selectByPrimaryKey(workRecordId);
        if (workRecord == null || workRecord.getIsDelete()) {
            return MyResponse.getErrorResponse("值班员工作记录不存在！");
        }
        if (userSession != null) {
            if(workRecord.getIsShift() && userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())){
                return MyResponse.getErrorResponse("操作失败，已交接班次不能修改！");
            }
        }
        return MyResponse.getSuccessResponse(workRecord);
    }

    @Override
    public Map<String, Object> removeByWorkRecordIds(String workRecordIds, User userSession) {
        if (StringUtil.isEmpty(workRecordIds)) {
            return MyResponse.getErrorResponse("请选择值班记录！");
        }
        List<Integer> workRecordIdList = StringUtil.parseIdList(workRecordIds);
        WorkRecordExample example = new WorkRecordExample();
        example.createCriteria().andWorkRecordIdIn(workRecordIdList)
                .andIsShiftEqualTo(false)
                .andIsDeleteEqualTo(false);

        List<Integer> shiftRecordIdList = bShiftRecordMapper.getShiftRecordIdList(example);

        //删除记录
        WorkRecord workRecord = new WorkRecord();
        workRecord.setIsDelete(true);
        int count = workRecordMapper.updateByExampleSelective(workRecord, example);
        //更新班次值班员
        bShiftRecordMapper.updateWorkers(shiftRecordIdList);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> removeByShiftRecordIds(List<Integer> shiftRecordIdList, User userSession) {
        WorkRecordExample example = new WorkRecordExample();
        WorkRecordExample.Criteria criteria = example.createCriteria().andShiftRecordIdIn(shiftRecordIdList)
                .andIsDeleteEqualTo(false);
        if(userSession != null && userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())){
            criteria.andIsShiftEqualTo(false);
        }
        WorkRecord workRecord = new WorkRecord();
        workRecord.setIsDelete(true);
        int count = workRecordMapper.updateByExampleSelective(workRecord, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> removeByMonth(Date dutyDate) {
        DateTime month = new DateTime(dutyDate);
        int end = month.dayOfMonth().getMaximumValue();
        WorkRecordExample example = new WorkRecordExample();
        example.createCriteria().andDutyDateGreaterThanOrEqualTo(month.withDayOfMonth(1).withTime(0,0,0,0).toDate())
                .andDutyDateLessThanOrEqualTo(month.withDayOfMonth(end).withTime(23,59,59,0).toDate())
                .andIsShiftEqualTo(false)
                .andIsDeleteEqualTo(false);
        WorkRecord workRecord = new WorkRecord();
        workRecord.setIsDelete(true);
        int count = workRecordMapper.updateByExampleSelective(workRecord, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public WorkRecord getByWatcherAndShiftRecordId(int shiftRecordId, String watcher) {
        WorkRecordExample example = new WorkRecordExample();
        example.createCriteria().andShiftRecordIdEqualTo(shiftRecordId)
                .andWatcherEqualTo(watcher)
                .andIsDeleteEqualTo(false);
        List<WorkRecord> workRecordList = workRecordMapper.selectByExample(example);
        if (workRecordList.isEmpty() == false) {
            return workRecordList.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public Map<String, Object> updateManager(String watcher, int shiftRecordId, User userSession) {
        //验证班次
        Map<String, Object> map = shiftRecordService.verifyShiftRecord(shiftRecordId, userSession);
        if (MyResponse.isErrorByRootMap(map)) {
            return map;
        }
        //旧负责人
        List<WorkRecord> workRecordList = getByShiftRecordId(shiftRecordId, true);
        //将原负责人改为普通员工
        if (workRecordList.isEmpty() == false) {
            //姓名相同不修改
            if (workRecordList.get(0).getWatcher().equals(watcher)) {
                return MyResponse.getDefaultSuccessResponse();
            }
            //不相同，将原负责人设为非负责人
            WorkRecord updateWatcher = new WorkRecord();
            updateWatcher.setIsManager(false);
            updateWatcher.setWorkRecordId(workRecordList.get(0).getWorkRecordId());
            int count = workRecordMapper.updateByPrimaryKeySelective(updateWatcher);
            if (count != 1) {
                return MyResponse.getSQLErrorResponse();
            }
        }
        //更新值班班次现场负责人
        ShiftRecord updateShiftRecord = new ShiftRecord();
        updateShiftRecord.setShiftRecordId(shiftRecordId);
        updateShiftRecord.setDutyOfficer(watcher);
        return shiftRecordService.update(updateShiftRecord, userSession);
    }

    @Override
    public List<WorkRecord> getByShiftRecordId(int shiftRecordId, Boolean isManager) {
        WorkRecordExample example = new WorkRecordExample();
        WorkRecordExample.Criteria criteria = example.createCriteria().andShiftRecordIdEqualTo(shiftRecordId)
                .andIsDeleteEqualTo(false);
        if (isManager != null) {
            criteria.andIsManagerEqualTo(isManager);
        }
        example.setOrderByClause("is_manager desc");
        return workRecordMapper.selectByExample(example);
    }

    @Override
    public Map<String, Object> importExcel(String filename, ShiftRecord shiftRecord) {
        ExcelReader example = new ExcelReader();
        List<List<String>> watcherList = new ArrayList<List<String>>();
        String prefix = filename.substring(filename.lastIndexOf(".") + 1);
        if (prefix.equals("xlsx") == false) {
            return MyResponse.getErrorResponse("导入失败！文件名后缀必须为xlsx！");
        }

        int count = 0;
        System.out.println("-- 程序开始 --" + filename);
        long time_1 = System.currentTimeMillis();
        try {
            watcherList = example.processOneSheet(filename, 1);
            if (watcherList.size() == 0) {
                return MyResponse.getErrorResponse("导入失败！");
            } else {
                count = shiftRecordService.insertRecords(watcherList, shiftRecord);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return MyResponse.getErrorResponse("导入失败！" + e.getMessage());
        }

        String msg = "成功导入" + count + "条记录！";
        long time_2 = System.currentTimeMillis();
        long runtime = time_2 - time_1;
        System.out.println("-- 程序结束 --");
        System.out.println("-- 耗时 --" + runtime + "ms");
        msg = msg + "耗时" + DateTimeModel.formatTime(runtime) + "！";
        return MyResponse.getResponse(true, msg);
    }

    @Override
    public Map<String, Object> upload(HttpServletRequest request, ShiftRecord shiftRecord) {
        //上传文件
        User userSession = BaseController.getUserSession(request);
        String url = "/file/workRecord/";
        String fileName = url + userSession.getUserId() + "_" + new Date().getTime();
        Map<String, Object> map = FileUtils.fileUpload(request, fileName, filepath);
        if (MyResponse.isSuccessByRootMap(map) == false) {
            return MyResponse.getResponse(false, "上传文件失败！");
        }
        if (map.get("data") != null) {
            ArrayList<Object> urls = (ArrayList<Object>) MyResponse.getDataByRootMap(map);
            Map<String, Object> fileUrl = (Map<String, Object>) urls.get(0);
            //保存文件路径
            addWorkRecordFile(fileUrl, userSession);
            //导入排班表
            String filename = filepath + fileUrl.get("newFilePath");
            return importExcel(filename, shiftRecord);
        }
        return MyResponse.getErrorResponse();
    }

    @Override
    public Map<String, Object> addWorkRecordFile(Map<String, Object> fileUrl, User userSession) {
        WorkItemFile workItemFile = new WorkItemFile();
        workItemFile.setFileType(EnumManage.FileTypeEnum.WorkRecord.getId());
        workItemFile.setAddManName(userSession.getUserName());
        workItemFile.setFileUrl(fileUrl.get("newFilePath").toString());
        workItemFile.setFileName(fileUrl.get("oldFileName").toString());
        return workItemFileService.add(workItemFile);
    }

    @Override
    public List<List<String>> getByMonth(Date dutyDate) {
        //获取值班员名单
        List<ItemType> watcherList = itemTypeService.getList(EnumManage.DataTypeEnum.Watcher.getId());
        List<List<String>> result = new ArrayList<List<String>>();
        for (int i = 0; i < watcherList.size(); i++) {
            List<String> workRecord = getByWatcher(watcherList.get(i).getTypeName(), dutyDate);
            result.add(workRecord);
        }
        return result;
    }

    @Override
    public List<String> getByWatcher(String watcher, Date dutyDate) {
        DateTime month = new DateTime(dutyDate);
        int end = month.dayOfMonth().getMaximumValue();
        List<String> watcherList = new ArrayList<String>(end);
        watcherList.add(watcher);
        //默认值班班次为休
        for (int i = 1; i <= end; i++) {
            watcherList.add(i, "<span class='text-danger'>" + EnumManage.ShiftTypeEnum.Null.getName() + "</span>");
        }
        //重新填写值班班次
        List<WorkRecord> workRecordList = getByMonthAndWatcher(watcher, dutyDate);
        for (int i = 0; i < workRecordList.size(); i++) {
            WorkRecord workRecord = workRecordList.get(i);
            DateTime day = new DateTime(workRecord.getDutyDate());
            watcherList.set(day.getDayOfMonth(), EnumManage.ShiftTypeEnum.getName(workRecord.getShiftType()));
        }
        return watcherList;
    }

    @Override
    public List<WorkRecord> getByMonthAndWatcher(String watcher, Date dutyDate) {
        DateTime month = new DateTime(dutyDate);
        WorkRecord workRecord = new WorkRecord();
        workRecord.setIsDelete(false);
        workRecord.setWatcher(watcher);
        workRecord.setDutyDate(month.withDayOfMonth(1).withTime(0,0,0,0).toDate());
        int end = month.dayOfMonth().getMaximumValue();
        Date dutyDateEnd = month.withDayOfMonth(end).withTime(23,59,59,0).toDate();
        WorkRecordExample example = getSearchExampleByModel(workRecord, dutyDateEnd);
        List<WorkRecord> workRecordList = workRecordMapper.selectByExample(example);
        return workRecordList;
    }

    @Override
    public Map<String, Object> verifyShiftRecord(Date dutyDate, int shiftType, String userName) {
        //验证值班班次
        ShiftRecord shiftRecord = shiftRecordService.getByDutyDateAndShiftType(dutyDate, shiftType);
        if (shiftRecord == null) {
            shiftRecord = new ShiftRecord();
            shiftRecord.setDutyDate(dutyDate);
            shiftRecord.setShiftType(shiftType);
            shiftRecord.setAddManName(userName);
            Map<String, Object> map = shiftRecordService.add(shiftRecord);
            if (MyResponse.isErrorByRootMap(map)) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return map;
            }
        }
        return MyResponse.getSuccessResponse(shiftRecord);
    }

    @Override
    public Map<String, Object> shiftByShiftRecordIds(List<Integer> shiftRecordIdList) {
        WorkRecordExample example = new WorkRecordExample();
        WorkRecordExample.Criteria criteria = example.createCriteria().andShiftRecordIdIn(shiftRecordIdList)
                .andIsShiftEqualTo(false)
                .andIsDeleteEqualTo(false);
        WorkRecord workRecord = new WorkRecord();
        workRecord.setIsShift(true);
        int count = workRecordMapper.updateByExampleSelective(workRecord, example);
        return MyResponse.getResponse(count);
    }
}
