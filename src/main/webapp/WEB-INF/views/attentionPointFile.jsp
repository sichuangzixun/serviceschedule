<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-content">
            <div class="m-b-xs">
                    <button class="btn btn-success btn-sm" onclick="openWindow('编辑通知','web/attentionPoint/update?attentionPointId=${attentionPoint.attentionPointId}')">
                        <i class="fa fa-pencil"> 编辑</i>
                    </button>
                    <button onclick="remove(${attentionPoint.attentionPointId})" class="btn btn-danger btn-sm">
                        <i class="glyphicon glyphicon-trash"></i> 删除
                    </button>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <dl class="dl-horizontal">
                        <dt>通知内容：</dt>
                        <dd>
                            ${attentionPoint.pointContent}
                        </dd>
                        <dt>类型：</dt>
                        <dd>${PointTypeEnum[attentionPoint.pointType]}</dd>
                        <dt>标签：</dt>
                        <dd>${attentionPoint.itemTypeNames}</dd>
                    </dl>
                </div>
                <div class="col-sm-6">
                    <dl class="dl-horizontal">
                        <dt>开始日期：</dt>
                        <dd>
                            <fmt:formatDate value="${attentionPoint.beginDate}" pattern="${DateFormat}"/>
                        </dd>
                        <dt>终止日期：</dt>
                        <dd>
                            <fmt:formatDate value="${attentionPoint.endDate}" pattern="${DateFormat}"/>
                        </dd>
                        <dt>终止理由：</dt>
                        <dd><strong class="text-danger">${attentionPoint.endInformation}</strong></dd>
                    </dl>
                </div>
            </div>
            <div class="mail-box-header">
                <div class="mail-tools tooltip-demo">
                    <button onclick="reload()" class="btn btn-white btn-sm" data-toggle="tooltip" title="刷新">
                        <i class="glyphicon glyphicon-refresh"></i>
                    </button>
                    <button id="btn_add" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> 添加
                    </button>
                    <button id="btn_remove" class="btn btn-danger btn-sm" data-toggle="tooltip" title="移除附件">
                        <i class="glyphicon glyphicon-trash"></i> 删除
                    </button>
                </div>
            </div>
            <div class="mail-box">
                <table class="table table-hover table-mail">
                    <tbody>
                    <tr class="unread">
                        <td class="check-mail">
                            <input type="checkbox" class="i-checks" id="select_all" value="1">
                        </td>
                        <td class="">名称</td>
                        <td class="">上传人</td>
                        <td>上传时间</td>
                        <td class=""></td>
                    </tr>
                    <c:forEach var="file" items="${fileList}">
                        <tr class="">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks" name="fileId[]" value="${file.fileId }">
                            </td>
                            <td class="project-title">${file.fileName}</td>
                            <td>${file.addManName}</td>
                            <td>
                                <fmt:formatDate value="${file.addTime}" pattern="yyyy-MM-dd HH:mm" />
                            </td>
                            <td class="project-actions">
                                <a href="download?filename=${file.fileUrl}" download="${file.fileName}" class="btn btn-white btn-sm">
                                    <i class="glyphicon glyphicon-save"></i>
                                    下载
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="hr-line-dashed"></div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()"> <i
            class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>

<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/iCheck/icheck.min.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script>

    $(document).ready(function() {
        $(".i-checks").iCheck({
            checkboxClass : "icheckbox_square-green",
            radioClass : "iradio_square-green",
        })
    });
</script>
<script type="text/javascript">

    $(function() {

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    function getIdSelections() {
        var spCodesTemp = "";
        $("input:checkbox[name='fileId[]']:checked").each(
            function(i) {
                if (0 == i) {
                    spCodesTemp = $(this).val();
                } else {
                    spCodesTemp += ("," + $(this).val());
                }
            });
        return spCodesTemp;
    }

    function remove(ids) {
        go(ids, url + "api/attentionPoint/remove?attentionPointIds=", "确认要移除选定的项目吗？");
    }

    var ButtonInit = function() {
        var oInit = new Object();
        var postdata = {};
        var url = "api/workItemFile/";

        oInit.Init = function() {
            //初始化页面上面的按钮事件
            $('#btn_add').click(function() {
                openWindow("上传附件","web/workItemFile/add?fileType=${fileType}&recordId=${attentionPoint.attentionPointId}");
            });
            $('#btn_remove').click(function() {
                var ids = getIdSelections();
                go(ids, url + "remove?fileIds=", "确认要移除选定的项目吗？", reload);
            });
            $('#select_all').on('ifChecked', function(event) {
                $("input").iCheck('check');
            });
            $('#select_all').on('ifUnchecked', function(event) {
                $("input").iCheck('uncheck');
            });

        };

        return oInit;
    };


</script>
<style>
    .tab-pane {
        font-size: 14px;
    }
</style>
</body>
</html>
