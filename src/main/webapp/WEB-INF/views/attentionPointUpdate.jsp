<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <ol class="breadcrumb">
                        <li><a href="web/attentionPoint"> 通知</a></li>
                        <li>
                            <strong>编辑通知</strong>
                        </li>
                    </ol>
                    <form:form modelAttribute="attentionPoint" class="form-horizontal m-t" id="typeForm" action="">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-danger">*通知：</label>
                            <div class="col-sm-9">
                                <form:textarea path="pointContent" class="form-control"
                                               style="height: 150px;"></form:textarea>
                                <span class="help-block m-b-none">通知不能为空，字数1000以内</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">类型：</label>
                            <div class="col-sm-4">
                                <form:select path="pointType" class="form-control">
                                    <c:forEach items="${PointTypeEnum}" var="type">
                                        <form:option value="${type.key}">${type.value}</form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">标签：</label>
                            <div class="col-sm-4">
                                <form:select path="itemTypeId" class="form-control">
                                    <c:forEach items="${itemTypeList}" var="type">
                                        <form:option value="${type.typeId}">${type.typeName}</form:option>
                                    </c:forEach>
                                </form:select>
                                <form:hidden path="itemTypeNames"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">开始日期：</label>
                            <div class="col-sm-4">
                                <input id="beginDate" class="form-control layer-date"
                                       value="<fmt:formatDate value="${attentionPoint.beginDate}" pattern="${DateFormat}"/>"/>
                                <input type="hidden" name="beginDate" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">结束日期：</label>
                            <div class="col-sm-4">
                                <input id="endDate" class="form-control layer-date"
                                       value="<fmt:formatDate value="${attentionPoint.endDate}" pattern="${DateFormat}"/>"/>
                                <input type="hidden" name="endDate" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-9">
                                <button class="btn btn-primary" type="button" id="submit">提交</button>
                                <form:hidden path="attentionPointId"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/laydate/laydate.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#submit').click(function () {
            if (valid_form()) {
                var url = "api/attentionPoint/update";
                $('#submit').attr("disabled", "disabled");
                var vPost = $('#typeForm').serialize();
                post(url, vPost, closeWindow);
            }

        });

        //验证表单
        function valid_form() {
            if ($('textarea[name="pointContent"]').val() == '') {
                alert("通知必须填写！");
                return false;
            }
            if ($('select[name="itemTypeId"]').val() == '') {
                alert("标签必须选择！");
                return false;
            }

            var text = $('#itemTypeId').find("option:selected").text();
            $('input[name="itemTypeNames"]').val(text);

            var beginDate = $('#beginDate').val();
            if (beginDate != '') {
                $("input[name='beginDate']").val(new Date(beginDate));
                $("input[name='beginDate']").removeAttr("disabled");
            } else {
                alert("开始日期必须填写！");
                return false;
            }
            var endDate = $('#endDate').val();
            if (endDate != '') {
                $("input[name='endDate']").val(new Date(endDate));
                $("input[name='endDate']").removeAttr("disabled");
            } else {
                alert("结束日期必须填写！");
                return false;
            }
            return true;
        }

        //日期
        laydate.render({
            elem: '#beginDate'
        });

        //日期
        laydate.render({
            elem: '#endDate'
        });

    });

</script>
</body>
</html>
