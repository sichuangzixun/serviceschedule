<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2>用户管理</h2>
        <ol class="breadcrumb">
            <li>个人资料</li>
            <li><strong>修改密码</strong></li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <!-- <a href="#" onclick="JavaScript:history.back(-1);" class="btn btn-primary">返回</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>修改密码</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal m-t" id="userForm">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">用户名：</label>
                            <div class="col-sm-9">
                                <input name="loginName" type="text" readonly="readonly" value="${loginName }"
                                       class="form-control" placeholder=""/>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">旧密码：</label>
                            <div class="col-sm-9">
                                <input type="password" id="oldPwd" class="form-control" placeholder="">
                                <input type="hidden" name="oldPassword"/>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">新密码：</label>
                            <div class="col-sm-9">
                                <input type="password" id="pwd" class="form-control" placeholder="">
                                <input type="hidden" name="newPassword"/>
                                <span class="help-block m-b-none">
										<i class="fa fa-info-circle"></i>
										密码长度为5-20个字符
									</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">确认新密码：</label>
                            <div class="col-sm-9">
                                <input type="password" id="pwd2" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-9 text-left">
                                <button class="btn btn-primary" type="button" id="submit">提交</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/md5.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var successCode = <%=MyResponse.getSuccessCode()%>;
        var needLoginCode = <%=MyResponse.getNeedLoginCode()%>;
        var basePath = "<%= BaseController.getBasePath(request) %>";
        $('#submit').click(function () {

            var vPwd = $('#pwd').val();
            var vPwd2 = $('#pwd2').val();
            var oldPwd = $('#oldPwd').val();
            if (oldPwd == '') {
                alert("请输入旧密码！");
                return false;
            }
            if (vPwd == '') {
                alert("请输入新密码！");
                return false;
            }
            if (vPwd2 == '') {
                alert("请再次输入新密码！");
                return false;
            }
            if (vPwd.length < 5 || vPwd.length > 20) {
                alert("密码长度为5-20个字符！");
                return false;
            }

            $('#userForm input[name="newPassword"]').val(hex_md5(vPwd));

            $('#userForm input[name="oldPassword"]').val(hex_md5(oldPwd));
            var url = "api/user/changePassword";
            var vPost = $('#userForm').serialize();
            post(url, vPost, logout);
        });

        function page_top() {
            $("html,body").animate({scrollTop: 0}, 500);
        }
    });

</script>
<style>
    form input, select {
        min-width: 200px;
        max-width: 350px;
    }
</style>
</body>
</html>
