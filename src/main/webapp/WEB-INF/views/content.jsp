<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2018/7/16
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-8">
                    <h3>
                        交班班组内容
                        <span class="label label-primary">${ShiftStatusEnum[previousShiftRecord.shiftRecord.shiftStatus]}</span>
                        <%--<button class="btn btn-warning btn-xs"--%>
                                <%--onclick="openWindow('查看交接记录', 'web/shiftRecord/info?shiftRecordId=${previousShiftRecord.shiftRecord.shiftRecordId}', false)">--%>
                            <%--导出--%>
                        <%--</button>--%>
                        <button onclick="reload()" class="btn btn-white btn-sm">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </button>
                    </h3>
                    <table class="table table-bordered m-b-xs">
                        <tr>
                            <td>值班日期：
                                <strong class="text-danger">
                                    <fmt:formatDate value="${previousShiftRecord.shiftRecord.dutyDate}"
                                                    pattern="${DateFormat}"/>
                                </strong>
                            </td>
                            <td>交班班次：
                                <strong class="text-danger">
                                    ${ShiftTypeEnum[previousShiftRecord.shiftRecord.shiftType]}
                                </strong>
                            </td>
                            <td>现场负责人：
                                <strong class="text-danger">
                                    ${previousShiftRecord.shiftRecord.dutyOfficer}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">交班班员：
                                <span class="text-danger">${previousShiftRecord.shiftRecord.workers}</span></td>
                        </tr>
                    </table>
                    <div style="height: 300px;" class="m-b border-bottom">
                        <div class="full-height-scroll" style="width: auto; height: 100%;">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover m-b-none">
                                    <tr>
                                        <th width="80px">标签</th>
                                        <th width="60%">交接内容</th>
                                        <th>完成时间</th>
                                        <th>完成人员</th>
                                        <th>操作</th>
                                    </tr>
                                    <tbody>
                                    <c:forEach items="${previousShiftRecord.workItemList}" var="workItem">
                                        <tr <c:if test="${workItem.isComplete}">class="bg-success" </c:if>>
                                            <td><span class="label label-primary">${workItem.itemTypeNames}</span></td>
                                            <td>${workItem.workContent}<br>
                                                <strong class="text-danger re-br">${workItem.handleInformation}</strong>
                                            </td>
                                            <td>
                                                <fmt:formatDate value="${workItem.endTime}"
                                                                pattern="${TimeFormat}"/>
                                            </td>
                                            <td>${workItem.endHandlerName}</td>
                                            <td>
                                                <button class="btn btn-success btn-xs"
                                                        onclick="handle(${workItem.workItemId})">
                                                    <i class="fa fa-pencil"> 处理</i>
                                                </button>
                                                <button class="btn btn-primary btn-xs"
                                                        onclick="complete(${workItem.workItemId})">
                                                    <i class="fa fa-check"> 完成</i>
                                                </button>
                                                <button class="btn btn-white btn-xs"
                                                        onclick="upload(${workItem.workItemId})">
                                                    <i class="fa fa-cloud-upload"> 附件</i>
                                                </button>
                                            </td>
                                        </tr>
                                    </c:forEach>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <h3>
                        本班班组内容
                        <span class="label label-primary">${ShiftStatusEnum[currentShiftRecord.shiftRecord.shiftStatus]}</span>
                        <c:choose>
                            <c:when test="${currentShiftRecord == null}">
                                <button class="btn btn-primary btn-xs"
                                        onclick="openWindow('添加班组', 'web/shiftRecord/add')">
                                    <i class="fa fa-plus"> 添加班组</i>
                                </button>
                            </c:when>
                            <c:otherwise>
                                <button class="btn btn-success btn-xs"
                                        onclick="update(${currentShiftRecord.shiftRecord.shiftRecordId})">
                                    <i class="fa fa-pencil"> 编辑</i>
                                </button>
                                <button class="btn btn-danger btn-xs" onclick="endShift()">
                                    <i class="fa fa-check"> 提交</i>
                                </button>
                                <button class="btn btn-warning btn-xs"
                                        onclick="openWindow('查看交接记录', 'web/shiftRecord/info?shiftRecordId=${currentShiftRecord.shiftRecord.shiftRecordId}', false)">
                                    导出
                                </button>
                            </c:otherwise>
                        </c:choose>
                    </h3>
                    <table class="table table-bordered m-b-xs">
                        <tr>
                            <td>值班日期：<strong class="text-danger">
                                <fmt:formatDate value="${currentShiftRecord.shiftRecord.dutyDate}"
                                                pattern="${DateFormat}"/>
                            </strong></td>
                            <td>本班班次：
                                <strong class="text-danger">
                                    ${ShiftTypeEnum[currentShiftRecord.shiftRecord.shiftType]}
                                </strong>
                            </td>
                            <td>现场负责人：
                                <strong class="text-danger">${currentShiftRecord.shiftRecord.dutyOfficer}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">本班班员：
                                <span class="text-danger">${currentShiftRecord.shiftRecord.workers}</span></td>
                        </tr>
                    </table>
                    <div class="m-b-xs">
                        <button class="btn btn-primary btn-xs"
                                onclick="openWindow('添加工作事项', 'web/workItem/add?addShiftRecordId=${currentShiftRecord.shiftRecord.shiftRecordId}')">
                            <i class="fa fa-plus"> 添加工作</i>
                        </button>
                    </div>
                    <div style="height: 300px;" class="border-bottom">
                        <div class="full-height-scroll" style="width: auto; height: 100%;">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover m-b-none">
                                    <tr>
                                        <th width="80px">标签</th>
                                        <th width="60%">交接内容</th>
                                        <th>完成时间</th>
                                        <th>完成人员</th>
                                        <th>操作</th>
                                    </tr>
                                    <tbody>
                                    <c:forEach items="${currentShiftRecord.workItemList}" var="workItem">
                                        <tr <c:if test="${workItem.isComplete}">class="bg-success" </c:if>>
                                            <td><span class="label label-primary">${workItem.itemTypeNames}</span></td>
                                            <td>
                                                    ${workItem.workContent}<br>
                                                <strong class="text-danger re-br">${workItem.handleInformation}</strong>
                                            </td>
                                            <td>
                                                <fmt:formatDate value="${workItem.endTime}"
                                                                pattern="${TimeFormat}"/>
                                            </td>
                                            <td>
                                                    ${workItem.endHandlerName}
                                            </td>
                                            <td>
                                                <button class="btn btn-success btn-xs"
                                                        onclick="edit(${workItem.workItemId})">
                                                    <i class="fa fa-pencil"> 编辑</i>
                                                </button>
                                                <button class="btn btn-white btn-xs"
                                                        onclick="upload(${workItem.workItemId})">
                                                    <i class="fa fa-cloud-upload"> 附件</i>
                                                </button>
                                                <button onclick="remove(${workItem.workItemId})" class="btn btn-danger btn-xs">
                                                    <i class="glyphicon glyphicon-trash"></i> 删除
                                                </button>
                                            </td>

                                        </tr>
                                    </c:forEach>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <blockquote class="" style="font-size:14px;height: 800px;">
                        <h3>通知
                            <button id="btn_add_point" class="btn btn-primary btn-xs">
                                <i class="glyphicon glyphicon-plus"></i> 添加通知
                            </button>
                        </h3>
                        <div class="full-height-scroll" style="width: auto; height: 100%;">
                            <ol class="list-group clear-list text-warning">
                                <c:forEach items="${attentionPointList}" var="attentionPoint">
                                    <li class="list-group-item">
                                        <span class="label label-success">${PointTypeEnum[attentionPoint.pointType]}</span>
                                        <span class="label label-primary">${attentionPoint.itemTypeNames}</span>
                                            ${attentionPoint.pointContent}
                                        <div class="agile-detail">
                                            <button class="pull-right btn btn-primary btn-xs m-l-xs"
                                                    onclick="openWindow('查看通知','web/attentionPoint/info?attentionPointId=${attentionPoint.attentionPointId}',false)">
                                                查看
                                            </button>
                                            <button type="button"
                                                    onclick="openWindow('终止宣贯','web/attentionPoint/end?attentionPointId=${attentionPoint.attentionPointId}')"
                                                    class="pull-right btn btn-xs btn-danger">结束宣贯
                                            </button>
                                            <small class="text-muted"><fmt:formatDate value="${attentionPoint.endDate}"
                                                                                      pattern="${TimeFormat}"/></small>
                                        </div>

                                    </li>
                                </c:forEach>
                            </ol>
                        </div>
                    </blockquote>


                </div>

            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">


    $(function () {
        $(".full-height-scroll").slimScroll({height: "100%"})
    });

    function update(shiftRecordId) {
        var href = "web/shiftRecord/update?shiftRecordId=" + shiftRecordId;
        openWindow("修改交接记录", href);
    }
    function endShift() {
        go("${currentShiftRecord.shiftRecord.shiftRecordId}", "api/shiftRecord/updateShiftStatus?shiftRecordIds=", "确定提交该项目？", reload);
    }

    $('#btn_add_point').click(function () {
        var href = "web/attentionPoint/add?addShiftRecordId=${currentShiftRecord.shiftRecord.shiftRecordId}";
        openWindow("添加通知", href);
    });


    function remove(workItemId) {
        go(workItemId, "api/workItem/remove?workItemIds=", "确认要移除选定的项目吗？", reload);
    }

    function handle(workItemId) {
        var href = "web/workItem/handle?workItemId=" + workItemId + "&shiftRecordId=${currentShiftRecord.shiftRecord.shiftRecordId}";
        openWindow("处理工作", href);
    }

    function complete(workItemId) {
        var href = "web/workItem/complete?workItemId=" + workItemId + "&shiftRecordId=${currentShiftRecord.shiftRecord.shiftRecordId}";
        openWindow("标记已完成", href);
    }

    function edit(workItemId) {
        var href = "web/workItem/update?workItemId=" + workItemId;
        openWindow("修改工作内容", href);
    }

    function upload(workItemId) {
        var href = "web/workItemFile/add?fileType=1&recordId=" + workItemId;
        openWindow("上传附件", href);
    }
</script>

</body>
</html>
