<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <title>服务调度交接记录工具</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html"/>
    <![endif]-->
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow: hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close">
            <i class="fa fa-times-circle"></i>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
							<span>
								<img alt="image" class="img-circle" src="" onerror="this.style.display='none'"/>
							</span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="clear">
									<span class="block m-t-xs">
										<strong class="font-bold">${user.userName }</strong>
										<b class="caret"></b>
									</span>
									<span class="text-muted text-xs block"></span>
								</span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a class="J_menuItem" href="web/user/changePassword">修改密码</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="web/user/logout">安全退出</a>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element"></div>
                </li>
                <li>
                    <a href="web/user/content" class="J_menuItem" data-index="0">
                        <i class="glyphicon glyphicon-home"></i>
                        <span class="nav-label">首页</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-table"></i>
                        <span class="nav-label">历史记录</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="web/shiftRecord" class="J_menuItem">查询班次</a>
                        </li>
                        <li>
                            <a href="web/workItem" class="J_menuItem">查询内容</a>
                        </li>
                        <li>
                            <a href="web/attentionPoint" class="J_menuItem">查询通知</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" >
                        <i class="fa fa-table"></i>
                        <span class="nav-label">排班记录</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="web/workRecord">排班表</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href="web/workRecord/list">值班记录</a>
                        </li>
                    </ul>
                </li>
                <c:if test="${user.userType == 1}">
                    <li>
                        <a href="#">
                            <i class="fa fa-cog"></i>
                            <span class="nav-label">系统设置</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="web/itemType?dataType=2">管理值班员</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="web/itemType?dataType=1">管理工作标签</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="web/user/list" class="J_menuItem" >
                            <i class="fa fa-users"></i>
                            <span class="nav-label">用户管理</span>
                        </a>
                    </li>
                </c:if>

            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row content-tabs">
            <button class="roll-nav roll-left J_tabLeft">
                <i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs J_menuTabs">
                <div class="page-tabs-content">
                    <a href="javascript:;" class="active J_menuTab" data-id="web/user/content">首页</a>
                </div>
            </nav>
            <button class="roll-nav roll-right J_tabRight">
                <i class="fa fa-forward"></i>
            </button>
            <div class="dropdown btn-group roll-nav roll-right ">
                <button class="dropdown-toggle J_tabClose" data-toggle="dropdown">
                    关闭操作
                    <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu dropdown-menu-right">
                    <li class="J_tabShowActive">
                        <a>定位当前选项卡</a>
                    </li>
                    <li class="divider"></li>
                    <li class="J_tabCloseAll">
                        <a>关闭全部选项卡</a>
                    </li>
                    <li class="J_tabCloseOther">
                        <a>关闭其他选项卡</a>
                    </li>
                </ul>
            </div>
            <a href="web/user/logout" class="roll-nav roll-right J_tabExit">
                <i class="fa fa fa-sign-out"></i>
                退出
            </a>
        </div>
        <div class="row J_mainContent" id="content-main">
            <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="web/user/content" frameborder="0"
                    data-id="web/user/content" seamless></iframe>
        </div>
        <div class="footer">
            <div class="pull-right">
                &copy; 2018
                <a href="#" target="_blank">思创智汇（广州）科技有限公司</a>
            </div>
        </div>
    </div>
    <!--右侧部分结束-->
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="hplus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="hplus/js/hplus.min.js?v=4.1.0"></script>
<script type="text/javascript" src="hplus/js/contabs.min.js"></script>
<script src="hplus/js/plugins/pace/pace.min.js"></script>
<script src="js/function.js"></script>
<script>

    //打开新窗口
    function createNewTab(id, href, text) {
        var m = 0,
            l = $.trim(text),
            k = true;
        if (id == undefined || $.trim(id).length == 0) {
            return false;
        }
        $(".J_menuTab").each(function () {
            if ($(this).data("id") == id) {
                if (!$(this).hasClass("active")) {
                    $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
                    // g(this);
                }
                $(".J_mainContent .J_iframe").each(function () {
                    if ($(this).data("id") == id) {
                        $(this).show().siblings(".J_iframe").hide();
                        $(this).attr('src', href);
                        return false;
                    }
                });
                k = false;
                return false;
            }
        });
        if (k) {
            var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + id + '">' + l + ' <i class="fa fa-times-circle"></i></a>';
            $(".J_menuTab").removeClass("active");
            var n = '<iframe class="J_iframe" name="iframe' + m + '" width="100%" height="100%" src="' + href + '" frameborder="0" data-id="' + id + '" seamless></iframe>';
            $(".J_mainContent").find("iframe.J_iframe").hide().parents(".J_mainContent").append(n);
            $(".J_menuTabs .page-tabs-content").append(p);
            // g($(".J_menuTab.active"));
        }
        return false;
    }

    $(function () {

        $('.dropdown').mouseout(function () {
            window.setTimeout(function () {
                $('.dropdown').removeClass('open');
            }, 10000);
        });

    });

</script>
</body>
</html>
