<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-table.css"
          rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-editable.css">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="ibox">
    <div class="ibox-content">
        <div class="alert alert-warning alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close"
                    type="button">×</button>
            批量添加多个内容用空格分隔！
        </div>
        <form:form modelAttribute="itemType" class="form-inline" id="typeForm" action="">
            <div class="form-group">
                <label class="control-label text-danger">*内容：</label>
                    <form:input path="typeName" class="form-control" placeholder="请输入文本" cssStyle="width: 300px"/>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="button" id="submit">添加</button>
                <form:hidden path="dataType"/>
            </div>
        </form:form>
        <div id="toolbar" class="tooltip-demo">
            <button onclick="reload()" class="btn btn-white btn-sm">
                <i class="glyphicon glyphicon-refresh"></i>
            </button>
            <button id="btn_remove" class="btn btn-danger btn-sm">
                <i class="glyphicon glyphicon-trash"></i> 删除
            </button>
        </div>
        <table id="table"></table>
    </div>
</div>

<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script
        src="hplus/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<script
        src="hplus/js/plugins/bootstrap-table/bootstrap-table-export.js"></script>
<script src="hplus/js/plugins/bootstrap-table/tableExport.js"></script>
<script src="hplus/js/plugins/bootstrap-table/analytics.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
<![endif]-->
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">
    $(function () {
        //1.初始化Table
        var oTable = new TableInit();
        oTable.Init();

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    var TableInit = function () {
        var oTableInit = new Object();
        var cols = [{
            checkbox: true,
        },{
            field: 'typeName',
            title: '内容',
            align: 'center'
        }, {
            field: 'addTime',
            title: '添加时间',
            align: 'center',
            formatter: dateTimeFormatter
        }];
        //初始化Table
        oTableInit.Init = function () {
            $('#table').bootstrapTable({
                url: 'api/itemType/get?dataType=${itemType.dataType}', //请求后台的URL（*）
                method: 'get', //请求方式（*）
                toolbar: '#toolbar', //工具按钮用哪个容器
                striped: true, //是否显示行间隔色
                cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true, //是否显示分页（*）
                sortable: true, //是否启用排序
                sortOrder: "asc", //排序方式
                queryParams: oTableInit.queryParams,//传递参数（*）
                sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
                pageNumber: 1, //初始化加载第一页，默认第一页
                pageSize: 50, //每页的记录行数（*）
                pageList: [50, 200, 500, 1000], //可供选择的每页的行数（*）
                escape: true,
                dataField: 'data',
                search: false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: false,
                showColumns: true, //是否显示所有的列
                //showRefresh : true, //是否显示刷新按钮
                minimumCountColumns: 2, //最少允许的列数
                clickToSelect: true, //是否启用点击选中行
                height: 600, //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: "ID", //每一行的唯一标识，一般为主键列
                responseHandler: oTableInit.returnParams,//返回参数（*）
                //showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
                cardView: false, //是否显示详细视图
                detailView: false, //是否显示父子表
                showExport: true,
                exportTypes: ['excel', 'csv', 'txt'],
                exportOptions: {
                    fileName: '工作内容' + DateFormat(new Date(), 'yyyy-MM-dd'),
                    ignoreColumn: [0, -1],
                },
                columns: cols,
            });
        };
        //得到查询的参数
        oTableInit.queryParams = function (params) {
            var temp = 'length=' + params.limit + '&begin=' + params.offset;
            return temp;
        };
        //得到返回的参数
        oTableInit.returnParams = function (res) {
            return responsePageData(res);
        };


        return oTableInit;
    };

    function getIdSelections() {
        return $.map($('#table').bootstrapTable('getSelections'), function (row) {
            return row.typeId
        });
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};
        var url = "api/itemType/";

        oInit.Init = function () {

            //初始化页面上面的按钮事件

            $('#btn_remove').click(function () {
                var ids = getIdSelections();
                go(ids, url + "remove?typeIds=", "确认要移除选定的项目吗？");

            });

            $('#submit').click(function () {
                if (valid_form()) {
                    var url = "api/itemType/add";
                    var vPost = $('#typeForm').serialize();
                    post(url, vPost, refreshTable);
                }
            });

            //验证表单
            function valid_form() {
                if ($('input[name="typeName"]').val() == '') {
                    alert("内容必须填写！");
                    return false;
                }
                return true;
            }
        };

        return oInit;
    };
</script>
</body>
</html>
