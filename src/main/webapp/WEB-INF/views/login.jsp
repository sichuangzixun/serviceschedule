<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name"></h1>
        </div>
        <h3>欢迎使用服务调度交接记录工具</h3>
        <p class="text-danger text-center">${msg}</p>
        <form class="m-t" role="form" id="userForm">
            <div class="form-group">
                <input type="text" class="form-control" name="loginName" placeholder="用户名" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" placeholder="密码" required="">
                <input type="hidden" value="" name="password">
            </div>
            <button type="button" id="login" class="btn btn-primary block full-width m-b">登 录</button>
        </form>
    </div>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/md5.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">
    $(function () {
        $('#login').click(function (e) {
            var vUser = $('#userForm input[name="loginName"]').val();
            var vPwd = $('#password').val();
            if (vUser == '') {
                alert('请填写用户名');
                return false;
            }
            if (vPwd == '') {
                alert('请填写用户密码');
                return false;
            }
            var str = hex_md5(vPwd);
            $('#userForm input[name="password"]').val(str);
            var vPost = $('#userForm').serialize();
            var url = "api/user/login";
            post(url, vPost, go_index);
        });

    });


</script>
</body>
</html>