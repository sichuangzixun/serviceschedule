<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>服务调度交接记录工具</title>
<link href="/schedule/hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
<link href="/schedule/hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<link href="/schedule/hplus/css/animate.min.css" rel="stylesheet">
<link href="/schedule/hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">

	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>系统提示</h5>
					</div>
					<div class="ibox-content text-center">
						<div class="alert alert-danger">${msg }</div>
						<br />
						<a href="#" onclick="JavaScript:history.back(-1);" class="btn btn-primary m-t">返回</a>
						<a href="/schedule" class="btn btn-primary m-t" target="_top">主页</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- jQuery -->
	<script src="/schedule/hplus/js/jquery.min.js?v=2.1.4"></script>
	<script src="/schedule/hplus/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/schedule/hplus/js/content.min.js?v=1.0.0"></script>
</body>
</html>