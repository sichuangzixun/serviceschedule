<%--
  Created by IntelliJ IDEA.
  User: acer
  Date: 2018/7/16
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content">
    <div class="ibox">
        <div class="ibox-content">
            <div class="m-b-xs">
                <button class="btn btn-success btn-sm"
                        onclick="openWindow('编辑交接班次','web/shiftRecord/update?shiftRecordId=${shiftRecord.shiftRecordId}')">
                    <i class="fa fa-pencil"></i> 编辑
                </button>
                <button id="btn_export" class="btn btn-warning btn-sm">
                    导出
                </button>
                <button id="btn_print" class="btn btn-primary btn-sm">
                   打印
                </button>
            </div>
            <div id="print_area">
            <table class="table " id="table" style="width: 210mm;" border="1px solid">
                <tr>
                    <td colspan="7">
                        <strong id="title"><fmt:formatDate value="${shiftRecord.dutyDate}" pattern="yyyy年M月d日"/>${ShiftTypeEnum[shiftRecord.shiftType]}交班记录</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25mm;"><strong>交接时间</strong></td>
                    <td colspan="2">
                        <span class=""><fmt:formatDate value="${shiftRecord.shiftTime}" pattern="${TimeFormat}"/></span>
                    </td>
                    <td style="width: 30mm;"><strong>交班班次</strong></td>
                    <td style="width: 30mm;">
                        ${ShiftTypeEnum[shiftRecord.shiftType]}
                    </td>
                    <td style="width: 30mm;"><strong>现场负责人</strong></td>
                    <td style="width: 30mm;">
                        ${shiftRecord.dutyOfficer}
                    </td>
                </tr>
                <tr>
                    <td><strong>交班班员</strong></td>
                    <td colspan="6" class="text-left">
                        <span>${shiftRecord.workers}</span></td>
                </tr>
                <c:set var="longSize" value="${longAttentionPoints.size() > 0?longAttentionPoints.size():1}"/>
                <c:set var="shortSize" value="${shortAttentionPoints.size() > 0?shortAttentionPoints.size():1}"/>
                <tr>
                    <td rowspan="${longSize+shortSize}">
                        <strong>通知</strong>
                    </td>
                    <td width="60" <c:if test="${longAttentionPoints.size() > 1}">rowspan
                            ="${longAttentionPoints.size()}"
                    </c:if>>
                        <strong>长期</strong>
                    </td>
                    <td colspan="5" class="text-left">
                        <c:if test="${longAttentionPoints.size() > 0}">
                            1.<strong>${longAttentionPoints[0].itemTypeNames}</strong> ${longAttentionPoints[0].pointContent}
                        </c:if>
                    </td>
                </tr>
                <c:forEach items="${longAttentionPoints}" var="item" varStatus="status">
                    <c:if test="${status.index > 0}">
                        <tr>
                            <td colspan="5" class="text-left">
                                    ${status.index + 1}. <strong>${item.itemTypeNames}</strong> ${item.pointContent}
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
                <tr>
                    <td <c:if test="${shortAttentionPoints.size() > 1}">rowspan
                    ="${shortAttentionPoints.size()}"
                    </c:if>><strong>短期</strong></td>
                    <td colspan="5" class="text-left">
                        <c:if test="${shortAttentionPoints.size() > 0}">
                            1.<strong>${shortAttentionPoints[0].itemTypeNames}</strong> ${shortAttentionPoints[0].pointContent}
                        </c:if>
                    </td>
                </tr>
                <c:forEach items="${shortAttentionPoints}" var="item" varStatus="status">
                    <c:if test="${status.index > 0}">
                        <tr>
                            <td colspan="5" class="text-left">
                                    ${status.index + 1}. <strong>${item.itemTypeNames}</strong> ${item.pointContent}
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
                <tr>
                    <td <c:if test="${workItemList.size() > 1}">rowspan="${workItemList.size()}"
                    </c:if><strong>交班内容</strong></td>
                    <td colspan="6" class="text-left">
                        <c:if test="${workItemList.size() > 0}">
                            1. <strong>${workItemList[0].itemTypeNames}</strong> ${workItemList[0].workContent}<br>
                            <c:if test="${workItemList[0].handleInformation.length() > 0}">
                                <strong class="text-danger"><span
                                        class="re-br">${workItemList[0].handleInformation}</span></strong><br>
                            </c:if>
                        </c:if>
                    </td>
                </tr>
                <c:forEach items="${workItemList}" var="item" varStatus="status">
                    <c:if test="${status.index > 0}">
                        <tr>
                            <td colspan="6" class="text-left">
                           <span class="top-border">
                            ${status.index + 1}. <strong>${item.itemTypeNames}</strong> ${item.workContent}<br>
                            <c:if test="${item.handleInformation.length() > 0}">
                                <strong class="text-danger"><span
                                        class="re-br">${item.handleInformation}</span></strong>
                            </c:if>
                           </span>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </table>
                <style>

                    table tr td{
                        text-align: center;
                        border: 1px solid black;
                        border-top: 1px solid black!important;
                    }
                    table{
                        border-spacing: 0;
                        border-collapse: collapse;
                    }

                    .text-left {
                        text-align: left;
                    }
                    .text-danger{color: red;}
                </style>
            </div>
        </div>
    </div>

</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script src="js/jquery.table2excel.js?v=2.0"></script>
<script src="js/jquery-migrate-1.1.0.js"></script>
<script src="js/jquery.jqprint-0.3.js"></script>

<script type="text/javascript">

    //初始化页面上面的按钮事件

    $('#btn_shift').click(function () {
        go("${shiftRecord.shiftRecordId}", "api/shiftRecord/updateShiftStatus?shiftRecordIds=", "确定提交该项目？", reload);
    });

    $("#btn_export").click(function () {
        if (!confirm("是否导出数据？")) {
            return;
        }
        $('#table').table2excel({
            filename: $('#title').text()
        });
    });

    //打印
    $('#btn_print').click(function () {
        $("#print_area").jqprint();
    });
</script>

</body>
</html>
