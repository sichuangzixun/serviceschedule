<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/plugins/treeview/bootstrap-treeview.css" rel="stylesheet">
    <link href="hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>账号设置</h5>
                </div>
                <div class="ibox-content">
                    <form:form modelAttribute="user" class="form-horizontal m-t" id="userForm" action="">
                        <div class="form-group">
                            <label class="col-sm-3 control-label ">登录账号：</label>
                            <div class="col-sm-3">
                                <form:input path="loginName" type="text" class="form-control"/>
                                <span class="help-block m-b-none text-warning"><i class="fa fa-info-circle"></i> 登录账号不能为空！不能重复！</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label ">密码：</label>
                            <div class="col-sm-3">
                                <input id="password" type="password" class="form-control"/>
                                <input type="hidden" name="password">
                                <span class="help-block m-b-none text-warning"><i class="fa fa-info-circle"></i> 密码长度为5-20个字符!</span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label ">昵称/姓名：</label>
                            <div class="col-sm-3">
                                <form:input path="userName" type="text" class="form-control"/>
                                <span class="help-block m-b-none text-warning"><i class="fa fa-info-circle"></i> 昵称/姓名不能为空！</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-3 text-left">
                                <button class="btn btn-primary" type="button" id="submit">提交</button>
                                <form:input path="userId" type="hidden"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/treeview/bootstrap-treeview.js"></script>
<script src="hplus/js/plugins/iCheck/icheck.min.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/md5.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">
    var needLoginCode = <%=MyResponse.getNeedLoginCode()%>;
    var basePath = "<%= BaseController.getBasePath(request) %>";
    var successCode = <%=MyResponse.getSuccessCode()%>;
    function page_top() {
        $("html,body").animate({scrollTop: 0}, 500);
    }

    $(document).ready(function () {

        if ($('input[name="userId"]').val() != '') {
            $("#loginName").attr("readonly", "true");
        }

        $('#submit').click(function () {

            if ($('input[name="userName"]').val() == '') {
                alert("昵称/姓名必须填写！");
                return false;
            }
            var vPwd = $('#password').val();
            if ($('input[name="userId"]').val() == '') {
                if ($('input[name="loginName"]').val() == '') {
                    alert("登录账号必须填写！");
                    return false;
                }

                if (vPwd == '') {
                    alert("密码必须填写！");
                    return false;
                }

            }
            if(vPwd != ''){
                if (vPwd.length < 5 || vPwd.length > 20) {
                    alert("密码长度为5-20个字符！");
                    return false;
                }
                $('input[name="password"]').val(hex_md5(vPwd));
            }

            var url = "api/user/save";
            var vPost = $('#userForm').serialize();
            post(url, vPost, closeWindow);
        });

    });

</script>
<style>
    form input, textarea, select {
        min-width: 300px;
        max-width: 550px;
    }
</style>
</body>
</html>
