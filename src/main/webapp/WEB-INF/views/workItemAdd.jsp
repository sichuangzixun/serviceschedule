<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>添加工作事项</h5>
                </div>
                <div class="ibox-content">
                    <div class="alert alert-warning">
                        已交接班次只有管理员可以修改信息。
                    </div>
                    <form:form modelAttribute="workItem" class="form-horizontal m-t" id="typeForm" action="">
                        <div class="form-group">
                            <label class="control-label text-danger">*工作内容：</label>
                            <div class="col-sm-9">
                                <form:textarea path="workContent" class="form-control"
                                               style="height: 100px;"></form:textarea>
                                <span class="help-block m-b-none">工作内容不能为空，字数1000以内</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">标签：</label>
                            <div class="col-sm-4">
                                <form:select path="itemTypeId" class="form-control">
                                    <c:forEach items="${itemTypeList}" var="type">
                                        <form:option value="${type.typeId}">${type.typeName}</form:option>
                                    </c:forEach>
                                </form:select>
                                <form:hidden path="itemTypeNames" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">处理情况：</label>
                            <div class="col-sm-9">
                                <form:textarea path="handleInformation" class="form-control"
                                               style="height: 100px;"></form:textarea>
                                <span class="help-block m-b-none">字数1000以内</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">完成时间：</label>
                            <div class="col-sm-3">
                                <input id="endTime" class="form-control layer-date"
                                       value="<fmt:formatDate value="${workItem.endTime}" pattern="${TimeFormat}"/>"/>
                                <input type="hidden" name="endTime" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-9 text-left">
                                <button class="btn btn-primary" type="button" id="submit" >提交</button>
                                <button class="btn btn-white" type="button" onclick="reload()">清空</button>
                                <form:hidden path="addShiftRecordId"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/laydate/laydate.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        //提交
        $('#submit').click(function () {
            if (valid_form()) {
                var url = "api/workItem/add";
                $('#submit').attr("disabled", "disabled");
                var vPost = $('#typeForm').serialize();
                post(url, vPost, closeWindow);
            }

        });

        //验证表单
        function valid_form() {

            if ($('textarea[name="workContent"]').val() == '') {
                alert("工作内容必须填写！");
                return false;
            }
            if ($('select[name="itemTypeId"]').val() == '') {
                alert("标签必须选择！");
                return false;
            }
            var endTime = $('#endTime').val();
            if (endTime != '') {
                $("input[name='endTime']").val(new Date(endTime));
                $("input[name='endTime']").removeAttr("disabled");
            }

            var text = $('#itemTypeId').find("option:selected").text();
            $('input[name="itemTypeNames"]').val(text);
            return true;
        }

        //日期范围
        laydate.render({
            elem: '#endTime'
            , type: 'datetime'
        });
    });

</script>
</body>
</html>
