<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>标记已完成</h5>
                </div>
                <div class="ibox-content">
                    <div class="alert alert-warning">
                        已完成工作不能添加处理情况。
                    </div>
                    <form:form modelAttribute="workItem" class="form-horizontal m-t" id="typeForm"
                               action="" >
                        <div class="form-group">
                            <label class="control-label text-danger">*工作内容：</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">${workItem.workContent}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">标签：</label>
                            <div class="col-sm-4">
                                <p class="form-control-static">${workItem.itemTypeNames}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">完成时间：</label>
                            <div class="col-sm-3">
                                <input id="endTime" class="form-control layer-date"
                                       value="<fmt:formatDate value="${workItem.endTime}" pattern="${TimeFormat}"/>"/>
                                <input type="hidden" name="endTime" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">完成人：</label>
                            <div class="col-sm-3">
                                <select id="endHandlerName" name="endHandlerName" class="form-control">
                                    <c:forEach items="${watcherList }" var="watcher">
                                        <option value="${watcher.typeName }">${watcher.typeName }</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">处理情况：</label>
                            <div class="col-sm-9">
                                <textarea name="handleInformation" class="form-control"
                                          style="height: 100px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-9 text-left">
                                <button class="btn btn-primary" type="button" id="submit">提交</button>
                                <form:hidden path="workItemId"/>
                                <input type="hidden" name="shiftRecordId" value="${shiftRecord.shiftRecordId}"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/laydate/laydate.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#endHandlerName').find("option[value='${workItem.endHandlerName}']").attr("selected","selected");
        //提交
        $('#submit').click(function () {
            if (valid_form()) {
                var url = "api/workItem/end";
                $('#submit').attr("disabled", "disabled");
                var vPost = $('#typeForm').serialize();
                post(url, vPost, closeWindow);
            }

        });

        //验证表单
        function valid_form() {
            var endTime = $('#endTime').val();
            if (endTime != '') {
                $("input[name='endTime']").val(new Date(endTime));
                $("input[name='endTime']").removeAttr("disabled");
            } else {
                alert("完成时间必须填写！");
            }

            if ($('input[name="endHandlerName"]').val() == '') {
                alert("完成人必须填写！");
                return false;
            }

            return true;
        }

        //日期范围
        laydate.render({
            elem: '#endTime'
            , type: 'datetime'
        });
    });

</script>
</body>
</html>
