<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>上传附件</h5>
                </div>
                <div class="ibox-content">
                    <form:form modelAttribute="workItemFile" class="form-horizontal m-t" id="typeForm" action="web/workItemFile/upload" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">上传附件：</label>
                            <div class="col-sm-9">
                                <input name="file" type="file" id="file" class="form-control" multiple/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-9">
                                <button class="btn btn-primary" type="submit" >提交</button>
                                <button class="btn btn-white" type="button" onclick="reload()">清空</button>
                                <form:hidden path="fileType"/>
                                <form:hidden path="recordId"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#typeForm').submit(function (e) {
            if(valid_form() == false){
                e.preventDefault();
            }
        });

        //验证表单
        function valid_form() {
            if ($('input[name="file"]').val() == '') {
                alert("请选择文件！");
                return false;
            }
            return true;
        }

    });

</script>
</body>
</html>
