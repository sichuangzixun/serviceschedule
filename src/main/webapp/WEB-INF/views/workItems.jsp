<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-table.css"
          rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-editable.css">
</head>
<body class="gray-bg" id="page">
<div class="ibox">
    <div class="ibox-content">
        <form id="searchForm" class="form-inline" role="form" action="web/workItem/exportExcel" target="_blank"
              method="post">
            <div class="form-group">
                <label class="control-label">录入日期范围</label>
                <input id="addTime" class="form-control layer-date" value="">
                <input type="hidden" name="addTime">
                <input type="hidden" name="addTimeEnd">
            </div>
            <div class="form-group">
                <label class="control-label">完成日期范围</label>
                <input id="endTime" class="form-control layer-date" value="">
                <input type="hidden" name="endTime">
                <input type="hidden" name="endTimeEnd">
            </div>
            <div class="form-group">
                <label class="control-label">完成状态</label>
                <select name="isComplete" class="form-control">
                    <option value="">全部</option>
                    <option value="false">未完成</option>
                    <option value="true">已完成</option>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">录入班次</label>
                <select name="addShiftType" class="form-control">
                    <option value="">全部</option>
                    <c:forEach items="${ShiftTypeEnum}" var="type">
                        <option value="${type.key}">${type.value}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">完成班次</label>
                <select name="endShiftType" class="form-control">
                    <option value="">全部</option>
                    <c:forEach items="${ShiftTypeEnum}" var="type">
                        <option value="${type.key}">${type.value}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">工作内容</label>
                <input type="text" name="workContent" class="form-control"/>
            </div>
            <div class="form-group">
                <label class="control-label">标签</label>
                <div class="input-group">
                    <select name="itemTypeNames" class="form-control">
                        <option value="">全部</option>
                        <c:forEach items="${itemTypeList }" var="type">
                            <option value="${type.typeName }">${type.typeName }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <button type="button" id="btn_query" class="btn btn-success">搜索</button>
                <button type="reset" id="reset" class="btn btn-white">清空</button>
                <input type="hidden" name="isDelete" value="false"/>
                <button type="submit" id="btn_export" class="btn btn-warning">导出全部</button>
            </div>
        </form>
        <div id="toolbar" class="tooltip-demo">
            <button onclick="reload()" class="btn btn-white btn-sm">
                <i class="glyphicon glyphicon-refresh"></i>
            </button>
            <button id="btn_complete" class="btn btn-warning btn-sm" data-toggle="tooltip" title="标记为已完成">
                <i class="fa fa-check"></i> 标记已完成
            </button>
            <button id="btn_remove" class="btn btn-danger btn-sm">
                <i class="glyphicon glyphicon-trash"></i> 删除
            </button>
        </div>
        <table id="table"></table>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()"> <i
            class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script
        src="hplus/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<script
        src="hplus/js/plugins/bootstrap-table/bootstrap-table-export.js"></script>
<script src="hplus/js/plugins/bootstrap-table/tableExport.js"></script>
<script src="hplus/js/plugins/bootstrap-table/analytics.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>

<![endif]-->
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/laydate/laydate.js"></script>
<script src="js/function.js"></script>
<script src="js/formatter.js"></script>
<script type="text/javascript">
    $(function () {
        //1.初始化Table
        var oTable = new TableInit();
        oTable.Init();

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    var TableInit = function () {
        var oTableInit = new Object();
        var cols = [{
            checkbox: true,
        }, {
            title: '序号',
            width: '50',
            align: 'center',
            formatter: indexFormatter
        }, {
            field: 'operate',
            title: '操作',
            align: 'center',
            width: '130',
            events: operateEvents,
            formatter: operateFormatter
        }, {
            field: 'addTime',
            title: '录入日期',
            align: 'center',
            width: '90',
            formatter: dateTimeFormatter
        }, {
            field: 'addShiftType',
            title: '录入班次',
            align: 'center',
            formatter: shiftTypeFormatter
        }, {
            field: 'endTime',
            title: '完成日期',
            align: 'center',
            width: '90',
            formatter: dateTimeFormatter
        }, {
            field: 'endShiftType',
            title: '完成班次',
            align: 'center',
            formatter: shiftTypeFormatter
        }, {
            field: 'endHandlerName',
            title: '完成人',
            align: 'center'
        }, {
            field: 'itemTypeNames',
            title: '标签',
            align: 'center'
        }, {
            field: 'workContent',
            title: '工作内容',
            // width: '30%',
            align: 'center'
        }, {
            field: 'handleInformation',
            title: '处理情况',
            align: 'center'
        }, {
            field: 'isComplete',
            title: '完成状态',
            align: 'center',
            formatter: booleanFormatter
        }, {
            field: 'addManName',
            title: '添加人',
            align: 'center',
        }];

        //初始化Table
        oTableInit.Init = function () {
            $('#table').bootstrapTable({
                url: 'api/workItem/search', //请求后台的URL（*）
                method: 'get', //请求方式（*）
                toolbar: '#toolbar', //工具按钮用哪个容器
                striped: true, //是否显示行间隔色
                cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true, //是否显示分页（*）
                sortable: true, //是否启用排序
                sortOrder: "asc", //排序方式
                queryParams: oTableInit.queryParams,//传递参数（*）
                sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
                pageNumber: 1, //初始化加载第一页，默认第一页
                pageSize: 50, //每页的记录行数（*）
                pageList: [50, 200, 500, 1000], //可供选择的每页的行数（*）
                escape: true,
                dataField: 'data',
                search: false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: false,
                showColumns: true, //是否显示所有的列
                //showRefresh : true, //是否显示刷新按钮
                minimumCountColumns: 2, //最少允许的列数
                clickToSelect: true, //是否启用点击选中行
                height: 600, //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: "ID", //每一行的唯一标识，一般为主键列
                responseHandler: oTableInit.returnParams,//返回参数（*）
                //showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
                cardView: false, //是否显示详细视图
                detailView: false, //是否显示父子表
                showExport: true,
                exportTypes: ['excel', 'csv', 'txt'],
                exportOptions: {
                    fileName: '工作事项' + DateFormat(new Date(), 'yyyy-MM-dd'),
                    ignoreColumn: [0, -1],
                },
                columns: cols,
            });
        };

        //得到查询的参数
        oTableInit.queryParams = function (params) {
            initDate();
            var temp = $('#searchForm').serialize() + '&length=' + params.limit + '&begin=' + params.offset;
            return temp;
        };
        //得到返回的参数
        oTableInit.returnParams = function (res) {
            return responsePageData(res);
        };

        function operateFormatter(value, row, index) {
            return [
                '<a class="view btn btn-white btn-xs" href="javascript:void(0)" title="">',
                '<i class="fa fa-folder"> 查看</i>',
                '</a> ',
                '<a class="edit btn btn-white btn-xs" href="javascript:void(0)" title="">',
                '<i class="fa fa-pencil"></i> 编辑',
                '</a>'].join(' ');
        }

        return oTableInit;
    };

    function getIdSelections() {
        return $.map($('#table').bootstrapTable('getSelections'), function (row) {
            return row.workItemId
        });
    }

    window.operateEvents = {
        'click .view': function (e, value, row, index) {
            var href = "web/workItem/info?workItemId=" + row.workItemId;
            openWindow("查看交接内容", href, false);
        },
        'click .edit': function (e, value, row, index) {
            var href = "web/workItem/update?workItemId=" + row.workItemId;
            openWindow("编辑交接内容", href);
        }
    };

    function initDate() {
        var endTime = $('#endTime').val();
        if (endTime != '') {
            var days = endTime.split(" - ");
            $('#searchForm input[name="endTime"]').val(new Date(days[0]));
            $('#searchForm input[name="endTimeEnd"]').val(new Date(days[1]));
            $('#searchForm input[name="endTime"]').removeAttr("disabled");
            $('#searchForm input[name="endTimeEnd"]').removeAttr("disabled");
        } else {
            $('#searchForm input[name="endTime"]').attr("disabled", "disabled");
            $('#searchForm input[name="endTimeEnd"]').attr("disabled", "disabled");
        }

        var addTime = $('#addTime').val();
        if (addTime != '') {
            var days = addTime.split(" - ");
            $('#searchForm input[name="addTime"]').val(new Date(days[0]));
            $('#searchForm input[name="addTimeEnd"]').val(new Date(days[1]));
            $('#searchForm input[name="addTime"]').removeAttr("disabled");
            $('#searchForm input[name="addTimeEnd"]').removeAttr("disabled");
        } else {
            $('#searchForm input[name="addTime"]').attr("disabled", "disabled");
            $('#searchForm input[name="addTimeEnd"]').attr("disabled", "disabled");
        }
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};
        var url = "api/workItem/";

        oInit.Init = function () {
            //初始化页面上面的按钮事件

            $('#btn_remove').click(function () {
                var ids = getIdSelections();
                go(ids, url + "remove?workItemIds=", "确认要移除选定的项目吗？");
            });

            $('#btn_complete').click(function () {
                var ids = getIdSelections();
                go(ids, url + "complete?workItemIds=", "确认要标记选定的项目为【已完成】吗？");
            });

            //search按钮绑定查询事件
            $('#btn_query').click(function () {
                layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
                //重新搜索
                $('#table').bootstrapTable('refreshOptions', {
                    url: 'api/workItem/search?',
                    pageNumber: 1
                });
            });

            //日期
            laydate.render({
                elem: '#endTime',
                range: true
            });
            laydate.render({
                elem: '#addTime',
                range: true
            });

            $("#searchForm").submit(function (e) {
                if (!confirm("是否导出数据？")) {
                    e.preventDefault();
                    return;
                }
                initDate();
            });
        };

        return oInit;
    };

</script>
<style>
    .tab-pane {
        font-size: 14px;
    }
</style>
</body>
</html>
