<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/chosen/chosen.css" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>添加值班员</h5>
                </div>
                <div class="ibox-content">
                    <div class="alert alert-warning">值班员姓名提交后不能修改。</div>
                    <form:form modelAttribute="workRecord" class="form-horizontal m-t" id="typeForm" action="">

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-danger">*值班员：</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <select id="watcher" name="watcher" data-placeholder="请选择值班员"
                                            class="chosen-select " multiple style="width:350px;">
                                        <c:forEach items="${watcherList }" var="watcher">
                                            <option value="${watcher.typeName }">${watcher.typeName }</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-danger">*值班日期：</label>
                            <div class="col-sm-4">
                                <input id="dutyDate" class="form-control layer-date" value="<fmt:formatDate value='${workRecord.dutyDate}' pattern='${DateFormat}'/>">
                                <input type="hidden" name="dutyDate">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-danger">*值班班次：</label>
                            <div class="col-sm-4">
                                <form:select path="shiftType" class="form-control" itemValue="${workRecord.shiftType}">
                                    <c:forEach items="${ShiftTypeEnum}" var="type">
                                        <form:option value="${type.key}">${type.value}</form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-label"></div>
                            <div class="col-sm-4">
                                <button class="btn btn-primary" type="button" id="submit">提交</button>
                                <button class="btn btn-white" type="button" onclick="reload()">清空</button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/chosen/chosen.jquery.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/laydate/laydate.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">
    var needLoginCode = <%=MyResponse.getNeedLoginCode()%>;
    var basePath = "<%= BaseController.getBasePath(request) %>";
    var successCode = <%=MyResponse.getSuccessCode()%>;

    $(document).ready(function () {
        var config = {
            ".chosen-select" : {},
            ".chosen-select-deselect" : {
                allow_single_deselect : !0
            },
            ".chosen-select-no-single" : {
                disable_search_threshold : 10
            },
            ".chosen-select-no-results" : {
                no_results_text : "Oops, nothing found!"
            },
            ".chosen-select-width" : {
                width : "95%"
            }
        };
        for ( var selector in config)
            $(selector).chosen(config[selector]);

        $('#submit').click(function () {
            if (valid_form()) {
                var url = "api/workRecord/add";
                $('#submit').attr("disabled", "disabled");
                var vPost = $('#typeForm').serialize();
                post(url, vPost, closeWindow);
            }
        });

        //验证表单
        function valid_form() {
            if ($('select[name="watcher"]').val() == '') {
                alert("值班员必须填写！");
                return false;
            }
            var dutyDate = $('#dutyDate').val();
            if (dutyDate != '') {
                $('input[name="dutyDate"]').val(new Date(dutyDate));
            }else {
                alert("值班日期不能为空！");
                return false;
            }
            return true;
        }

        //日期范围
        laydate.render({
            elem: '#dutyDate',
            type: 'date'
        });

    });

</script>
</body>
</html>
