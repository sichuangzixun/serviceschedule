<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<% request.setAttribute("TimeFormat", BaseController.TimeFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content">

    <div class="mail-box-header">
        <div class="mail-tools tooltip-demo">
            <button onclick="reload()" class="btn btn-white btn-sm" data-toggle="tooltip" title="刷新">
                <i class="glyphicon glyphicon-refresh"></i>
            </button>
            <button id="btn_remove" class="btn btn-danger btn-sm" data-toggle="tooltip" title="移除附件">
                <i class="glyphicon glyphicon-trash"></i> 删除
            </button>
        </div>
    </div>
    <div class="mail-box">
        <table class="table table-hover table-mail">
            <tbody>
            <tr class="unread">
                <td class="check-mail">
                    <input type="checkbox" class="i-checks" id="select_all" value="1">
                </td>
                <td class="">名称</td>
                <td class="">上传人</td>
                <td>上传时间</td>
                <td class=""></td>
            </tr>
            <c:forEach var="file" items="${fileList}">
                <tr class="">
                    <td class="check-mail">
                        <input type="checkbox" class="i-checks" name="fileId[]" value="${file.fileId }">
                    </td>
                    <td class="project-title">${file.fileName}</td>
                    <td>${file.addManName}</td>
                    <td>
                        <fmt:formatDate value="${file.addTime}" pattern="yyyy-MM-dd HH:mm"/>
                    </td>
                    <td class="project-actions">
                        <a href="download?filename=${file.fileUrl}" download="${file.fileName}"
                           class="btn btn-white btn-sm">
                            <i class="glyphicon glyphicon-save"></i>
                            下载
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="hr-line-dashed"></div>
    </div>

</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()"> <i
            class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>

<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/iCheck/icheck.min.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>

<script>

    $(document).ready(function () {
        $(".i-checks").iCheck({
            checkboxClass: "icheckbox_square-green",
            radioClass: "iradio_square-green",
        })
    });
</script>
<script type="text/javascript">

    $(function () {

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    var successCode = '<%=MyResponse.getSuccessCode()%>';
    var needLoginCode = '<%=MyResponse.getNeedLoginCode()%>';
    var basePath = "<%= BaseController.getBasePath(request) %>";

    function getIdSelections() {
        var spCodesTemp = "";
        $("input:checkbox[name='fileId[]']:checked").each(
            function (i) {
                if (0 == i) {
                    spCodesTemp = $(this).val();
                } else {
                    spCodesTemp += ("," + $(this).val());
                }
            });
        return spCodesTemp;
    }
    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};
        var url = "api/workItemFile/";

        oInit.Init = function () {
            //初始化页面上面的按钮事件
            $('#btn_remove').click(function () {
                var ids = getIdSelections();
                go(ids, url + "remove?fileIds=", "确认要移除选定的项目吗？", reload);
            });

            $('#select_all').on('ifChecked', function (event) {
                $("input").iCheck('check');
            });
            $('#select_all').on('ifUnchecked', function (event) {
                $("input").iCheck('uncheck');
            });

        };

        return oInit;
    };


</script>
<style>
    .tab-pane {
        font-size: 14px;
    }
</style>
</body>
</html>
