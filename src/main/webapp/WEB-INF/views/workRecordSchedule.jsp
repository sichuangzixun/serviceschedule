<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-table.css"
          rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-editable.css">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" style="min-width: 1700px">
<div class="ibox">
    <div class="ibox-content">
        <form id="searchForm" class="form-inline" role="form" action=""
              method="post">
            <div class="form-group">
                <label class="control-label">值班月份</label>
                <input id="dutyDate" class="form-control layer-date"
                       value="<fmt:formatDate value='${dutyDate}' pattern='yyyy-MM'/>">
                <input type="hidden" name="dutyDate">
            </div>
            <div class="form-group">
                <button type="submit" id="btn_query" class="btn btn-success">搜索</button>
                <button type="reset" id="reset" class="btn btn-white">清空</button>
            </div>
        </form>
        <div id="toolbar" class="tooltip-demo m-b">
            <button onclick="reload()" class="btn btn-white btn-sm">
                <i class="glyphicon glyphicon-refresh"></i>
            </button>
            <button onclick="upload();" class="btn btn-primary btn-sm">
                导入
            </button>
            <button id="btn_export" class="btn btn-warning btn-sm">
                导出
            </button>
            <button id="btn_remove" class="btn btn-danger btn-sm">
                <i class="glyphicon glyphicon-trash"></i> 删除
            </button>
            <button onclick="openWindow('计划排班表','web/workRecord/file', false)" class="btn btn-white btn-sm">
                <i class="glyphicon glyphicon-save"></i>计划排班表
            </button>
        </div>

        <table id="table" class="table table-bordered">
            <thead>
            <tr>
                <th colspan="${end+2}">
                    <h3 id="title"><fmt:formatDate value="${dutyDate }" pattern="yyyy年MM月"/>服务调度排班表</h3>
                </th>
            </tr>
            <tr>
                <th>序号</th>
                <th>日期</th>
                <c:forEach var="day" begin="1" end="${end}">
                    <th>${month}/${day}</th>
                </c:forEach>
            </tr>
            <tr>
                <th></th>
                <th>星期</th>
                <c:forEach var="day" begin="1" end="${end}">
                    <c:set var="now" value="${day}-${month}-${year}"/>
                    <fmt:parseDate value="${now}" var="parsedEmpDate"
                                   pattern="dd-MM-yyyy"/>
                    <fmt:formatDate value="${parsedEmpDate}" pattern="E" var="dayOfWeek"/>
                    <th>${fn:replace(dayOfWeek, "星期", "")}</th>
                </c:forEach>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="list" items="${watcherList}" varStatus="status">
                <tr>
                    <td>${status.index + 1}</td>
                    <c:forEach items="${list}" var="item">
                        <td>${item}</td>
                    </c:forEach>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>
</div>

<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="js/laydate/laydate.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script src="js/jquery.table2excel.js"></script>
<style>
    table tr th, td {
        text-align: center;
    }
</style>
<script type="text/javascript">
    $(function () {

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    function initDate() {
        var dutyDate = $('#dutyDate').val();
        if (dutyDate != '') {
            $('input[name="dutyDate"]').val(new Date(dutyDate));
        } else {
            alert("值班月份不能为空！");
        }
    }

    function upload() {
        initDate();
        var vPost = $('#searchForm').serialize();
        openWindow('导入排班表', 'web/workRecord/uploadExcel?' + vPost);
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};

        oInit.Init = function () {

            //初始化页面上面的按钮事件
            $('#btn_remove').click(function () {
                initDate();
                var url = "api/workRecord/removeByMonth?";
                var ids = $('#searchForm').serialize();
                go(ids, url, "确认要移除整个月份的值班记录吗？", none);
            });

            //日期范围
            laydate.render({
                elem: '#dutyDate',
                type: 'month'
            });

            $("#searchForm").submit(function (e) {
                initDate();
            });

            $("#btn_export").click(function () {
                if (!confirm("是否导出数据？")) {
                    return;
                }
                $('#table').table2excel({
                    filename: $('#title').text()
                });
            });
        };

        return oInit;
    };
</script>
</body>
</html>
