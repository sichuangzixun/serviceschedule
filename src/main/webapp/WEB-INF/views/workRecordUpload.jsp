<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@page import="com.sichuang.schedule.common.MyResponse" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% request.setAttribute("DateFormat", BaseController.DateFormat);%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg" id="page">
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <ol class="breadcrumb">
                        <li><a href="web/daySchedule/info?scheduleId=${workRecord.scheduleId}"> 服务调度交接班记录</a></li>
                        <li><strong>导入值班员</strong></li>
                    </ol>
                </div>
                <div class="ibox-content">
                    <div class="alert alert-warning">导入文件为EXCEL2007版（后缀为xlsx）！导入文件较大，请耐心等待！</div>
                    <form class="form-horizontal m-t" id="uploadForm" enctype="multipart/form-data"
                          action="web/workRecord/import" method="post">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">值班月份：</label>
                            <div class="col-sm-6">
                                <input id="dutyDate" class="form-control layer-date" value="<fmt:formatDate value='${dutyDate}' pattern='yyyy-MM'/> ">
                                <input type="hidden" name="dutyDate">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">选择文件：</label>
                            <div class="col-sm-6">
                                <input name="filename" type="file" id="file"
                                       class="form-control" accept=".xlsx"/>
                                <span class="help-block m-b-none">
										<i class="fa fa-info-circle"></i> 文件后缀为"xlsx"，大小不超过60M
										</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary" id="submitForm">提交</button>

                            </div>
                        </div>
                    </form>
                    <div class="progress progress-striped active">
                        <div style="width: 0%" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-danger">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                    <div class="progress-value"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/laydate/laydate.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        //导入
        $('#uploadForm').submit(function(e) {
            if (!confirm("是否导入数据？")){
                e.preventDefault();
                return;
            }
            var filename = $('#file').val();
            if (filename == "") {
                alert("请选择文件上传！");
                e.preventDefault();
                return false;
            }
            var dutyDate = $('#dutyDate').val();
            if (dutyDate != '') {
                $('input[name="dutyDate"]').val(new Date(dutyDate));
            }else {
                alert("值班月份不能为空！");
            }
            layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
        });

        //日期范围
        laydate.render({
            elem: '#dutyDate',
            type: 'month'
        });

    });

</script>
</body>
</html>
