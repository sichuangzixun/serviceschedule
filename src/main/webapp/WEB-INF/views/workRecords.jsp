<%@page import="com.sichuang.schedule.controller.BaseController" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=BaseController.getBasePath(request)%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>服务调度交接记录工具</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-table.css"
          rel="stylesheet">
    <link href="hplus/css/plugins/bootstrap-table/bootstrap-editable.css">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="ibox">
    <div class="ibox-content">
        <form id="searchForm" class="form-inline" role="form" action="api/daySchedule/exportExcel" target="_blank" method="post">
            <div class="form-group">
                <label class="control-label">值班日期范围</label>
                <input id="dutyDate" class="form-control layer-date" value="">
                <input type="hidden" name="dutyDate">
                <input type="hidden" name="dutyDateEnd">
            </div>
            <div class="form-group">
                <label class="control-label">值班班次</label>
                <select name="shiftType" class="form-control">
                    <option value="">全部</option>
                    <c:forEach items="${ShiftTypeEnum}" var="type">
                        <option value="${type.key}">${type.value}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">值班员</label>
                <input type="text" id="watcher" name="watcher" class="form-control" list="watcherList"/>
                <datalist id="watcherList">
                    <c:forEach items="${watcherList}" var="watcher">
                        <option value="${watcher.typeName}">${watcher.typeName}</option>
                    </c:forEach>

                </datalist>
            </div>
            <div class="form-group">
                <button type="button" id="btn_query" class="btn btn-success">搜索</button>
                <button type="reset" id="reset" class="btn btn-white">清空</button>
                <input type="hidden" name="isDelete" value="false"/>
            </div>
        </form>
        <div id="toolbar" class="tooltip-demo">
            <button onclick="reload()" class="btn btn-white btn-sm">
                <i class="glyphicon glyphicon-refresh"></i>
            </button>
            <button onclick="openWindow('添加值班记录','web/workRecord/add')" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i> 添加
            </button>
            <button id="btn_remove" class="btn btn-danger btn-sm">
                <i class="glyphicon glyphicon-trash"></i> 删除
            </button>
        </div>
        <table id="table"></table>
    </div>
</div>

<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="js/laydate/laydate.js"></script>
<script src="hplus/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script
        src="hplus/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<script
        src="hplus/js/plugins/bootstrap-table/bootstrap-table-export.js"></script>
<script src="hplus/js/plugins/bootstrap-table/tableExport.js"></script>
<script src="hplus/js/plugins/bootstrap-table/analytics.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
<![endif]-->
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="js/function.js"></script>
<script src="js/formatter.js"></script>
<script type="text/javascript">
    $(function () {
        //1.初始化Table
        var oTable = new TableInit();
        oTable.Init();

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    var TableInit = function () {
        var oTableInit = new Object();
        var scheduleCol = [{
            checkbox: true,
        }, {
            title: '序号',
            width: '50',
            align: 'center',
            formatter: indexFormatter
        },{
            field: 'operate',
            title: '操作',
            align: 'center',
            width: '100',
            events: operateEvents,
            formatter: operateFormatter
        },{
            field: 'dutyDate',
            title: '值班日期',
            width: '150',
            align: 'center',
            formatter: dateFormatter
        }];
        var cols = scheduleCol.concat(workRecordCols);
        //初始化Table
        oTableInit.Init = function () {
            $('#table').bootstrapTable({
                url: 'api/workRecord/search', //请求后台的URL（*）
                method: 'get', //请求方式（*）
                toolbar: '#toolbar', //工具按钮用哪个容器
                striped: true, //是否显示行间隔色
                cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true, //是否显示分页（*）
                sortable: true, //是否启用排序
                sortOrder: "asc", //排序方式
                queryParams: oTableInit.queryParams,//传递参数（*）
                sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
                pageNumber: 1, //初始化加载第一页，默认第一页
                pageSize: 50, //每页的记录行数（*）
                pageList: [50, 200, 500, 1000], //可供选择的每页的行数（*）
                escape: true,
                dataField: 'data',
                search: false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: false,
                showColumns: true, //是否显示所有的列
                //showRefresh : true, //是否显示刷新按钮
                minimumCountColumns: 2, //最少允许的列数
                clickToSelect: true, //是否启用点击选中行
                height: 600, //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: "ID", //每一行的唯一标识，一般为主键列
                responseHandler: oTableInit.returnParams,//返回参数（*）
                //showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
                cardView: false, //是否显示详细视图
                detailView: false, //是否显示父子表
                showExport: true,
                exportTypes: ['excel', 'csv', 'txt'],
                exportOptions: {
                    fileName: '值班记录' + DateFormat(new Date(), 'yyyy-MM-dd'),
                    ignoreColumn: [0, -1],
                },
                columns: cols,
            });
        };
        //得到查询的参数
        oTableInit.queryParams = function (params) {
            initDate();
            var temp = $('#searchForm').serialize() + '&length=' + params.limit + '&begin=' + params.offset;
            return temp;
        };
        //得到返回的参数
        oTableInit.returnParams = function (res) {
            return responsePageData(res);
        };


        function operateFormatter(value, row, index) {
            return [
                '<a class="edit btn btn-white btn-xs" href="javascript:void(0)" title="">',
                '<i class="fa fa-edit"></i> 编辑',
                '</a>',
                ].join('');
        }

        return oTableInit;
    };

    function getIdSelections() {
        return $.map($('#table').bootstrapTable('getSelections'), function (row) {
            return row.workRecordId
        });
    }

    window.operateEvents = {
        'click .edit': function (e, value, row, index) {
            var href = "web/workRecord/update?workRecordId=" + row.workRecordId;
            openWindow("编辑值班记录", href);
        }
    };


    function initDate() {
        var dutyDate = $('#dutyDate').val();
        if (dutyDate != '') {
            var days = dutyDate.split(" - ");
            $('#searchForm input[name="dutyDate"]').val(new Date(days[0]));
            $('#searchForm input[name="dutyDateEnd"]').val(new Date(days[1]));
            $('#searchForm input[name="dutyDate"]').removeAttr("disabled");
            $('#searchForm input[name="dutyDateEnd"]').removeAttr("disabled");
        } else {
            $('#searchForm input[name="dutyDate"]').attr("disabled", "disabled");
            $('#searchForm input[name="dutyDateEnd"]').attr("disabled", "disabled");
        }
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};
        var url = "api/workRecord/";

        oInit.Init = function () {

            //初始化页面上面的按钮事件
            $('#btn_remove').click(function () {
                var ids = getIdSelections();
                go(ids, url + "remove?workRecordIds=", "确认要移除选定的项目吗？");

            });

            //search按钮绑定查询事件
            $('#btn_query').click(function () {
                layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
                //重新搜索
                $('#table').bootstrapTable('refreshOptions', {
                    url: 'api/workRecord/search?',
                    pageNumber: 1
                });
            });

            //日期范围
            laydate.render({
                elem: '#dutyDate',
                range: true
            });

            $("#searchForm").submit(function (e) {
                if (!confirm("是否导出数据？")) {
                    e.preventDefault();
                    return;
                }
                initDate();
            });
        };

        return oInit;
    };
</script>
</body>
</html>
