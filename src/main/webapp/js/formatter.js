/**
 * 序号
 * @param value
 * @param row
 * @param index
 * @returns {*}
 */
function indexFormatter(value, row, index) {
    return index + 1;
}

var workRecordCols = [{
    field: 'shiftType',
    title: '值班班次',
    align: 'center',
    formatter: shiftTypeFormatter
},{
    field: 'watcher',
    title: '值班员',
    align: 'center'
}, {
    field: 'isManager',
    title: '现场负责人',
    align: 'center',
    formatter: booleanFormatter
}, {
    field: 'addTime',
    title: '添加时间',
    align: 'center',
    formatter: dateTimeFormatter,
}, {
    field: 'addManName',
    title: '添加人',
    align: 'center',
}];

/**
 * 值班班次：0-无，1-白班，2-早班，3-夜班，4-宵班
 * @param val
 * @returns {*}
 */
function shiftTypeFormatter(val){
    var type = {0:'无',1:'白班',2:'早班',3:'夜班',4:'宵班'};
    return type[val];
}

/**
 * 考勤类型：1-正常，2-迟到，3-早退，4-缺席
 * @param val
 * @returns {*}
 */
function attendanceTypeFormatter(val){
    var type = {1:'正常',2:'迟到',3:'早退',4:'缺席'};
    return type[val];
}

/**
 * 交接状态：1-未交接，2-已交接
 * @param val
 * @returns {*}
 */
function shiftStatusFormatter(val){
    var type = {1:'未交接',2:'已交接'};
    return type[val];
}

/**
 * 注意事项类型：1-长期知识，2-短期知识，3-常规工作
 * @param val
 * @returns {*}
 */
function pointTypeFormatter(val){
    var type = {1:'长期知识',2:'短期知识',3:'常规工作'};
    return type[val];
}

/**
 * 工作内容类型：1-常规，2-紧急
 * @param val
 * @returns {*}
 */
function contentTypeFormatter(val){
    var type = {1:'常规',2:'紧急'};
    return type[val];
}

/**
 * 用户类型：1-管理员，2-普通用户
 * @param val
 * @returns {*}
 */
function userTypeFormatter(val){
    var type = {1:'管理员',2:'普通用户'};
    return type[val];
}

/**
 * 布尔型状态
 * @param val
 * @returns {*}
 */
function booleanFormatter(val) {
    if(val == false){
        return "否";
    }
    if(val == true){
        return "是";
    }
    return val;
}