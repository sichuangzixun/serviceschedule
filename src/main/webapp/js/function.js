/**
 * Created by sc on 2017-3-10.
 */
var basePath = "/schedule/";
var needLoginCode = -1;
var successCode = 40000;
var errorCode = 40001;
/**
 * ajax请求失败
 */
$(document).ajaxError(function () {
    if (layer) {
        layer.closeAll('loading');
    }
    alert("服务器请求失败！");
});

/**
 * 处理ajax返回数据
 * @param data
 */
function responseData(data) {
    if(layer){
        layer.closeAll('loading');
    }
    if (data.hasOwnProperty("code") == false) {
        alert("请求失败！");
    }
    if (data.code == needLoginCode) {
        go_index();
    }
    if (data.code == errorCode) {
        alert(data.msg);
    }
}

/**
 * 分页信息
 * @param res
 * @returns {{data: Array, total: number}}
 */
function responsePageData(res) {
    responseData(res);
    var data = [];
    var total = 0;
    if (res.code == successCode) {
        data = res.data;
        total = res.page.totalRecords;
    }
    return {
        "data": data,
        "total": total,
    };
}
/**
 * 操作
 * @param ids
 * @param act
 * @param word
 */
function go(ids, act, word, successResponse) {
    if (ids.length == 0) {
        alert("您还没有选择！");
        return;
    }
    if (!confirm(word))
        return;
    if (successResponse == null) {
        successResponse = refreshTable;
    }
    layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
    $.post(act + ids, "", function (data) {
        responseData(data);
        if (data.code == successCode) {
            alert(data.msg);
            successResponse();
        }
    });
}

/**
 * 提交表单
 * @param url
 * @param vPost
 * @param successResponse
 */
function post(url, vPost, successResponse) {
    if (successResponse == null) {
        successResponse = refreshTable;
    }
    layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
    $.post(url, vPost, function (data) {
        responseData(data);
        if (data.code == successCode) {
            if($.trim(data.msg) != ""){
                alert(data.msg);
            }
            successResponse();
        }
        $('#submit').removeAttr("disabled");
    });
}

/**
 * 刷新表格数据
 */
function refreshTable() {
    $('#table').bootstrapTable('refresh');
}

/**
 * 刷新页面
 */
function reload() {
    window.location.reload();
}

/**
 * 跳转到首页
 */
function go_index() {
    top.location.href = basePath;
}

/**
 * 退出登录
 */
function logout() {
    top.location.href = basePath + 'web/user/logout';
}
/**
 * 无响应
 */
function none() {

}

/**
 * 返回页面顶部
 */
function page_top() {
    $("html,body").animate({scrollTop: 0}, 500);
}

/**
 * 初始化select选中选项
 * @param select
 * @param values
 */
function chose_mult_set_ini(select, values) {
    var arr = values.split(',');
    var length = arr.length;
    var value = '';
    for (i = 0; i < length; i++) {
        value = arr[i];
        if (value != "") {
            $(select + " option[value='" + value + "']").attr('selected',
                'selected');
        }

    }
}

/**
 * 格式化日期时间
 * @param data
 * @returns {string|*}
 */
function dateTimeFormatter(data) {
    return DateFormat(data, 'yyyy-MM-dd hh:mm')
}

/**
 * 格式化日期
 * @param data
 * @returns {string|*}
 */
function dateFormatter(data) {
    return DateFormat(data, 'yyyy-MM-dd')
}

/**
 * 格式化时间
 * @param data
 * @returns {string|*}
 */
function timeFormatter(data) {
    return DateFormat(data, 'hh:mm')
}

/**
 * 时间戳格式化
 * @param timestamp
 * @param format
 * @returns
 */
function DateFormat(timestamp, format) {
    if (timestamp == null) {
        return "-";
    }
    timestamp = new Date(timestamp);
    var date = {
        "M+": timestamp.getMonth() + 1,
        "d+": timestamp.getDate(),
        "h+": timestamp.getHours(),
        "m+": timestamp.getMinutes(),
        "s+": timestamp.getSeconds(),
        "q+": Math.floor((timestamp.getMonth() + 3) / 3),
        "S+": timestamp.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (timestamp.getFullYear() + '')
            .substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k]
                : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
}

// 保留两位小数点
function toDecimal(num) {
    return parseInt(num * 100) / 100;
}

// 验证手机号码
function validate_mobile(mobile) {
    if (mobile.length > 0) {
        if (mobile.length != 11) {
            return false;

        }

        var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        if (!myreg.test(mobile)) {
            return false;

        }
    }
    return true;
}

/*
 * 功能： 模拟form表单的提交 参数： URL 跳转地址 PARAMTERS 参数 返回值： 创建时间：20160713 创建人：
 */
function Post(URL, PARAMTERS) {
    // 创建form表单
    var temp_form = document.createElement("form");
    temp_form.action = URL;
    // 如需打开新窗口，form的target属性要设置为'_blank'
    temp_form.target = "_self";
    temp_form.method = "post";
    temp_form.style.display = "none";
    // 添加参数
    for (var item in PARAMTERS) {
        var opt = document.createElement("textarea");
        opt.name = PARAMTERS[item].name;
        opt.value = PARAMTERS[item].value;
        temp_form.appendChild(opt);
    }
    document.body.appendChild(temp_form);
    // 提交数据
    temp_form.submit();
}

/**
 * 秒转换为时分秒
 *
 * @param value
 * @returns {String}
 */
function formatSeconds(value) {
    var theTime = parseInt(value);// 秒
    var theTime1 = 0;// 分
    var theTime2 = 0;// 小时
    // alert(theTime);
    if (theTime > 60) {
        theTime1 = parseInt(theTime / 60);
        theTime = parseInt(theTime % 60);
        // alert(theTime1+"-"+theTime);
        if (theTime1 > 60) {
            theTime2 = parseInt(theTime1 / 60);
            theTime1 = parseInt(theTime1 % 60);
        }
    }
    var result = "" + parseInt(theTime) + "秒";
    if (theTime1 > 0) {
        result = "" + parseInt(theTime1) + "分" + result;
    }
    if (theTime2 > 0) {
        result = "" + parseInt(theTime2) + "小时" + result;
    }
    return result;
}

/**
 * 打开iframe窗口
 * @param title
 * @param url
 * @param refresh 是否刷新
 */
function openWindow(title, url, refresh) {
    layer.closeAll();
    layer.open({
        id: 'editWindow',
        type: 2,
        title: title,
        shadeClose: true,
        maxmin: true,
        // shade: true,
        area: ['90%', '90%'],
        content: url
        , end: function () {
            refresh = refresh == null ? false : refresh;
            if (refresh) {
                //右上角关闭回调
                window.location.reload();
            }
        }
    });
}

/**
 * 关闭编辑窗口
 */
function closeWindow() {
    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    parent.layer.close(index); //再执行关闭
}

/**
 * textarea显示换行符
 */
$(".re-br").each(function() {
    var temp =  $(this).text().replace(/\n|\r\n/g,'<br/>');
    $(this).html(temp);
});